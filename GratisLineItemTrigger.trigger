trigger GratisLineItemTrigger on Gratis_Line_Items__c (after insert, before delete, after update){
   
   //Definitions
   ID ownerId;
   ID sortingId;
   ID oldsortingId;
   List<Warehouse_Document__c> wdlist;
     // List<Warehouse__c> wlist;
   Warehouse_Document__c wd;
   List<Warehouse_Document_Line__c> createList = new List<Warehouse_Document_Line__c>();
   Warehouse__c w;
   Map<ID, ID> gratisItemsMapForUpdate = new Map<ID, ID>();
   
   if (Trigger.IsInsert)
   {
       Set<ID> ids = Trigger.newMap.keySet();
       List<Gratis_Line_Items__c> glis = 
       [SELECT Id, Date_Sent_Left__c,Title__c, Quantity__c, Related_Gratis_Item__c, Related_Gratis_Item__r.Campaign__c, Related_Gratis_Item__r.Id, Related_Gratis_Item__r.RecordTypeId, Related_Gratis_Item__r.OwnerId FROM Gratis_Line_Items__c 
       WHERE id in :ids 
       and (Related_Gratis_Item__r.Campaign__c = null OR Related_Gratis_Item__r.Adoption__c != null) //campaigns not managed - need to create shipment manually except if created from adoption
       and Related_Gratis_Item__r.RecordTypeId != '012200000000Wit' //not orders
       ORDER BY Related_Gratis_Item__r.OwnerId];

       for (Gratis_Line_Items__c gli: glis)
       {
           //checking the owner and the date
           sortingId = gli.Related_Gratis_Item__r.OwnerId;
           ID warehouseDocumentId;
           Date gidate = Date.newInstance(gli.Date_Sent_Left__c.year(), gli.Date_Sent_Left__c.month(), 1);
           //managing multiple owners in bulk create
           if (oldsortingId==null || sortingId != oldsortingId)
           {
           
           system.debug('sortingId'+sortingId);
           
                //update what was until now
                if (oldsortingId!=null)
                {
                system.debug('oldsortingId'+oldsortingId);
                WarehouseManagement.updateDocument(oldsortingId,createList,'add', wd);
                //WarehouseManagement.updateStock(oldsortingId,createList,'substract');
                createList =  new List<Warehouse_Document_Line__c>();
                }
           //checking whether there is an warehouse for that person
           
           if (WarehouseManagement.userHasWarehouse(sortingId))
           {
           w = WarehouseManagement.getWarehouseByUser(sortingId);
           //wlist = [select id from Warehouse__c where Warehouse_Owner__c =: sortingId];
         
             
         
                   //checking whether there is an open GRATIS document for that user and month
                   wdlist = [select id from Warehouse_Document__c where Status__c = 'New' and RecordType.Name ='Gratis' and Warehouse__c =: w.id and Date__c >=: gidate];
                   //no document - add new GRATIS doc.
                   if (wdlist.size() == 0)
                   {
                       wd = new Warehouse_Document__c();
                       wd.Status__c = 'New';
                       wd.RecordTypeId = [Select Id from RecordType where DeveloperName = 'Gratis'][0].Id;
                       wd.Date__c = gidate ;
                       wd.Warehouse__c = w.id;
                       database.insert(wd);
                   }
                   
                   wdlist = [select id, RecordType.Name, Warehouse__r.OwnerId  from Warehouse_Document__c where Status__c = 'New' and RecordType.Name ='Gratis' and Warehouse__c =: w.id and Date__c >=: gidate];
                   wd = wdlist[0];
               }
               oldsortingId = sortingId;
           }
           
           if (wd != null)
           {
               if (!gratisItemsMapForUpdate.containsKey(gli.Related_Gratis_Item__c))
               {
               gratisItemsMapForUpdate.put(gli.Related_Gratis_Item__c, wd.id);
               }
               //End Gratis Document not exists
               //move lines and update stock
               Warehouse_Document_Line__c wdl = new Warehouse_Document_Line__c();
               wdl.Document__c = wd.id;
               wdl.WD_book_title__c = gli.Title__c;
               wdl.Quantity__c = gli.Quantity__c;
               createList.Add(wdl);
           }
       }
       //end iteration through trigger new
           if (wd != null)
           {
           WarehouseManagement.updateDocument(sortingId,createList,'add', wd);
           //WarehouseManagement.updateStock(sortingId,createList,'substract');
           createList =  new List<Warehouse_Document_Line__c>();
           }
       //update gratis items with docids
       List<Gratis_Item__c> giInMap = [Select Id from Gratis_Item__c where id in:gratisItemsMapForUpdate.keySet()];
       List<Gratis_Item__c> giToUpdate = new List<Gratis_Item__c>();
       for (Gratis_Item__c gi: giInMap)
       {
       gi.Warehouse_Document__c = gratisItemsMapForUpdate.get(gi.Id);
       giToUpdate.Add(gi);
       }
       database.update(giToUpdate);
     
   }
   //end is insert
    
 if (Trigger.IsUpdate)
   {
       Set<ID> ids = Trigger.newMap.keySet();
       List<Gratis_Line_Items__c> glis = 
       [SELECT Id, Date_Sent_Left__c,Title__c, Quantity__c, Related_Gratis_Item__c, Related_Gratis_Item__r.Id, Related_Gratis_Item__r.RecordTypeId, Related_Gratis_Item__r.OwnerId, Related_Gratis_Item__r.Warehouse_Document__c FROM Gratis_Line_Items__c 
       WHERE id in :ids 
       and Related_Gratis_Item__r.Warehouse_Document__c != null //only attached to a doc
       and Related_Gratis_Item__r.Warehouse_Document__r.Status__c != 'Approved' //that is not approved
       and Related_Gratis_Item__r.RecordTypeId != '012200000000Wit' //not orders
       ORDER BY Related_Gratis_Item__r.Warehouse_Document__c];
       for (Gratis_Line_Items__c gli: glis)
       {
           //checking the owner
           sortingId = gli.Related_Gratis_Item__r.Warehouse_Document__c;
           ownerId = gli.Related_Gratis_Item__r.OwnerId;
            
           //managing multiple owners in bulk create
           if (oldsortingId==null || sortingId != oldsortingId)
           {
           
                //update what was until now
                if (oldsortingId!=null)
                {

                WarehouseManagement.updateDocument(ownerId,createList,'add', wd);
                //WarehouseManagement.updateStock(ownerId,createList,'substract');
                createList =  new List<Warehouse_Document_Line__c>();
                }
                   wdlist = [select id, RecordType.Name, Warehouse__r.OwnerId from Warehouse_Document__c where id=:sortingId];
                   oldsortingId = sortingId;
                   wd = wdlist[0];
           }
           

           if (wd != null)
           {

               //End Gratis Document not exists
               //move lines and update stock
               Warehouse_Document_Line__c wdl = new Warehouse_Document_Line__c();
               wdl.Document__c = wd.id;
               wdl.WD_book_title__c = gli.Title__c;
               wdl.Quantity__c = gli.Quantity__c - trigger.oldMap.get(gli.Id).quantity__c;
               createList.Add(wdl);
           }
       }
         if (wd != null)
           {
           //end iteration through trigger new
           WarehouseManagement.updateDocument(ownerId,createList,'add', wd);
          // WarehouseManagement.updateStock(ownerId,createList,'substract');
           createList =  new List<Warehouse_Document_Line__c>();
           }
   }
   //end is update 

if (Trigger.IsDelete)
   {
       Set<ID> ids = Trigger.oldMap.keySet();
       List<Gratis_Line_Items__c> glis = 
       [SELECT Id, Date_Sent_Left__c,Title__c, Quantity__c, Related_Gratis_Item__c, Related_Gratis_Item__r.Id, Related_Gratis_Item__r.RecordTypeId, Related_Gratis_Item__r.OwnerId, Related_Gratis_Item__r.Warehouse_Document__c FROM Gratis_Line_Items__c 
       WHERE id in :ids 
       and Related_Gratis_Item__r.Warehouse_Document__c != null //only attached to a doc
       and Related_Gratis_Item__r.Warehouse_Document__r.Status__c != 'Approved' //that is not approved
       and Related_Gratis_Item__r.RecordTypeId != '012200000000Wit' //not orders
       ORDER BY Related_Gratis_Item__r.Warehouse_Document__c];
       for (Gratis_Line_Items__c gli: glis)
       {
           //checking the owner
           sortingId = gli.Related_Gratis_Item__r.Warehouse_Document__c;
           ownerId = gli.Related_Gratis_Item__r.OwnerId;
           //managing multiple owners in bulk create
           if (oldsortingId==null || sortingId != oldsortingId)
           {

                //update what was until now
                if (oldsortingId!=null)
                {
                WarehouseManagement.updateDocument(ownerId,createList,'add', wd);
                //WarehouseManagement.updateStock(ownerId,createList,'substract');
                createList =  new List<Warehouse_Document_Line__c>();

                }
                   system.debug(sortingId);
                   wdlist = [select id, RecordType.Name, Warehouse__r.OwnerId from Warehouse_Document__c where id=:sortingId];
                   oldsortingId = sortingId;
                   wd = wdlist[0];
           }
           
           
           if (wd != null)
           {
               //move lines and update stock
               Warehouse_Document_Line__c wdl = new Warehouse_Document_Line__c();
               wdl.Document__c = wd.id;
               wdl.WD_book_title__c = gli.Title__c;
               wdl.Quantity__c = 0 - gli.Quantity__c;
               createList.Add(wdl);
           }
       }
                  //end iteration through trigger OLD
         if (wd != null)
           {
           WarehouseManagement.updateDocument(ownerId,createList,'add', wd);
           //WarehouseManagement.updateStock(ownerId,createList,'substract');
           createList =  new List<Warehouse_Document_Line__c>();
           }
   }
   //end is delete
   
   
}