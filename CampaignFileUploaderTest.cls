@isTest(SeeAllData=true)
public class CampaignFileUploaderTest
{
public static Campaign campaign;
    public static testMethod void testWithErrorsInURL() 
    {
        Test.setCurrentPageReference(Page.CampaignMembersUpload);
        //test missing CAMPAIGN_URL_PARAM
        CampaignFileUploader controller = new CampaignFileUploader();
        //System.assert(controller.canProceed, 'Failed to identify that process can not be executed');
    }

   public static testMethod void testCancel()
   {
       createStandardCampaign();  
       test.startTest();
       //setting the campaign id
       Test.setCurrentPageReference(Page.CampaignMembersUpload);
       System.currentPageReference().getParameters().put(CampaignFileUploader.CAMPAIGN_URL_PARAM, campaign.Id);
       CampaignFileUploader controller = new CampaignFileUploader ();
       //testing cancel procedure
       controller.cancel();
       test.stopTest();
       database.delete(campaign);  
   }
   
      public static testMethod void testStandardProcess() 
      {    
        createStandardCampaign();  
        test.startTest();
        //setting the id
        Test.setCurrentPageReference(Page.CampaignMembersUpload);
        System.currentPageReference().getParameters().put(CampaignFileUploader.CAMPAIGN_URL_PARAM, campaign.Id);
        CampaignFileUploader controller = new CampaignFileUploader ();
 
        //test create member
        controller.defaultStatus = 'Responded';
        Blob bodyBlob=Blob.valueOf('apruzinski@gmail.com');
        controller.contentFile=bodyBlob;
        controller.ReadFile();
      
        //test update member
        bodyBlob=Blob.valueOf('apruzinski@gmail.com');
        controller.contentFile=bodyBlob;
        controller.ReadFile();
       
        test.stopTest();
        database.delete(campaign);
      }
      
     public static testMethod void testWithError() 
      {    
        createStandardCampaign();  
        test.startTest();
        Test.setCurrentPageReference(Page.CampaignMembersUpload);
        System.currentPageReference().getParameters().put(CampaignFileUploader.CAMPAIGN_URL_PARAM, campaign.Id);
        CampaignFileUploader controller = new CampaignFileUploader ();
        //delete campaign in the middle of the process
        database.delete(campaign);  
        controller.defaultStatus = 'Responded';
        Blob bodyBlob=Blob.valueOf('apruzinski@gmail.com');
        controller.contentFile=bodyBlob;
        controller.ReadFile();
        test.stopTest();
      }
    

        private static void createStandardCampaign() 
        {
          
      campaign                           = new Campaign();
      campaign.Name                      = 'Test Campaign';      
      campaign.StartDate                 = system.today();
      campaign.EndDate            = system.today();
      campaign.Campaign_Objectives__c    = 'Objectives';
      campaign.OUP_Country__c            = 'Poland';
      campaign.Type = 'Webinar';
      campaign.RecordTypeId = '012200000000ekJ';
      database.insert(campaign);
      
      }
      
}