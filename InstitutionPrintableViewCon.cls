public class InstitutionPrintableViewCon {

    public static final String INSTITUTE_URL_PARAM                = 'Id';
    public static final String RETURN_URL_PARAM                   = 'retURL';
    public static final String CANCEL_URL_PARAM                   = 'cancelURL';
    public static final String INCLUDE_ASSOCIATED_CONTACTS_PARAM  = 'incAC';
    public static final String INCLUDE_BOOKS_USED_PARAM           = 'incBU';
    public static final String INCLUDE_OPEN_ACTIVITIES_PARAM      = 'incOA';
    public static final String INCLUDE_ACTIVITY_HISTORY_PARAM     = 'incAH';
    public static final String INCLUDE_ADOPTIONS_PARAM            = 'incAD';
    public static final String INCLUDE_CLOSED_ADOPTIONS_PARAM     = 'incADC';
    public static final String INCLUDE_GRATIS_LINE_ITEMS_PARAM    = 'incGI';
    public static final String INCLUDE_PARTNERS_PARAM             = 'incPT';
    public static final String BOOKS_USED_START_DATE_PARAM        = 'buds';
    public static final String BOOKS_USED_END_DATE_PARAM          = 'budE';
    public static final String GRATIS_LINE_ITEMS_START_DATE_PARAM = 'gids';
    public static final String GRATIS_LINE_ITEMS_END_DATE_PARAM   = 'gidE';
    public static final String OPEN_ACTIVITIES_START_DATE_PARAM   = 'oads';
    public static final String OPEN_ACTIVITIES_END_DATE_PARAM     = 'oadE';    
    public static final String ACTIVITY_HISTORY_START_DATE_PARAM  = 'ahds';
    public static final String ACTIVITY_HISTORY_END_DATE_PARAM    = 'ahdE';
    
    private final Account account;
    private final ID instituteId;
    private final String retURL;
    private final String cancelURL;
    
    private List<Positions_Held__c> associatedContacts;
    private List<Books_Used__c> booksUsed;
    private List<Activity> openActivities;
    private List<Opportunity> adoptions; 
    private List<Gratis_Line_Items__c> gratisLineItems;
    private List<Activity> activities;    
    private List<Partner> partners;
    
    public Boolean incAC  { get; set; }
    public Boolean incBU  { get; set; }
    public Boolean incOA  { get; set; }
    public Boolean incAH  { get; set; }
    public Boolean incAD  { get; set; }
    public Boolean incADC { get; set; }    
    public Boolean incGI  { get; set; }
    public Boolean incPT  { get; set; }    

    private final Task bu_startDate;
    private final Task bu_endDate;
    private final Task gi_startDate;
    private final Task gi_endDate;
    private final Task oa_startDate;
    private final Task oa_endDate;
    private final Task ah_startDate;
    private final Task ah_endDate;
    
    public InstitutionPrintableViewCon() {
                
        try {
            instituteId = System.currentPageReference().getParameters().get(INSTITUTE_URL_PARAM);
            retURL      = System.currentPageReference().getParameters().get(RETURN_URL_PARAM);
            cancelURL   = System.currentPageReference().getParameters().get(CANCEL_URL_PARAM);
            
            // set defaults
            incAC  = (System.currentPageReference().getParameters().get(INCLUDE_ASSOCIATED_CONTACTS_PARAM).equals('1') == true);
            incBU  = (System.currentPageReference().getParameters().get(INCLUDE_BOOKS_USED_PARAM).equals('1') == true);
            incOA  = (System.currentPageReference().getParameters().get(INCLUDE_OPEN_ACTIVITIES_PARAM).equals('1') == true);
            incAH  = (System.currentPageReference().getParameters().get(INCLUDE_ACTIVITY_HISTORY_PARAM).equals('1') == true);
            incAD  = (System.currentPageReference().getParameters().get(INCLUDE_ADOPTIONS_PARAM).equals('1') == true);
            incADC = (System.currentPageReference().getParameters().get(INCLUDE_CLOSED_ADOPTIONS_PARAM).equals('1') == true);
            incGI  = (System.currentPageReference().getParameters().get(INCLUDE_GRATIS_LINE_ITEMS_PARAM).equals('1') == true);
            incPT  = (System.currentPageReference().getParameters().get(INCLUDE_PARTNERS_PARAM).equals('1') == true);
            
            associatedContacts  = new List<Positions_Held__c>();  
            booksUsed           = new List<Books_Used__c>();          
            bu_startDate        = new Task(ActivityDate = createDateFromString(System.currentPageReference().getParameters().get(BOOKS_USED_START_DATE_PARAM)));
            bu_endDate          = new Task(ActivityDate = createDateFromString(System.currentPageReference().getParameters().get(BOOKS_USED_END_DATE_PARAM)));
            
            openActivities      = new List<Activity>(); 
            oa_startDate        = new Task(ActivityDate = createDateFromString(System.currentPageReference().getParameters().get(OPEN_ACTIVITIES_START_DATE_PARAM)));
            oa_endDate          = new Task(ActivityDate = createDateFromString(System.currentPageReference().getParameters().get(OPEN_ACTIVITIES_END_DATE_PARAM)));
            
            adoptions           = new List<Opportunity>();
            gratisLineItems     = new List<Gratis_Line_Items__c>();
            gi_startDate        = new Task(ActivityDate = createDateFromString(System.currentPageReference().getParameters().get(GRATIS_LINE_ITEMS_START_DATE_PARAM)));
            gi_endDate          = new Task(ActivityDate = createDateFromString(System.currentPageReference().getParameters().get(GRATIS_LINE_ITEMS_END_DATE_PARAM)));
            
            // System.debug('user::[' + UserInfo.getUserId() +']');
            // partners            = new List<Partner>(); 
            activities          = new List<Activity>();
            ah_startDate        = new Task(ActivityDate = createDateFromString(System.currentPageReference().getParameters().get(ACTIVITY_HISTORY_START_DATE_PARAM)));
            ah_endDate          = new Task(ActivityDate = createDateFromString(System.currentPageReference().getParameters().get(ACTIVITY_HISTORY_END_DATE_PARAM)));
                                                             
            if (instituteId != null) {
                // account / institution
                account = [SELECT Id, Name, Phone, Website, Owner.Name, Target_Institution__c, Territory__r.Name, Other_Phone__c, Fax, Main_Email__c, 
                                  ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, School_Type_1__c, School_Type_2__c,
                                  School_Status__c, Local_School_Type__c, Exams_Offered__c, Notes__c, NumberOfEmployees, Courses_Interests__c, Parent.Name, Data_Quality_Description__c                                    
                           FROM Account
                           WHERE Id =: instituteId];
            }
            
            RefreshPreview();            
            
        } catch (Exception ex) {
            System.debug('EXCEPTION::PrintableViewCon[\n'+ex.getMessage()+'\n]\n');
        }       
    }
     
    public String getPageTitle() {
        return 'Printable View';
    }
        
    public Account getAccount() {
        return account;
    }
    
    public PageReference cancel() {
        return new PageReference(cancelURL);
    }
    
    public void RefreshPreview() {
        
        try {
            if (instituteId != null) {
                String sql = '';
                String sqlInner = '';
                
                // associated contacts
                if (incAC == true) {
                    sql  = 'SELECT p.Name, p.Contact__r.Name, p.Institution__r.Name, p.Job_Type__c, p.Main_Contact__c, p.Inactive__c, ';
                    sql += ' p.Contact__r.HomePhone, p.Contact__r.Phone, p.Contact__r.MobilePhone, p.Contact__r.Email, p.Contact__r.Do_Not_Email__c, p.Contact__r.MailingStreet, p.Contact__r.OTC__c ';
                    sql += 'FROM Positions_Held__c p ';
                    sql += 'WHERE (p.Institution__r.Id = \'' + instituteId + '\') AND (p.Inactive__c = false) ';
                    associatedContacts = Database.query(sql);
                }
                
                // books used
                if (incBU == true) {
                    sql  = 'SELECT b.Book_Used__r.Name, b.Adoption_Date__c, b.Publisher__c, b.Number_used__c, b.Price__c, b.Status__c, b.Teacher__r.Name, b.Renewal_Date__c ';
                    sql += 'FROM Books_Used__c b ';
                    sql += 'WHERE b.Institution__r.Id = \'' + instituteId + '\' ';
                    if (bu_startDate.activityDate != null) {
                        sql += ' AND (b.Adoption_Date__c >= '+formatDateToString(bu_startDate.activityDate)+') ';                   
                    }
                    if (bu_endDate.activityDate != null) {
                        sql += ' AND (b.Adoption_Date__c <= '+formatDateToString(bu_endDate.activityDate)+') ';
                    }                
                    sql += ' ORDER BY b.Renewal_Date__c DESC';                
                    booksUsed = Database.query(sql);                    
                }
                
                // open activities
                if (incOA == true) {
                    openActivities.clear();
                                        
                    List<Task> task = new List<Task>(); 
                    sql  = 'SELECT t.What.Name, t.Who.Name, t.WhoId, t.Subject, t.Status, t.Priority, t.Owner.Name, t.OwnerId, t.IsClosed, t.Id, t.Description, t.ActivityDate, t.Account.Name, t.AccountId, ';
                    sql += ' t.Task_Notes__c, t.Purpose__c ';
                    sql += 'FROM Task t ';
                    sql += 'WHERE (t.AccountId = \'' + instituteId + '\') AND (t.Status != \'Completed\') ';
                    if (oa_startDate.activityDate != null) {
                        sql += ' AND (t.ActivityDate >= '+formatDateToString(oa_startDate.activityDate)+') ';                   
                    }
                    if (oa_endDate.activityDate != null) {
                        sql += ' AND (t.ActivityDate <= '+formatDateToString(oa_endDate.activityDate)+') ';
                    }    
                       sql += ' ORDER BY t.ActivityDate DESC'; 
                    sql += ' ALL ROWS';
                    task = Database.query(sql);                 
                    if ((task != null) && (task.size() > 0)) {
                        for (Task tk : task) {
                            Activity at = new Activity(tk.Subject, tk.ActivityDate, tk.Who.Name, tk.What.Name, tk.Status, tk.Task_Notes__c, tk.Purpose__c, tk.Owner.Name, '', true);
                            openActivities.add(at);
                        }
                    }
                    
                    // activity history - event
                    List<Event> event = new List<Event>(); 
                    sql  = 'SELECT e.Subject, e.ActivityDate, e.Who.Name, e.Account.Name, e.Task_Notes__c, e.Purpose__c, e.Owner.Name, e.What.Name, e.Needs_Objectives__c ';
                    sql += 'FROM Event e ';
                    sql += 'WHERE (e.AccountId = \'' + instituteId + '\') ';
                    if (oa_startDate.activityDate != null) {
                        sql += ' AND ((e.ActivityDate >= '+formatDateToString(oa_startDate.activityDate)+') AND (e.ActivityDate > '+formatDateToString(System.today())+')) ';                   
                    }
                    if (oa_endDate.activityDate != null) {
                        sql += ' AND (e.ActivityDate <= '+formatDateToString(oa_endDate.activityDate)+')  ';  
                    }
                    if ((oa_startDate.activityDate == null) && (oa_endDate.activityDate == null)) {                        
                        // Standard Salesforce functionality moves Events to activity history when the 
                        // Event data is before today so check event date > todays date 
                        sql += ' AND (e.ActivityDate > '+formatDateToString(System.today())+') ';  
                    }    
                     sql += ' ORDER BY e.ActivityDate DESC'; 
                    sql += ' ALL ROWS';                
                    event = Database.query(sql);
                    if ((event != null) && (event.size() > 0)) {
                        for (Event ev : event) {
                            Activity at = new Activity(ev.Subject, ev.ActivityDate, ev.Who.Name, ev.What.Name, '', ev.Task_Notes__c, ev.Purpose__c, ev.Owner.Name, ev.Needs_Objectives__c, false);
                            openActivities.add(at);
                        }
                    }                              
                }
                
                // activity history - task
                if (incAH == true) {
                    activities.clear();
                                        
                    List<Task> task = new List<Task>(); 
                    sql  = 'SELECT t.What.Name, t.Who.Name, t.WhoId, t.Subject, t.Status, t.Priority, t.Owner.Name, t.OwnerId, t.IsClosed, t.Id, t.Description, t.ActivityDate, t.Account.Name, t.AccountId, ';
                    sql += ' t.Task_Notes__c, t.Purpose__c ';
                    sql += 'FROM Task t ';
                    sql += 'WHERE (t.AccountId = \'' + instituteId + '\') AND (t.Status = \'Completed\') ';
                    if (ah_startDate.activityDate != null) {
                        sql += ' AND (t.ActivityDate >= '+formatDateToString(ah_startDate.activityDate)+') ';                   
                    }
                    if (ah_endDate.activityDate != null) {
                        sql += ' AND (t.ActivityDate <= '+formatDateToString(ah_endDate.activityDate)+') ';
                    }    
                     sql += ' ORDER BY t.ActivityDate DESC'; 
                    sql += ' ALL ROWS';  
                    task = Database.query(sql);                 
                    if ((task != null) && (task.size() > 0)) {
                        for (Task tk : task) {
                            Activity at = new Activity(tk.Subject, tk.ActivityDate, tk.Who.Name, tk.What.Name, tk.Status, tk.Task_Notes__c, tk.Purpose__c, tk.Owner.Name, '', true);
                            activities.add(at);
                        }
                    }
                    
                    // activity history - event
                    List<Event> event = new List<Event>(); 
                    sql  = 'SELECT e.Subject, e.ActivityDate, e.Who.Name, e.Account.Name, e.Task_Notes__c, e.Purpose__c, e.Owner.Name, e.What.Name, e.Needs_Objectives__c ';
                    sql += 'FROM Event e ';
                    sql += 'WHERE (e.AccountId = \'' + instituteId + '\') ';
                    if (ah_startDate.activityDate != null) {
                        sql += ' AND (e.ActivityDate >= '+formatDateToString(ah_startDate.activityDate)+') ';                   
                    }
                    if (ah_endDate.activityDate != null) {
                        sql += ' AND (e.ActivityDate <= '+formatDateToString(ah_endDate.activityDate)+') ';
                    }
                    if ((ah_startDate.activityDate == null) && (ah_endDate.activityDate == null)) {                        
                        // Standard Salesforce functionality moves Events to activity history when the 
                        // Event data is before today so check event date <= todays date
                        sql += ' AND (e.ActivityDate <= '+formatDateToString(System.today())+') ';  
                    }
                     sql += ' ORDER BY e.ActivityDate DESC'; 
                    sql += ' ALL ROWS';     
                    event = Database.query(sql);
                    if ((event != null) && (event.size() > 0)) {
                        for (Event ev : event) {
                            Activity at = new Activity(ev.Subject, ev.ActivityDate, ev.Who.Name, ev.What.Name, '', ev.Task_Notes__c, ev.Purpose__c, ev.Owner.Name, ev.Needs_Objectives__c, false);
                            activities.add(at);
                        }
                    }
                }
                
                // adoptions
                if (incAD == true) {
                    sql  = 'SELECT o.Name, o.StageName, o.Probability, o.Amount, o.CloseDate, o.CurrencyIsoCode ';
                    sql += 'FROM Opportunity o ';
                    sql += 'WHERE (o.AccountId = \'' + instituteId + '\') ';
                    // default is to exclude closed opportunities
                    if (incADC == false) {
                        sql += ' AND (o.IsClosed = false) ';                        
                    }               
                    sql += 'ORDER BY o.CloseDate DESC';
                    adoptions = Database.query(sql);
                }
                
                // gratis line items
                if (incGI == true) {
                    gratisLineItems.clear();
                                    
                    List<Gratis_Item__c> gratisItem = new List<Gratis_Item__c>();
                    sqlInner = 'SELECT Related_Gratis_Item__r.Contact__r.Name, Related_Gratis_Item__r.Institution__r.Name, Drop_or_Order__c, Status__c, Quantity__c, Date_Sent__c, Title__r.Name FROM Gratis_Line_Items__r ';
                    if (gi_startDate.activityDate != null) {
                        sqlInner += ' WHERE (Date_Sent__c >= '+formatDateToString(gi_startDate.activityDate)+') ';                  
                    }               
                    if (gi_endDate.activityDate != null) {
                        // if the start date has been specified we need the 'AND'
                        if (gi_startDate.activityDate != null) {
                            sqlInner += ' AND ';
                        } else {
                            // no start date so prefix with 'WHERE'
                            sqlInner += ' WHERE ';          
                        }
                        sqlInner += ' (Date_Sent__c <= '+formatDateToString(gi_endDate.activityDate)+') ';
                        // sqlInner += ' ORDER BY Date_Sent__c DESC';  
                    }
                
                    sql  = 'SELECT g.Name, (' + sqlInner + ') ';
                    sql += 'FROM Gratis_Item__c g ';
                    sql += 'WHERE (g.Institution__c = \'' + instituteId + '\') ';    
                    sql += ' ORDER BY g.Date_Sent_Left__c DESC';            
                    gratisItem = Database.query(sql);
                    if ((gratisItem != null) && (gratisItem.size() > 0)) {                      
                        List<Gratis_Line_Items__c> gratisLineItem = null;
                        // iterate through the gratis items
                        for (Gratis_Item__c gi : gratisItem) {
                            if (gi.Gratis_Line_Items__r != null) {
                                gratisLineItem = gi.Gratis_Line_Items__r;
                                // iterate through the individual line item for the gratis item
                                for (Gratis_Line_Items__c lineItem : gratisLineItem) {
                                    gratisLineItems.add(lineItem);
                                }
                            }
                        }
                    }
                }
                
                // partners            
                if (incPT == true) {                                      
                    // partners.clear();                   
                    // List<Account> account = new List<Account>();
                    // sql  = 'SELECT (SELECT Id, AccountToId, Role FROM AccountPartnersFrom) ';
                    // sql += 'FROM Account a ';
                    // sql += 'WHERE (a.Id = \'' + instituteId + '\') ';               
                    // account = Database.query(sql);
                    // if ((account != null) && (account.size() > 0)) { 
                    //     List<AccountPartner> accountPartner = new List<AccountPartner>();
                    //     Partner partner = null;                      
                    //     for (Account ac : account) { 
                    //         accountPartner = ac.AccountPartnersFrom;
                    //         for (AccountPartner ap : accountPartner) {
                    //            sql  = 'SELECT  p.Role, p.Opportunity.Name, p.OpportunityId, p.AccountTo.Name, p.AccountToId ';
                    //             sql += 'FROM Partner p ';
                    //             sql += 'WHERE (p.AccountToId = \'' + ap.AccountToId + '\') ';       
                    //             //partner = Database.query(sql); 
                    //             if (partner != null) {
                    //              partners.add(partner);
                    //          }
                    //         }
                    //     }                     
                    // }                    
                }       
            }            
        } catch (Exception ex) {
            // System.debug('EXCEPTION::RefreshPreview[\n'+ex.getMessage()+'\n]\n');
        }
    }
    
    public String getBackToDetails() {
        return retURL;
    }
    
    public String getPrintDetails() {
        String url  = '/apex/InstitutionPrint?Id='+System.currentPageReference().getParameters().get(INSTITUTE_URL_PARAM);
        if (incAC == true)  { url += '&incAC=1'; }  else { url += '&incAC=0'; }
        if (incBU == true)  { url += '&incBU=1'; }  else { url += '&incBU=0'; }
        if (incOA == true)  { url += '&incOA=1'; }  else { url += '&incOA=0'; }
        if (incAH == true)  { url += '&incAH=1'; }  else { url += '&incAH=0'; }
        if (incAD == true)  { url += '&incAD=1'; }  else { url += '&incAD=0'; }
        if (incADC == true) { url += '&incADC=1'; } else { url += '&incADC=0'; }
        if (incGI == true)  { url += '&incGI=1'; }  else { url += '&incGI=0'; }
        if (incPT == true)  { url += '&incPT=1'; }  else { url += '&incPT=0'; }
        
        try {
            if (bu_startDate.activityDate != null) { url += '&buds='+formatDateToString(bu_startDate.activityDate); } else { url += '&buds='; }
            if (bu_endDate.activityDate != null)   { url += '&bude='+formatDateToString(bu_endDate.activityDate); } else { url += '&bude='; } 
            if (gi_startDate.activityDate != null) { url += '&gids='+formatDateToString(gi_startDate.activityDate); } else { url += '&gids='; } 
            if (gi_endDate.activityDate != null)   { url += '&gide='+formatDateToString(gi_endDate.activityDate); } else { url += '&gide='; }
            if (ah_startDate.activityDate != null) { url += '&ahds='+formatDateToString(ah_startDate.activityDate); } else { url += '&ahds='; } 
            if (ah_endDate.activityDate != null)   { url += '&ahde='+formatDateToString(ah_endDate.activityDate); } else { url += '&ahde='; }
            if (oa_startDate.activityDate != null) { url += '&oads='+formatDateToString(oa_startDate.activityDate); } else { url += '&oads='; } 
            if (oa_endDate.activityDate != null)   { url += '&oade='+formatDateToString(oa_endDate.activityDate); } else { url += '&oade='; }       
        } catch (Exception ex) {
            System.debug('EXCEPTION::getPrintDetails[\n'+ex.getMessage()+'\n]\n');
        }
        
        return url;
    }
    
    public String getTargetInstitutionImageName() {
        String checkboxImage = '/img/checkbox_unchecked.gif';
        
        try {
            if (account.Target_Institution__c == true) {
                checkboxImage = '/img/checkbox_checked.gif';
            }
        } catch (Exception ex) {
        }
        
        return checkboxImage;
    }
    
    public List<Positions_Held__c> getAssociatedContacts() {
        return associatedContacts;
    }
    
    public List<Books_Used__c> getBooksUsed() {
        return booksUsed;
    }
    
    public List<Activity> getOpenActivities() {
        return openActivities;
    }
    
    public List<Opportunity> getAdoptions() {
        return adoptions;
    }
    
    public List<Gratis_Line_Items__c> getGratisLineItems() {
        return gratisLineItems;
    }
    
    public List<Partner> getPartners() {
        return null;
    //    return partners;
    }
    
    public List<Activity> getActivities() {
        return activities;
    }    
        
    public Task getBookUsedStartDate() {
        return bu_startDate;
    }
    
    public Task getBookUsedEndDate() {
        return bu_endDate;
    }
       
    public Task getOpenActivitiesStartDate() {
        return oa_startDate;
    }
    
    public Task getOpenActivitiesEndDate() {
        return oa_endDate;
    }
    
    public Task getGratisLineItemsStartDate() {
        return gi_startDate;
    }
    
    public Task getGratisLineItemsEndDate() {
        return gi_endDate;
    }
            
    public Task getActivityHistoryStartDate() {
        return ah_startDate;
    }
    
    public Task getActivityHistoryEndDate() {
        return ah_endDate;
    }
    
    private Date createDateFromString(String dt) {
        Date newDate = null;
        if ((dt != null) && (dt.length() > 0)) {
            newDate = date.valueOf(dt);
        }
        return newDate;
    }
    
    private String formatDateToString(date src) {
        datetime dtTmp = datetime.newInstance(src.year(), src.month(), src.day());
        return dtTmp.format('yyyy-MM-dd');
    }


    // Inner Classe(s)
    
    public class Activity {
        
        private String subject;
        private Boolean isTask;
        private Date activityDate;
        private String name;
        private String relatedTo;
        private String status;
        private String notes;
        private String purpose;
        private String ownerName;
        private String needsObjectives;
        
        public String getSubject()          { return subject; }
        public Boolean getIsTask()          { return isTask; } 
        
        public Date getActivityDate()       { return activityDate; }
        public String getActivityDateToString() { 
            datetime dtTmp = datetime.newInstance(activityDate.year(), activityDate.month(), activityDate.day());
            return dtTmp.format('dd/MM/yyyy'); 
        }
        
        public String getName()             { return name; }
        public String getRelatedTo()        { return relatedTo; }
        public String getStatus()           { return status; }
        public String getNotes()            { return notes; }
        public String getPurpose()          { return purpose; }
        public String getOwnerName()        { return ownerName; }
        public String getNeedsObjectives()  { return needsObjectives; }     
                
        public Activity(String subject, Date activityDate, String name, String relatedTo, String status, String notes, String purpose, String ownerName, String needsObjectives, Boolean isTask) {
            this.subject         = subject;
            this.isTask          = isTask;
            this.activityDate    = activityDate;
            this.name            = name;
            this.relatedTo       = relatedTo;
            this.status          = status;
            this.notes           = notes;
            this.purpose         = purpose;
            this.ownerName       = ownerName;
            this.needsObjectives = needsObjectives;     
        }       
        
        public String getIsTaskImageName() {
            String checkboxImage = '/img/checkbox_unchecked.gif';
        
            try {
                if (isTask == true) {
                    checkboxImage = '/img/checkbox_checked.gif';
                }
            } catch (Exception ex) {
            }
            
            return checkboxImage;
        }       
    }   
}