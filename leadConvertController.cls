/*
        This is the controller for the Visual Force page leadConvertPage.
*/

public with sharing class leadConvertController extends PageControllerBase {
    
    // This is the lead that is to be converted
    public  Lead leadToConvert {get; set;}


    // Constructor for this controller
    public leadConvertController(ApexPages.StandardController stdController) {
        
        //get the ID to query for the Lead fields
        Id leadId = stdController.getId();
        
        leadToConvert = [SELECT Id, Status, OwnerId, Name, Company, Street, City, FirstName, LastName, Email, Contact_by_Email__c FROM Lead WHERE Id = :leadId];
    }

        /*
        These are instances of the components' controllers which this class will access.
        
        If you add new custom components, add an instance of the class here
        */
    public leadConvertCoreComponentController myComponentController { get; set; }

        
        /*
                These are the set methods which override the methods in PageControllerBase. 
                These methods will be called by the ComponentControllerBase class.
                If you add new custom components, a new overridden set method must be added here.
        */
    public override void setComponentController(ComponentControllerBase compController) {
        
        myComponentController = (leadConvertCoreComponentController)compController;
    
    }

        /*
                These are the get methods which override the methods in PageControllerBase.
                
                If you add new custom components, a new overridden get method must be added here.
        */
    public override ComponentControllerBase getMyComponentController() {

        return myComponentController;

    }

    
    // This method is called when the user clicks the Convert button on the VF Page
    public PageReference convertLead() {
                
        // This is the lead convert object that will convert the lead 
        Database.LeadConvert leadConvert = new database.LeadConvert();
       
        
        // if Lead Status is not entered show an error  
        if (myComponentController != null && myComponentController.leadConvert.Status == 'NONE'){
            
            PrintError('Please select a Lead Status.');
            return null;
            
        } 
        
        //set lead ID
        leadConvert.setLeadId(leadToConvert.Id);    
        
        //if the main lead convert component is not set then return
        if (myComponentController == NULL) return null;
        
        //if the Account is not set, then show an error
        if (myComponentController.selectedAccount == 'NONE')
        {
            PrintError('Please select an Account.');
            return null;
            
        }
           Boolean createPosition = true;
         if (myComponentController.selectedContact != 'NONE')
        {
         leadConvert.setContactId(myComponentController.selectedContact);
         Positions_Held__c [] phs = [Select Id from Positions_Held__c where Contact__c =: myComponentController.selectedContact and Institution__c =:myComponentController.selectedAccount];
         
         leadConvert.setAccountId([Select AccountId from Contact where Id=:myComponentController.selectedContact][0].AccountId);
         
            if (phs != null && phs.size() > 0) createPosition = false;
            
            
        }
        
        // otherwise set the account id
       else if (myComponentController != NULL && myComponentController.selectedAccount != 'NEW') {
            leadConvert.setAccountId(myComponentController.selectedAccount);
        }
        
        //set the lead convert status
        leadConvert.setConvertedStatus('Converted');
        
        //set the variable to create or not create an opportunity
        leadConvert.setDoNotCreateOpportunity(true);
        
                
        //set the owner id
        leadConvert.setOwnerId([Select OwnerId from Account where Id =: myComponentController.selectedAccount][0].OwnerId);
        
        //set to overwrite lead source
        LeadConvert.overwriteLeadSource = true;
        
        //set whether to have a notification email
        leadConvert.setSendNotificationEmail(myComponentController.sendOwnerEmail);
        
        system.debug('leadConvert --> ' + leadConvert);
        
        //convert the lead
        Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
        
        // if the lead converting was a success then create a task
        if (leadConvertResult.success)
        {
        
        //create a position held for the contact
        if (createPosition)
        {
        Positions_held__c phCreate = new Positions_held__c();
        phCreate.Contact__c = leadConvertResult.getContactId();
        phCreate.Institution__c = myComponentController.selectedAccount;
        phCreate.Subject__c = myComponentController.Subject;
        phCreate.Job_Type__c= myComponentController.Job_Type;
        phCreate.Level_of_Influence__c= myComponentController.Level_of_Influence;
        database.insert(phCreate);
        }
        
        Contact c = new Contact();
        c.Id = leadConvertResult.getContactId();
        
        c.Territory__c = [Select Territory__c from Account where Id =: myComponentController.selectedAccount][0].Territory__c;
        c.Address_Preference__c = 'Institution';
        c.Do_Not_Email__c = leadToConvert.Contact_by_Email__c;
        
        if (myComponentController.overwriteEmailAddress)
        {
        c.Email = leadToConvert.Email;
        }
        
        database.update(c);
                             
           
            
            // redirect the user to the newly created Account
            PageReference pageRef = new PageReference('/' + leadConvertResult.getContactId());
            
            pageRef.setRedirect(true);
            
            return pageRef; 
        }
        else
        {

            //if converting was unsucessful, print the errors to the pageMessages and return null
            System.Debug(leadConvertResult.errors);

            PrintErrors(leadConvertResult.errors);
            
            return null;
        }
        
        return null;

    }
        
        //this method will take database errors and print them to teh PageMessages 
    public void PrintErrors(Database.Error[] errors)
    {
        for(Database.Error error : errors)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, error.message);
            ApexPages.addMessage(msg);
        }
    }
    
    //This method will put an error into the PageMessages on the page
    public void PrintError(string error) {
        ApexPages.Message msg = new 
            ApexPages.Message(ApexPages.Severity.ERROR, error);
        ApexPages.addMessage(msg);
    } 
    
    
}