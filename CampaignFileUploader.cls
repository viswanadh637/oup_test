public class CampaignFileUploader
{

    public PageReference cancel() {
        return new PageReference('/'+campaignId);
    }


    public String defaultStatus { get; set; }
    public String initialNumber { get; set; }
    public String numberUpdated { get; set; }
    public String numberCreated { get; set; }
    public List<Contact> ContactsToCreate {get; set ;}
        public List<Lead> LeadsToCreate {get; set ;}
    public List<selectOption> statusList { get; set; }
    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    String[] emails= new String[]{};
    List<CampaignMember> MembersToUpdate;
    List<CampaignMember> MembersToUpdateLeads;
    List<CampaignMember> MembersToCreate;
     public static final String CAMPAIGN_URL_PARAM = 'Id';
     public ID campaignId {get;set;}
     public boolean canProceed {get; set;}
     
     
     public CampaignFileUploader()
     {

                    statusList = new List<SelectOption>();
                    campaignId = System.currentPageReference().getParameters().get(CAMPAIGN_URL_PARAM);
                        if (campaignId != null)
                        {
                        canProceed = true;
                        
                      for (CampaignMemberStatus status : [select c.Label, c.IsDefault from CampaignMemberStatus c where campaignId = :campaignId])
                      {
                      if (status.IsDefault)
                      {
                      defaultStatus = status.Label;
                      }
                      statusList.Add(new SelectOption(status.Label,status.Label));
                      }
                       
                        }
                        else
                        {
                        canProceed = false;
                         ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Please use this page from a campaign screen.');
                         ApexPages.addMessage(errormsg);     
                        }
                   
     }
     
    public void ReadFile()
    {
    
        try 
        {
            nameFile = contentFile.toString().replace('\r', '');     
            emails = nameFile.split('\n');
                     
            initialNumber = string.valueof(emails.size());
            Id[] memberContactIds = new Id[]{}; 
            String[] memberContactEmails = new String[]{};
             Id[] memberLeadsIds = new Id[]{};
            MembersToCreate = new List<CampaignMember>();
            MembersToUpdate = [Select Id, Status, ContactId, Contact.Email from CampaignMember where Contact.Email IN :emails and campaignId =:campaignId];
        
           
            
                for (CampaignMember cm : MembersToUpdate)
                {
                    cm.Status = defaultStatus;
                    memberContactIds.Add(cm.ContactId);
                    memberContactEmails.Add(cm.Contact.Email); 
                }
                
                
                   
                
                ContactsToCreate = [Select Id, Email from Contact where Email IN :emails and Id NOT IN :memberContactIds];
                
                for (Contact ct: ContactsToCreate)
                {
                    MembersToCreate.Add(new CampaignMember(ContactId = ct.Id, CampaignId = campaignId, Status = defaultStatus ));
                    memberContactEmails.Add(ct.Email); 
                }
                
                
                MembersToUpdateLeads = [Select Id, Status, LeadId, Lead.Email  from CampaignMember where Lead.Email IN :emails and Lead.Email NOT IN :memberContactEmails and campaignId =:campaignId];
                numberUpdated = string.valueof(MembersToUpdate.size()+MembersToUpdateLeads.size());
                     
                for (CampaignMember cm : MembersToUpdateLeads)
                {
                    cm.Status = defaultStatus;
                    memberLeadsIds.Add(cm.LeadId);
                }
                
                
                LeadsToCreate = [Select Id from Lead where Email IN :emails and Id NOT IN :memberLeadsIds and Email NOT IN :memberContactEmails];
                
                for (Lead ct: LeadsToCreate )
                {
                    MembersToCreate.Add(new CampaignMember(LeadId = ct.Id, CampaignId = campaignId, Status = defaultStatus ));
                }
            
            numberCreated = string.valueof(MembersToCreate.size());
            database.update(MembersToUpdate);
            database.update(MembersToUpdateLeads);
            database.insert(MembersToCreate);
         }
         catch (Exception e)
         {
             canProceed = false;     
             ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage());
             ApexPages.addMessage(errormsg); 
      
         }   
       
    }
    
}