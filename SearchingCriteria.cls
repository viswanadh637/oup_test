public class SearchingCriteria
{
    //sub classes
    
    public interface IWhereClause
    {
        String builderWhereClause();
    }
    
    public abstract class BaseWhereClause implements IWhereClause
    {
        public String field{get;set;}
        public String operator{get;set;}
        public String value{get;set;} 
        
        public BaseWhereClause (String field, String operator, String value)
        {
            this.field = field;
            this.operator = operator;
            this.value = value;
        }       
          
        public virtual String builderWhereClause()
        {
           return '';
        }
    }
    
    public class EqualWhereClause extends BaseWhereClause
    {
        public EqualWhereClause(String field, String operator, String value){super(field, operator, value);}
        
        public override String builderWhereClause()
        {
           return field + ' = ' + '\'' + value + '\'';
        }
    }
    
    public class NotEqualWhereClause extends BaseWhereClause
    {
        public NotEqualWhereClause(String field, String operator, String value){super(field, operator, value);}
        
        public override String builderWhereClause()
        {
           return field + ' != ' + '\'' + value + '\'';
        }
    }
    
    public class StartWithWhereClause extends BaseWhereClause
    {
        public StartWithWhereClause(String field, String operator, String value){super(field, operator, value);}
        
        public override String builderWhereClause()
        {
           return field + ' LIKE ' + '\'' + value + '%\'';
        }
    }
    
    public class EndWithWhereClause extends BaseWhereClause
    {
        public EndWithWhereClause(String field, String operator, String value){super(field, operator, value);}
        
        public override String builderWhereClause()
        {
           return field + ' LIKE ' + '\'%' + value + '\'';
        }
    }   
    
    public class ContainsWhereClause extends BaseWhereClause
    {
        public ContainsWhereClause(String field, String operator, String value){super(field, operator, value);}
        
        public override String builderWhereClause()
        {
           return field + ' LIKE ' + '\'%' + value + '%\'';
        }
    }   
    
    public class NotContainsWhereClause extends BaseWhereClause
    {
        public NotContainsWhereClause(String field, String operator, String value){super(field, operator, value);}
        
        public override String builderWhereClause()
        {
           return '(NOT ' + field + ' LIKE ' + '\'%' + value + '%\')';
        }
    } 
 
    public class InWhereClause extends BaseWhereClause
    {
        public InWhereClause(String field, String operator, String value){super(field, operator, value);}
        
        public override String builderWhereClause()
        {
           return field + ' IN(' + value + ')';
        }
    } 
    
    public class NotInWhereClause extends BaseWhereClause
    {
        public NotInWhereClause(String field, String operator, String value){super(field, operator, value);}
        
        public override String builderWhereClause()
        {
           return field + ' NOT IN(' + value + ')';
        }
    }   
    
    
    public static IWhereClause getWhereClause(String field, String operator, String value)
    {
        if(operator == 'q'){return new EqualWhereClause(field,operator,value);}
        else if(operator == 'q'){return new EqualWhereClause(field,operator,value);}
        else if(operator == 'n'){return new NotEqualWhereClause(field,operator,value);}
        else if(operator == 's'){return new StartWithWhereClause(field,operator,value);}
        else if(operator == 'e'){return new EndWithWhereClause(field,operator,value);}
        else if(operator == 'c'){return new ContainsWhereClause(field,operator,value);}
        else if(operator == 'k'){return new NotContainsWhereClause(field,operator,value);}
        else if(operator == 'u'){return new InWhereClause(field,operator,value);}
        else if(operator == 'x'){return new NotInWhereClause(field,operator,value);}
        else{return null;}        
    }

       
    /////////////////////////////////////////////////////////////

    public Integer index{get;set;}
    public Boolean isShowLogic{get;set;}
    public String field{get;set;}
    public String operator{get;set;}
    public String value{get;set;}
    //logic disjunction for combine multiple searching criteria (combine with next criteria)
    public String logic{get;set;} 
    
    public string op   { get; set; }
    public string cp   { get; set; }
    
    public SearchingCriteria()
    {
        this(1,true,'', '', '','AND');//initital SearchingCriteria by set 'AND' as default logic and 1 ad default index
    }
    
    public SearchingCriteria(Integer index)
    {
        this(index,true,'', '', '','AND');//initital SearchingCriteria by set 'AND' as default logic
    }
    
    public SearchingCriteria(Integer index,Boolean isShowLogic)
    {
        this(index,isShowLogic,'', '', '','AND');//initital SearchingCriteria by set 'AND' as default logic
    }
    
    public SearchingCriteria(Integer index,Boolean isShowLogic,String field, String operator, String value)
    {
        this(index,isShowLogic,field, operator, value,'AND');//initital SearchingCriteria by set 'AND' as default logic
    }
    
    public SearchingCriteria(Integer index,Boolean isShowLogic,String field, String operator, String value,String logic)
    {
        this.index = index;
        this.isShowLogic= isShowLogic;
        this.field = field;
        this.operator = operator;
        this.value = value;
        this.logic = logic;        
    }  
                 
    public Boolean getShowLogic()
    {    
        return isShowLogic;
    }   
    
    public String getWhereClause()
    {
        IWhereClause builder = SearchingCriteria.getWhereClause(field,operator,value);
        String query = builder.builderWhereClause();              
        return query;
    }    

    public String formWhereClause(String query,Boolean isLast)
    {   
        if(!isEmpty())
        {
            String currentQuery  = getWhereClause(); 
            if(!isLast)
            {
                currentQuery = currentQuery + ' ' + logic;
            }       
            query = isEmptyString(query) ? currentQuery : query + ' ' + currentQuery;
        }
        else
        {    
            System.debug('Criteria is empty');
        }
        return query;
    }  
    
    public Boolean isEmpty()
    {
        return (SearchingCriteria.isEmptyString(field) || SearchingCriteria.isEmptyString(operator) || SearchingCriteria.isEmptyString(value));        
    }   
        
    public static String formWhereClause(List<SearchingCriteria> searchingCriterias)
    {
        return formWhereClause(searchingCriterias,'');
    }
    
    public static String formWhereClause(List<SearchingCriteria> searchingCriterias,String advFilter)
    {
		String whereClause = '';

		try
		{	    	
	         // to avoid validatedCriterias update the existing isLast status , pass in the searchingCriterias deep clone instance
	         List<SearchingCriteria> validatedCriterias = validatedCriterias(searchingCriterias);
	         Map<String,String> criteriaMap;	         
	         
	         if(!isEmptyString(advFilter))
	         {
	             criteriaMap = new Map<String,String>();
	             for(SearchingCriteria s : validatedCriterias)
	             {
	                 criteriaMap.put(String.valueOf(s.index),s.getWhereClause());
	             }      
	            
	             for(Integer i = 0; i < advFilter.length();i++)
	             {
	                String c = advFilter.substring(i,i+1);
	                if(criteriaMap.containsKey(c))
	                {
	                    whereClause = whereClause + criteriaMap.get(c);
	                }
	                else
	                {
	                    whereClause = whereClause + c;
	                }
	             }  
	         }
	         else
	         {
	         	// reset to default
	         	for (SearchingCriteria s : validatedCriterias) {
                    s.op = null;
	         		s.cp = null;
	            }
	         	
	         	if (validatedCriterias.size() >= 3) {
	         		// iterate validated criteria(s)
	         		for (integer i = 0; i < validatedCriterias.size(); i++) {
	         			// check next criteria (if available)
	         			if ((i + 1) < (validatedCriterias.size() - 1)) {
	         				// current logic and next differ
	         				if (validatedCriterias[i].logic != validatedCriterias[(i + 1)].logic) {
	         					validatedCriterias[i].op = ' (';
	         					// for now, just close parenthesis at final logic
                                validatedCriterias[(validatedCriterias.size() - 1)].cp = ')';
	         				}
	         			}
	         		}
	         	}
	         	
                integer index = 0;
                for(SearchingCriteria s : validatedCriterias)
                {
                    index ++;                    
system.debug('formWhereClause::10b whereClause:=' + whereClause);                    
                    whereClause = s.formWhereClause(whereClause,(index == validatedCriterias.size()));
system.debug('formWhereClause::11 s.op:=' + s.op + ', s.cp:='+s.cp);                    
                    whereClause = (s.op != null) ? whereClause + s.op : whereClause; 
                    whereClause = (s.cp != null) ? whereClause + s.cp : whereClause;                    
system.debug('formWhereClause::10a whereClause:=' + whereClause);
                } 

system.debug('formWhereClause::05 whereClause:=' + whereClause);                
                                 
            }	         
	         
	        if(!isEmptyString(whereClause))
	        {          
	          whereClause = ' WHERE (' + whereClause + ')';
	        }
	        
        } catch(Exception ex) {
    		System.debug('Exception::String formWhereClause() = ['+ex.getMessage()+']');
    	}
        
        return whereClause;
    }
    
    private static List<SearchingCriteria> validatedCriterias(List<SearchingCriteria> searchingCriterias)
    {
        List<SearchingCriteria> validatedCriterias = new List<SearchingCriteria>();
        for(SearchingCriteria s : searchingCriterias)
        {
            if(!s.isEmpty())
            {               
                validatedCriterias.add(s);             
            }
        }
        
        return validatedCriterias;
    }  
    
    public static Boolean isEmptyString(String s)
    {
        return (s == null || s.length()== 0);
    }   
}