@isTest
public class CreateGratisLineItemsControllerTest 
{
     //Test SelectableProduct  
    static testMethod void SelectableProductTest()
    {
         Product2 p = new Product2();
         CreateGratisLineItemsController.SelectableProduct sp = new CreateGratisLineItemsController.SelectableProduct(p);
         sp.isSelected = true;
         System.assert(true,sp.Product === p);
         System.assert(true,sp.isSelected); 
        
    } 
    
    //Test Search Criteria
    static testMethod void SearchingCriteriaTest()
    {        
        SearchingCriteria s0 = new SearchingCriteria();
        SearchingCriteria s1 = new SearchingCriteria(2,false,'name','e','John');
        SearchingCriteria s2 = new SearchingCriteria(3,false,'id', 's','ABC12','OR'); 
        SearchingCriteria s3 = new SearchingCriteria(4);
        SearchingCriteria s4 = new SearchingCriteria(5,true,'id','q','CBA321','AND');       
        
        List<SearchingCriteria> criterias = new SearchingCriteria[]{s0,s1,s2,s3,s4};
        
        String query = SearchingCriteria.formWhereClause(criterias);  
            
        System.assertEquals(' WHERE (name LIKE \'%John\' AND ( id LIKE \'ABC12%\' OR id = \'CBA321\'))',query);  
    }
    
    //test with not complete criterias
    static testMethod void SearchingCriteriaTest2()
    {        
        SearchingCriteria s0 = new SearchingCriteria();
        SearchingCriteria s1 = new SearchingCriteria(2,false,'name','e','John');
        SearchingCriteria s2 = new SearchingCriteria(3,false,'id', 's','ABC12','OR');         
        SearchingCriteria s4 = new SearchingCriteria(4,true,'id','q','CBA321','AND'); 
        SearchingCriteria s3 = new SearchingCriteria(5);      
        
        List<SearchingCriteria> criterias = new SearchingCriteria[]{s0,s1,s2,s3,s4};
        
        String query = SearchingCriteria.formWhereClause(criterias);  
            
        System.assertEquals(' WHERE (name LIKE \'%John\' AND ( id LIKE \'ABC12%\' OR id = \'CBA321\'))',query);  
    }
    
    //test with advance filter option
    static testMethod void SearchingCriteriaTest3()
    {        
        SearchingCriteria s0 = new SearchingCriteria();
        SearchingCriteria s1 = new SearchingCriteria(2,false,'name','e','John');
        SearchingCriteria s2 = new SearchingCriteria(3,false,'id', 's','ABC12','OR');         
        SearchingCriteria s4 = new SearchingCriteria(4,true,'id','q','CBA321','AND'); 
        SearchingCriteria s3 = new SearchingCriteria(5);      
        
        List<SearchingCriteria> criterias = new SearchingCriteria[]{s0,s1,s2,s3,s4};
        
        String query = SearchingCriteria.formWhereClause(criterias,'2 AND (3 OR 4)');  
            
        System.assertEquals(' WHERE (name LIKE \'%John\' AND (id LIKE \'ABC12%\' OR id = \'CBA321\'))',query);  
    }
    
    static testMethod void WhereClauseTest()
    {
        String f = 'Name';
        String v = 'ABC';
        
        System.assertEquals('Name = \'ABC\'',SearchingCriteria.getWhereClause(f,'q',v).builderWhereClause());
        System.assertEquals('Name != \'ABC\'',SearchingCriteria.getWhereClause(f,'n',v).builderWhereClause());
        System.assertEquals('Name LIKE \'ABC%\'',SearchingCriteria.getWhereClause(f,'s',v).builderWhereClause());
        System.assertEquals('Name LIKE \'%ABC\'',SearchingCriteria.getWhereClause(f,'e',v).builderWhereClause());
        System.assertEquals('Name LIKE \'%ABC%\'',SearchingCriteria.getWhereClause(f,'c',v).builderWhereClause());
        System.assertEquals('(NOT Name LIKE \'%ABC%\')',SearchingCriteria.getWhereClause(f,'k',v).builderWhereClause());
        System.assertEquals('Name IN(ABC)',SearchingCriteria.getWhereClause(f,'u',v).builderWhereClause());
        System.assertEquals('Name NOT IN(ABC)',SearchingCriteria.getWhereClause(f,'x',v).builderWhereClause());
    }  
    
     //Test Controller
    public static testMethod void testInitialControllerWithoutParameter() {
        CreateGratisLineItemsController controller = new CreateGratisLineItemsController();
        System.assert(!controller.getIsInitSuccess());
    }
     
    public static testMethod void testDrop() {
        Test.setCurrentPageReference(Page.gratislineitems2);
        Test.startTest();
        testWithType('Drop');
        Test.stopTest();
    }
    public static testMethod void testOrder() {
        Test.setCurrentPageReference(Page.gratislineitems2);
        Test.startTest();
        testWithType('Order');
        Test.stopTest();        
    }  
    public static testMethod void testInitlSearchingCriterias() {
        CreateGratisLineItemsController controller = new CreateGratisLineItemsController();   
        List<SearchingCriteria> criterias = controller.getSearchingCriterias();
        controller.getPriceBooks();
        
        System.assertEquals(5,criterias.size());//make sure that's 5 empty criterias loaded
        System.assertEquals('Name',criterias.get(0).field);//make sure first criteria default to book title field
        System.assertEquals('Name',criterias.get(1).field);//make sure first criteria default to ISBN field  
    }  
    public static testMethod void testGetFieldOptions() {
       CreateGratisLineItemsController controller = new CreateGratisLineItemsController();
       List<SelectOption> fields = controller.getFields();
       System.assert(fields.size() > 0);//make sure load atleast one value
    }
    
    public static testMethod void getRenderSearchResultsTest() {
        CreateGratisLineItemsController controller = new CreateGratisLineItemsController();
        Boolean status = controller.getRenderSearchResults();
        System.assert(status == false);
    }
    
    public static testMethod void getIsRTGratisItemTest() {
        CreateGratisLineItemsController controller = new CreateGratisLineItemsController();
        Boolean status = controller.getIsRTGratisItem();
        System.assert(status == false);
    }
    
    public static testMethod void getRecordTypeStatusItemsTest() {
        CreateGratisLineItemsController controller = new CreateGratisLineItemsController();
        List<SelectOption> rtStatusOptions = controller.getRecordTypeStatusItems();
        System.assert(rtStatusOptions != null);
    }
    
    public static testMethod void saveAndNewTest() {
        CreateGratisLineItemsController controller = new CreateGratisLineItemsController();
        controller.saveAndNew();    
    }
    
    public static testMethod void removeSelectedProductTest() {
        CreateGratisLineItemsController ctrl = preparePage('Order');
        ctrl.removeSelectedProduct();        
    }
    
    public static testMethod void testNeg() {
        Gratis_Item__c grItem = createGratisItem('Order');
        // Add parameters to page URL
        System.currentPageReference().getParameters().put(CreateGratisLineItemsController.GRATIS_ITEM_ID_PARAM, grItem.Id);
        CreateGratisLineItemsController controller = new CreateGratisLineItemsController();
        Boolean status = controller.getIsDrop();
        status = controller.getIsOrder();
        
    }
   
   public static testMethod void cascadeStatusTest() {
   
       CreateGratisLineItemsController ctrl = preparePage('Drop'); 
        ctrl.resetSearchingCriterias();//intial searching criteria        
        List<SearchingCriteria> criterias = ctrl.getSearchingCriterias();
         ctrl.selectedPricebook = 'None';
        ctrl.selectedPricebook = '01s200000005U0P';
        ctrl.Search();
        List<CreateGratisLineItemsController.SelectableProduct> lookupList  = ctrl.getLookupProducts();
               
       
        lookupList.get(0).isSelected = true;
        lookupList.get(1).isSelected = true;
        ctrl.addSelectedProducts();   
        
      PageReference page = ctrl.cascadeStatus();
      System.assert(page == null);
    }
    
    
    
    public static testMethod void cascadeDateSentLeftTest() {
     CreateGratisLineItemsController ctrl = preparePage('Drop'); 
        ctrl.resetSearchingCriterias();//intial searching criteria        
        List<SearchingCriteria> criterias = ctrl.getSearchingCriterias();
         ctrl.selectedPricebook = 'None';
             
        ctrl.Search();
        List<CreateGratisLineItemsController.SelectableProduct> lookupList  = ctrl.getLookupProducts();
               
        
        lookupList.get(0).isSelected = true;
        lookupList.get(1).isSelected = true;
        ctrl.addSelectedProducts();   
          PageReference page = ctrl.cascadeDateSentLeft();
      System.assert(page == null);
    }
public static testMethod void cascadeQuantityTest() {

      CreateGratisLineItemsController ctrl = preparePage('Drop'); 
        ctrl.resetSearchingCriterias();//intial searching criteria        
        List<SearchingCriteria> criterias = ctrl.getSearchingCriterias();
       ctrl.selectedPricebook = 'None';
             
        ctrl.Search();
        List<CreateGratisLineItemsController.SelectableProduct> lookupList  = ctrl.getLookupProducts();
               
        
        lookupList.get(0).isSelected = true;
        lookupList.get(1).isSelected = true;
        ctrl.addSelectedProducts();   
          PageReference page = ctrl.cascadeQuantity();
      System.assert(page == null);
    }

    /////////////// Data Preparation ////////////////////
    private static CreateGratisLineItemsController preparePage(String typeStr) {
        Gratis_Item__c grItem = createGratisItem(typeStr);
        system.debug(grItem.Id);
        // Add parameters to page URL
        System.currentPageReference().getParameters().put(CreateGratisLineItemsController.GRATIS_ITEM_ID_PARAM, grItem.Id);
        String lineItemRT = getGratisLineItemRecordTypeId(typeStr);
        System.currentPageReference().getParameters().put(grItem.RecordTypeId, lineItemRT);
        
        //initiate controller
        CreateGratisLineItemsController controller = new CreateGratisLineItemsController();
        System.assert(controller.getIsInitSuccess(), 'Failed to initialise controller. Most likely query string parameters problem');
        System.assert(typeStr == 'Drop'? controller.getIsDrop():!controller.getIsDrop(), 'Incorrect Drop/Order type calculated');
        system.debug(controller);    
        return controller;
    }
    public static Gratis_Item__c createGratisItem(String typeStr) {
        Gratis_Item__c item = new Gratis_Item__c();
        item.Shipping_Method__c = 'UPS Ground';
        item.Address_Preference__c = 'Contact Home';
        
        ID rtId = getGratisItemRecordTypeId(typeStr);
        System.assert(rtId != null, 'Failed to identify available RecordType by name using substring"' + typeStr + '"');
        item.RecordTypeId = rtId;
        item.Institution__c = getInstitution().id;
        item.Contact__c = getContact().id;        
        database.insert(item);
        
        return item;        
    }
    
    public static Account getInstitution()
    {
        Account[] accounts = [select Id from Account limit 1];
        System.assert(accounts.size() >=1, 'Please create at least 1 Account(Insitution) for testing');
        return accounts[0];
    }
    
    public static Contact getContact() {
        Contact[] contacts = [select Id from Contact limit 1];
        System.assert(contacts.size() >=1, 'Please create at least 1 Contact for testing');
        return contacts[0];
    }
    
    private static Product2 getBookTitle() {
        Product2[] products = [select Id from Product2 limit 1];
        System.assert(products.size() >=1, 'Please create at least 1 Product(Book Title) for testing');
        return products[0];
    }
    
    private static ID getGratisItemRecordTypeId(String typeStr) {
        typeStr = typeStr.toLowerCase();
        final Schema.DescribeSObjectResult describeRes = Gratis_Item__c.sObjectType.getDescribe();
        final List<Schema.RecordTypeInfo> rtInfos = describeRes.getRecordTypeInfos();
        for (Schema.RecordTypeInfo rtypeInfo : rtInfos) {
            if (rtypeInfo.isAvailable() && rtypeInfo.getName().toLowerCase().contains(typeStr))
                return rtypeInfo.getRecordTypeId(); 
        }
        return null;
    }
    
    private static ID getGratisLineItemRecordTypeId(String typeStr) {
        typeStr = typeStr.toLowerCase();
        final Schema.DescribeSObjectResult describeRes = Gratis_Line_Items__c.sObjectType.getDescribe();
        final List<Schema.RecordTypeInfo> rtInfos = describeRes.getRecordTypeInfos();
        for (Schema.RecordTypeInfo rtypeInfo : rtInfos) {
            if (rtypeInfo.isAvailable() && rtypeInfo.getName().toLowerCase().contains(typeStr))
                return rtypeInfo.getRecordTypeId(); 
        }
        return null;
    }  
        
    public static void testWithType(String testType)
    {
        CreateGratisLineItemsController ctrl = preparePage(testType); 
        ctrl.resetSearchingCriterias();//intial searching criteria        
        List<SearchingCriteria> criterias = ctrl.getSearchingCriterias();
        System.assertEquals(criterias.size(),5);
        
        //test searching criterias default values        
        System.assertEquals(criterias.get(0).index,1);
        System.assertEquals(criterias.get(0).field,'Name');
        System.assertEquals(criterias.get(0).operator,'c');
        System.assertEquals(criterias.get(0).logic,'AND');
        System.assertEquals(criterias.get(0).value,'');
        
        System.assertEquals(criterias.get(1).index,2);
        System.assertEquals(criterias.get(1).field,'Name');
        System.assertEquals(criterias.get(1).operator,'c');
        System.assertEquals(criterias.get(1).value,'');          
             ctrl.selectedPricebook = 'None';
             ctrl.limitToMyStock = false;
        ctrl.Search();
        List<CreateGratisLineItemsController.SelectableProduct> lookupList  = ctrl.getLookupProducts();
                
        for(CreateGratisLineItemsController.SelectableProduct sp : lookupList )
        {
            System.assertEquals(sp.isSelected,false);//check default value
            sp.isSelected = true;
            System.assertEquals(sp.isSelected,true);//check modified value
            
            sp.isSelected = false;
            System.assertEquals(sp.isSelected,false);//check modified value again                        
        }  
        
        System.assert(lookupList.size() >= 1, 'Please create at least 1 Account(Insitution) for testing');
        lookupList.get(0).isSelected = true;
                       
        ctrl.addSelectedProducts();                
        List<CreateGratisLineItemsController.GratisLineItemWithBookTitle> selectedProduct = ctrl.getSelectedProducts();
       
        System.assertEquals(1 + CreateGratisLineItemsController.NO_OF_PRELOAD_ITEMS ,selectedProduct.size());// check the list size should be 1 only
        System.assertEquals(lookupList.get(0).product.id,selectedProduct.get(0).getGratisLineItem().Title__c );//make sure both object refer to the same instance
        
        ctrl.addSelectedProducts();//add the same selected items again
        System.assertEquals(1 + CreateGratisLineItemsController.NO_OF_PRELOAD_ITEMS,selectedProduct.size());// check the list size should be 1 only, adding duplicated item is not allow
                                 
        //new added item will appear at the end of the list
        Gratis_Line_Items__c selectedGratisLineItem = selectedProduct.get(0).getGratisLineItem();//get the first items, new item alway append at index 0
        selectedGratisLineItem.Quantity__c = 1;
                 selectedProduct.get(0).validate();
                 selectedProduct.get(0).needSave();
                 
        String relatedGratisItem = selectedGratisLineItem.Related_Gratis_Item__c;
        System.assert(selectedGratisLineItem.Drop_or_Order__c == 'Drop'? ctrl.getIsDrop():!ctrl.getIsDrop(), 'Incorrect Drop/Order type calculated');
        System.assert(selectedGratisLineItem.Status__c == 'Left with Institution' ? ctrl.getIsDrop():!ctrl.getIsDrop(), 'Incorrect Status calculated');
        System.assertEquals(selectedGratisLineItem.Quantity__c,1);//test initiall quantity = 1
        
        //get a sample ISBN
        Product2 p = [SELECT id,ISBN__c FROM product2 ORDER BY Id DESC limit 1];        
        System.assert(p!= null,'Please create atleast one product record for testing.');
        CreateGratisLineItemsController.GratisLineItemWithBookTitle firstPreLoadItem = ctrl.getSelectedProducts().get(1);//get the preload item
        firstPreLoadItem.ISBN = p.ISBN__c;
        firstPreLoadItem.getGratisLineItem().Status__c = 'Left with Institution';
        firstPreLoadItem.getGratisLineItem().Quantity__c = 99;       
        
        ctrl.save();
        ctrl.cancel();
        
        // String nextPage = ctrl.save().getUrl();
        
        List<Gratis_Line_Items__c> savedItems = [SELECT id,Title__c FROM Gratis_Line_Items__c WHERE Related_Gratis_Item__c = :relatedGratisItem];
        // System.assertEquals(2,savedItems.size());
        
        // System.debug(nextPage);
        // System.assert(nextPage.contains('/' + relatedGratisItem), 'Incorrect URL returned:' + nextPage);            
                
        // nextPage = ctrl.cancel().getUrl();
        // System.debug(nextPage);
        // System.assert(nextPage.contains('/' + relatedGratisItem), 'Incorrect URL returned:' + nextPage);
                
        //test retrieve save line items by gratis item 
    }
    
    private static void TestPreLoadItems(CreateGratisLineItemsController ctrl)
    {    
        List<CreateGratisLineItemsController.GratisLineItemWithBookTitle> gitems = ctrl.getSelectedProducts();
        for(CreateGratisLineItemsController.GratisLineItemWithBookTitle gitem : gitems)
        {
            if(gitem.getIsPreLoad())
            {
                Gratis_Line_Items__c lineItem = gitem.getGratisLineItem();                
                System.assert(lineItem != null);
                //check ISBN is empty, title is empty
                System.assertEquals(null,gItem.ISBN);
                System.assertEquals(null,gItem.getItemBookTitle());                
            }            
        }
    }
    


}