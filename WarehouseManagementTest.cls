@isTest(SeeAllData=true)
public class WarehouseManagementTest
{

//data factory
//create warehouse

private static Warehouse__c createWarehouse()
{
Warehouse__c w = new Warehouse__c();
w.Name = 'a';
w.City__c = 'City__c';
w.OUP_Country__c = 'Poland';
w.Postcode__c = '00-000';
w.Street__c ='Street__c';
w.Warehouse_Owner__c = UserInfo.getUserId();
Database.SaveResult sr = Database.Insert(w);



if (sr.isSuccess())
{
w = [Select Id from Warehouse__c where Id =: sr.Id][0];
}
else
{
 for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('fields that affected this error: ' + err.getFields());
        }
}

User u = [select id from User where email = 'simon.bewick@oup.com'][0];
Warehouse_User__c wu = new Warehouse_User__c();
wu.Warehouse__c = w.id;
wu.User__c = u.id;
 sr = Database.Insert(wu);
if (sr.isSuccess())
{
wu.id = sr.Id;
database.Update(wu);
}

return w;
}
 
//create lines with warehouse
 public static testMethod void testCreateGratisLinesWithWarehouse()
 {    
      test.startTest();
      Warehouse__c w = createWarehouse();
      CreateGratisLineItemsControllerTest.testWithType('Drop');
      w = [Select Id, Total_Quantity__c from Warehouse__c where id=:w.id LIMIT 1];
      system.debug('After test with create gratis line controller');
      system.debug(w.Total_Quantity__c);
      //create
      test.stopTest();
}
 public static testMethod void testTransfer()
 {    
      test.startTest();
      Warehouse_Document__c wd = createTransfer();
      
      Test.setCurrentPageReference(Page.WarehouseDocumentUpload);
       System.currentPageReference().getParameters().put(WarehouseDocumentUpload.DOCUMENT_URL_PARAM, wd.Id);
       WarehouseDocumentUpload controller = new WarehouseDocumentUpload();
       
       //testing cancel procedure
        //test create isbn
        Blob bodyBlob=Blob.valueOf('9780194767330,1');
        controller.contentFile=bodyBlob;
        controller.ReadFile();
      
          
      approveDocument(wd.Id);
      test.stopTest();
      }


//create-update lines with warehouse
 public static testMethod void testCreateUpdateGratisLinesWithWarehouse()
 {      
 
      test.startTest();
      Warehouse__c w = createWarehouse();
      Gratis_Item__c gi = CreateGratisLineItemsControllerTest.createGratisItem('Drop');
      addLines(gi);
      w = [Select Id, Total_Quantity__c from Warehouse__c where id=:w.id LIMIT 1];
      system.debug('After add lines');
      system.debug(w.Total_Quantity__c);
     system.assertequals(w.Total_Quantity__c, -20);
      //update
      List<Gratis_Line_Items__c> glis = [Select Id from Gratis_Line_Items__c where Related_Gratis_Item__c=:gi.Id];
      glis[0].Quantity__c = 4;
      glis[1].Quantity__c = 1;
            database.update(glis);
       w = [Select Id, Total_Quantity__c from Warehouse__c where id=:w.id LIMIT 1];
      system.debug('After update 4 1');
      system.debug(w.Total_Quantity__c);
system.assertequals(w.Total_Quantity__c, -21);
      test.stopTest();
 }
 
 //create-delete gi with warehouse
 public static testMethod void testCreateDeleteGratisItemWithWarehouse()
 {      
 
      test.startTest();
      Warehouse__c w = createWarehouse();
      Gratis_Item__c gi = CreateGratisLineItemsControllerTest.createGratisItem('Drop');
      addLines(gi);

      w = [Select Id, Total_Quantity__c from Warehouse__c where id=:w.id LIMIT 1];
      system.debug('After add lines');
      system.debug(w.Total_Quantity__c);
            system.assertequals(w.Total_Quantity__c, -20);
      //delete
       database.delete(gi);
       w = [Select Id, Total_Quantity__c from Warehouse__c where id=:w.id LIMIT 1];
      system.debug('After delete');
      system.debug(w.Total_Quantity__c);
      system.assertequals(w.Total_Quantity__c, 0);
      test.stopTest();
 }
 
 
 
 
 //create-delete lines with warehouse
 public static testMethod void testCreateMassGratisLinesWithWarehouse()
 {      
      Warehouse__c w = createWarehouse();
      Gratis_Item__c gi = CreateGratisLineItemsControllerTest.createGratisItem('Drop');
      test.startTest();
      addMassLines(gi);      
      w = [Select Id, Total_Quantity__c from Warehouse__c where id=:w.id LIMIT 1];
      system.debug('After mass add lines');
      system.debug(w.Total_Quantity__c);
      test.stopTest();
 }
 
  //create-delete lines with warehouse
 public static testMethod void testCreateDeleteGratisLinesWithWarehouseAfterApprove()
 {      
 
      test.startTest();
      Warehouse__c w = createWarehouse();
      Gratis_Item__c gi = CreateGratisLineItemsControllerTest.createGratisItem('Drop');
      addLines(gi);
      w = [Select Id, Total_Quantity__c from Warehouse__c where id=:w.id LIMIT 1];
      system.debug('After add lines');
      system.debug(w.Total_Quantity__c);
      Gratis_Item__c giCheck = [Select id, Warehouse_Document__c from Gratis_Item__c where id=:gi.id];
      approveDocument(giCheck.Warehouse_Document__c);
      List<Gratis_Line_Items__c> glis = [Select Id from Gratis_Line_Items__c where Related_Gratis_Item__c=:giCheck.Id];
      //delete    
      database.delete(glis);
      w = [Select Id, Total_Quantity__c from Warehouse__c where id=:w.id LIMIT 1];
      system.debug('After delete glis');
      system.debug(w.Total_Quantity__c);
      List<Warehouse_Document_Line__c> wdl = [Select Id from Warehouse_Document_Line__c where Document__c =:giCheck.Warehouse_Document__c];
      wdl[0].Quantity__c = 100;

      
      try
    {
database.update(wdl);
throw new MyException('An exception should have been thrown by the trigger but was not.'); // 1. If we get to this line it means an error was not added and the test class should throw an exception here. 2. MyException class extends Exception.
}
catch(Exception e)
{
Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot delete or edit line of an approved document') ? true : false;
System.AssertEquals(expectedExceptionThrown, true);
} 
w = [Select Id, Total_Quantity__c from Warehouse__c where id=:w.id LIMIT 1];
system.debug('After try update');
      system.debug(w.Total_Quantity__c);
            try
    {
database.delete(wdl);
throw new MyException('An exception should have been thrown by the trigger but was not.'); // 1. If we get to this line it means an error was not added and the test class should throw an exception here. 2. MyException class extends Exception.
}
catch(Exception e)
{
Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot delete or edit line of an approved document') ? true : false;
System.AssertEquals(expectedExceptionThrown, true);
} 

w = [Select Id, Total_Quantity__c from Warehouse__c where id=:w.id LIMIT 1];
system.debug('After try delete');
      system.debug(w.Total_Quantity__c);
      
      
           
      test.stopTest();
 }
 
 
 //create lines without warehouse
 public static testMethod void testCreateGratisLinesWithOutWarehouse()
 {

    
      test.startTest();
      CreateGratisLineItemsControllerTest.testWithType('Drop');
      //create
      test.stopTest();
}
//create-update lines without warehouse
 public static testMethod void testCreateUpdateGratisLinesWithoutWarehouse()
 {      
 
      test.startTest();
      Gratis_Item__c gi = CreateGratisLineItemsControllerTest.createGratisItem('Drop');
      addLines(gi);
      //update
      List<Gratis_Line_Items__c> glis = [Select Id from Gratis_Line_Items__c where Related_Gratis_Item__c=:gi.Id];
      glis[0].Quantity__c = 3;
      glis[1].Quantity__c = 1;
      database.update(glis);
      test.stopTest();
 }
 
 
 //create-delete lines without warehouse
 public static testMethod void testCreateDeleteGratisLinesWithoutWarehouse()
 {      
 
      test.startTest();
      Gratis_Item__c gi = CreateGratisLineItemsControllerTest.createGratisItem('Drop');
      addLines(gi);
      List<Gratis_Line_Items__c> glis = [Select Id from Gratis_Line_Items__c where Related_Gratis_Item__c=:gi.Id];
      //delete    
      database.delete(glis);
      test.stopTest();
 }
 
 
 
 public static void addLines(Gratis_Item__c gi)
 {
  List<Gratis_Line_Items__c> glis = new List<Gratis_Line_Items__c>();
  Product2[] plist = [SELECT id FROM product2 ORDER BY Id DESC LIMIT 10]; //mass test
  for (Product2 p:plist)
  {
  Gratis_Line_Items__c gli = new Gratis_Line_Items__c ();
  gli.Related_Gratis_Item__c = gi.Id;
  gli.Status__c = 'Left with Institution';       
  gli.Drop_or_Order__c = 'Drop'; 
  gli.Title__c = p.Id;
  gli.Quantity__c = 2;
  gli.RecordTypeId = '012200000000Wij';
  gli.Date_Sent_Left__c = Date.Today();
  glis.add(gli);
  }
  database.insert(glis);
          
 } 

 public static void addMassLines(Gratis_Item__c gi)
 {
  List<Gratis_Line_Items__c> glis = new List<Gratis_Line_Items__c>();
  Product2[] plist = [SELECT id FROM product2 ORDER BY Id DESC LIMIT 100]; //mass test
  for (Product2 p:plist)
  {
  Gratis_Line_Items__c gli = new Gratis_Line_Items__c ();
  gli.Related_Gratis_Item__c = gi.Id;
  gli.Status__c = 'Left with Institution';       
  gli.Drop_or_Order__c = 'Drop'; 
  gli.Title__c = p.Id;
  gli.Quantity__c = 2;
  gli.RecordTypeId = '012200000000Wij';
  gli.Date_Sent_Left__c = Date.Today();
  glis.add(gli);
  }
  database.insert(glis);
          
 } 


//approve Document
public static void approveDocument (ID id) {

 // create the new approval request to submit
 Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
 req.setProcessDefinitionNameOrId('Warehouse_Document_Approval');
 req.setSkipEntryCriteria(true);
 
 req.setComments('Automatically submitted for approval on creation.');
 req.setObjectId(id);
 // submit the approval request for processing
 Approval.ProcessResult result = Approval.process(req);
 
 
  //List<Id> newWorkItemIds = result.getNewWorkitemIds();
        
        // Instantiate the new ProcessWorkitemRequest object and populate it
        //Approval.ProcessWorkitemRequest req2 = 
        //    new Approval.ProcessWorkitemRequest();
       // req2.setComments('Approving request.');
       // req2.setAction('Approve');
        //req2.setNextApproverIds(new Id[] {'005200000010XWk'});
        
        // Use the ID from the newly created item to specify the item to be worked
       // req2.setWorkitemId(newWorkItemIds.get(0));
        
        // Submit the request for approval
        //Approval.ProcessResult result2 =  Approval.process(req2);
}
public class MyException extends Exception {}

//upload file


public static Warehouse_Document__c wd;
    public static testMethod void testWithErrorsInURL() 
    {
        Test.setCurrentPageReference(Page.WarehouseDocumentUpload);
        //test missing CAMPAIGN_URL_PARAM
        WarehouseDocumentUpload controller = new WarehouseDocumentUpload();
        //System.assert(controller.canProceed, 'Failed to identify that process can not be executed');
    }

   public static testMethod void testCancel()
   {
       wd = createStandardDocument();  
       test.startTest();
       //setting the campaign id
       Test.setCurrentPageReference(Page.WarehouseDocumentUpload);
       System.currentPageReference().getParameters().put(WarehouseDocumentUpload.DOCUMENT_URL_PARAM, wd.Id);
       WarehouseDocumentUpload controller = new WarehouseDocumentUpload();
       //testing cancel procedure
       controller.cancel();
       test.stopTest();
       database.delete(wd);  
   }
   
      public static testMethod void testStandardProcess() 
      {    
       wd = createStandardDocument();  
       test.startTest();
       //setting the campaign id
       Test.setCurrentPageReference(Page.WarehouseDocumentUpload);
       System.currentPageReference().getParameters().put(WarehouseDocumentUpload.DOCUMENT_URL_PARAM, wd.Id);
       WarehouseDocumentUpload controller = new WarehouseDocumentUpload();
       
       //testing cancel procedure
        //test create isbn
        Blob bodyBlob=Blob.valueOf('9780194767330,1');
        controller.contentFile=bodyBlob;
        controller.ReadFile();
      
        //test create no ISBN
        bodyBlob=Blob.valueOf('9788888888,1');
        controller.contentFile=bodyBlob;
        controller.ReadFile();
       
        test.stopTest();
        database.delete(wd);
      }
      
      
         public static testMethod void testDifferentCsvFilesProcess() 
      {    
       wd = createStandardDocument();  
       test.startTest();
       //setting the campaign id
       Test.setCurrentPageReference(Page.WarehouseDocumentUpload);
       System.currentPageReference().getParameters().put(WarehouseDocumentUpload.DOCUMENT_URL_PARAM, wd.Id);
       WarehouseDocumentUpload controller = new WarehouseDocumentUpload();
       
       //testing cancel procedure
        //test create isbn
        Blob bodyBlob=Blob.valueOf('9780194767330;1');
        controller.contentFile=bodyBlob;
        controller.ReadFile();
      
      bodyBlob=Blob.valueOf('"9780194767330";"1"');
        controller.contentFile=bodyBlob;
        controller.ReadFile();
        bodyBlob=Blob.valueOf('"9780194767330","1"');
        controller.contentFile=bodyBlob;
        controller.ReadFile();
        //test create no ISBN
        bodyBlob=Blob.valueOf('9788888888;1');
        controller.contentFile=bodyBlob;
        controller.ReadFile();
       
        test.stopTest();
        database.delete(wd);
      }
      
     public static testMethod void testWithError() 
      {    
       wd = createStandardDocument();  
       test.startTest();
       //setting the campaign id
       Test.setCurrentPageReference(Page.WarehouseDocumentUpload);
       System.currentPageReference().getParameters().put(WarehouseDocumentUpload.DOCUMENT_URL_PARAM, wd.Id);
       WarehouseDocumentUpload controller = new WarehouseDocumentUpload();
       
        //delete campaign in the middle of the process
        database.delete(wd);  
        Blob bodyBlob=Blob.valueOf('9780194767330,1');
        controller.contentFile=bodyBlob;
        controller.ReadFile();
        test.stopTest();
      }
    

        private static Warehouse_Document__c createStandardDocument() 
        {
          
Warehouse__c w = createWarehouse();
wd = new Warehouse_Document__c();
wd.Warehouse__c = w.Id;
wd.RecordTypeId =[select id from RecordType where DeveloperName = 'Receipt'][0].Id;
wd.Status__c = 'New';
wd.Date__c = Date.Today();
Database.SaveResult sr = Database.Insert(wd);

wd = [Select Id from Warehouse_Document__c where Id =: sr.Id][0];
return wd;
      }

private static Warehouse_Document__c createTransfer() 
        {
          
Warehouse__c w = createWarehouse();
wd = new Warehouse_Document__c();
wd.Warehouse__c = w.Id;
wd.RecordTypeId =[select id from RecordType where DeveloperName = 'Shipment'][0].Id;
wd.Type__c = 'Transfer - out';
wd.Status__c = 'New';
wd.Date__c = Date.Today();
wd.Transfer_Warehouse__c = w.Id;
Database.SaveResult sr = Database.Insert(wd);

wd = [Select Id from Warehouse_Document__c where Id =: sr.Id][0];
return wd;
      }     


}