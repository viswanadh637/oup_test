/* Controller for Batch Creation of GratisLineItems */
public class GratisLineItemsBatchCon {
    private static final Integer ITEMS_PER_PAGE = 2;
    public static final String GRATIS_ITEM_ID_PARAM = 'gratisItemId';
    public static final String ITEMS_PER_PAGE_PARAM = 'itemsPerPage';
    
    private final ID gratisItemId; //parent Gratis Item
    private Gratis_Item__c gratisItem;//parent Gratis Item
    private List<Gratis_Line_Items__c> lineItems;
    private Integer itemsPerPage = ITEMS_PER_PAGE;
    
    public GratisLineItemsBatchCon () {
        //get parent Gratis ID
        gratisItemId = System.currentPageReference().getParameters().get(GRATIS_ITEM_ID_PARAM);
        if (gratisItemId == null) {
            addErrorMessage('Undefined gratisItemId parameter');
            return; 
        }
        try {
            gratisItem = [select Id, RecordTypeId, RecordType.Name from Gratis_Item__c where id=:gratisItemId];
            if (getGratisLineItemRecordType() == null) {
                addErrorMessage('Undefined Default Record Type parameter. Gratis Item RT:' + getGratisRecordType());
                gratisItem = null;
                return; 
            }
                        
        } catch (Exception e) {
            //failed to load Gratis Item
            return;
        }
        
        //Items per page
        String itemsPerPageStr = System.currentPageReference().getParameters().get(ITEMS_PER_PAGE_PARAM);
        itemsPerPage = ITEMS_PER_PAGE;
        try {
        	itemsPerPage = Integer.valueOf(itemsPerPageStr);        
        } catch (Exception eIgnore){}
        //init line items
        lineItems = new List<Gratis_Line_Items__c>();
        for (Integer i=0; i < itemsPerPage; i++) {
            lineItems.add(createGratisLineItem());  
        }           
    }
    public Boolean getIsInitSuccess() {
        return gratisItem != null;      
    }
    public List<Gratis_Line_Items__c> getGratisLineItems() {
        return lineItems;
    }
    private Gratis_Line_Items__c createGratisLineItem() {
        final Gratis_Line_Items__c item = new Gratis_Line_Items__c(Related_Gratis_Item__c = gratisItemId, RecordTypeId=getGratisLineItemRecordType() );
        //init pick lists
        if (getIsDrop()) {
                item.Status__c = 'Left with Institution';       
                item.Drop_or_Order__c = 'Drop';
        } else {
                item.Status__c = 'Pending';
                item.Drop_or_Order__c = 'Order';
        }
        
        //Title__c, Quantity__c, RecordTypeId, Status__c, Drop_or_Order__c
        //Orders
        //UPS_Tracking_ID__c, Other_Tracking_ID__c, Order_Number__c
        return item;
    }
    public Boolean getIsDrop() {
        String gratisRTName = gratisItem.RecordType.Name;
        gratisRTName = gratisRTName.toLowerCase();
        return gratisRTName.contains('drop');
    }
    public Gratis_Line_Items__c getDummyGratisLineItem() {
        return lineItems.get(0);
    }
    public PageReference save() {
        if (!saveInternal())
                return null;
        
        PageReference gratisPage = new PageReference('/' + gratisItemId);
        gratisPage.setRedirect(true);
        return gratisPage;      
    }
    public PageReference cancel() {
        PageReference gratisPage = new PageReference('/' + gratisItemId);
        gratisPage.setRedirect(true);
        return gratisPage;      
    }
    public PageReference saveAndNew() {
        if (!saveInternal())
        	return null;
                
        PageReference gratisPage = new PageReference(Page.GratisLineItemBatchCreate.getUrl() + '?' + constructQueryString());
        gratisPage.setRedirect(true);
        return gratisPage;      
                
    }
    private Boolean saveInternal() {
        try {
                //make sure we do not have duplicates
                final Map<ID, Gratis_Line_Items__c> selectedBookIds = new Map<ID, Gratis_Line_Items__c>();
                for (Gratis_Line_Items__c item: lineItems) {
                        if (!needSave(item))
                                continue;
                        if (!validate(item))
                                return false;
                                
                        if (item.Title__c != null) {
                                if (!selectedBookIds.containsKey(item.Title__c))
                                        selectedBookIds.put(item.Title__c, item);
                                else {
                                        //duplicates on the page
                                        item.addError('Duplicate title on the page');
                                        return false;
                                }
                        }       
                }
                        if (selectedBookIds.size() >0) {
                        final List<Gratis_Line_Items__c> existingLineItems = [select Title__r.Name, Title__c 
                                                                              from Gratis_Line_Items__c 
                                                                              where Related_Gratis_Item__c =: gratisItemId and Title__c in: selectedBookIds.keySet()];
                        if (existingLineItems.size() >0) {
                                for (Gratis_Line_Items__c existingItem: existingLineItems) {
                                        Gratis_Line_Items__c duplicateItem = selectedBookIds.get(existingItem.Title__c);
                                        if (duplicateItem != null) {
                                                duplicateItem.addError('Duplicate title ' + existingItem.Title__r.Name);
                                        }               
                                }
                                return false;
                        }
                        insert selectedBookIds.values();
                        return true;
                        } 
        } catch (Exception e) {
        	System.currentPageReference().getParameters().put('error', 'noInsert');                 
        }
        return false;
    }
    private Boolean needSave(Gratis_Line_Items__c item) {
        System.debug('item.Title__c != null=' + (item.Title__c != null));
        System.debug('item.Quantity__c != null=' + (item.Quantity__c != null));
        return item.Title__c != null || item.Quantity__c != null;// || item.Status__c != null || item.Drop_or_Order__c != null;
    }
    private Boolean validate(Gratis_Line_Items__c item) {
        if (item.Title__c == null) {
                item.addError('Title is Required field');
                return false;
        }
        if (item.Quantity__c == null || item.Quantity__c <1) {
                item.addError('Quantity is Required field');
                return false;
        }
        if (item.Status__c == null) {
                item.addError('Status is Required field');
                return false;
        }
        if (item.Drop_or_Order__c == null) {
                item.addError('"Drop or Order" is Required field');
                return false;
        }
        return true;
    }
    
    private ID getGratisLineItemRecordType() {
        String gratisRecTypeId = gratisItem.RecordTypeId; 
        //check 18 characters key
        String lineItemRecTypeId = System.currentPageReference().getParameters().get(gratisRecTypeId);
        if (lineItemRecTypeId == null) {
        	//check 15 characters key
        	lineItemRecTypeId = System.currentPageReference().getParameters().get(getGratisRecordType());
        }
        System.debug('gratisRecTypeId=' + gratisRecTypeId);
        System.debug('lineItemRecTypeId=' + lineItemRecTypeId);
        return lineItemRecTypeId;
    }
    private String getGratisRecordType() {
        String gratisRecTypeId = gratisItem.RecordTypeId;
        gratisRecTypeId = gratisRecTypeId.substring(0,15);
        return gratisRecTypeId;
    }
    
    /* URL Query String for next Batch */
    private String constructQueryString() {
    	String queryStr = GRATIS_ITEM_ID_PARAM + '=' + gratisItemId + '&' + 
    					getGratisRecordType() + '=' + getGratisLineItemRecordType() + '&' +
    					ITEMS_PER_PAGE_PARAM + '=' + itemsPerPage;
    	return queryStr;	
    }
    private void addErrorMessage(String msg) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
    }
}