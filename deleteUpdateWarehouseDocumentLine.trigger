trigger deleteUpdateWarehouseDocumentLine on Warehouse_Document_Line__c (before update) {

//sorting ids - in this case it will allow to update stock for each owner.??? or document??? GRATIS ONLY
ID sortingId;
ID oldsortingId;
Set<Id> ids = new Set<Id>();
List<Warehouse_Document_Line__c> createList = new List<Warehouse_Document_Line__c>();
//end definitions

if (Trigger.IsUpdate)
{
    ids = Trigger.newMap.KeySet();
    List<Warehouse_Document_Line__c> wdllist = [SELECT Id, Document__r.Status__c FROM Warehouse_Document_Line__c WHERE id in :ids and Document__r.Status__c = 'Approved'];
    For (Warehouse_Document_Line__c wdl: wdllist )
    {   
            Warehouse_Document_Line__c actualRecord = Trigger.newMap.get(wdl.Id);
            actualRecord.addError('Cannot delete or edit line of an approved document');
    }
    //end iteration
}
//end isUpdate
}
//end trigger