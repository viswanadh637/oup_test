@isTest
private class GratisLineItemsBatchConTests {
 
    public static testMethod void testWithErrorsInURL() {
        Test.setCurrentPageReference(Page.GratisLineItemBatchCreate);
        //test missing GRATIS_ITEM_ID_PARAM
        GratisLineItemsBatchCon controller = new GratisLineItemsBatchCon();
        //test missing RecordTypeId
        Gratis_Item__c grItem = createGratisItem('drop');
        System.currentPageReference().getParameters().put(GratisLineItemsBatchCon.GRATIS_ITEM_ID_PARAM, grItem.Id);
        controller = new GratisLineItemsBatchCon(); 
    }
    public static testMethod void testDrop() {
        Test.setCurrentPageReference(Page.GratisLineItemBatchCreate);
        Test.startTest();
        testWithType('Drop');
        Test.stopTest();
    }
    public static testMethod void testOrder() {
        Test.setCurrentPageReference(Page.GratisLineItemBatchCreate);
        Test.startTest();
        testWithType('Order');
        Test.stopTest();        
    }
    
    public static void testWithType(String typeStr) {
        GratisLineItemsBatchCon controller = preparePage(typeStr);
        controller.getDummyGratisLineItem();
        String relatedGratisItem = controller.getGratisLineItems().get(0).Related_Gratis_Item__c;
        //String nextPage = controller.saveAndNew().getUrl();
        //System.assert(nextPage.contains(GratisLineItemsBatchCon.GRATIS_ITEM_ID_PARAM  + '='+ relatedGratisItem), 'Incorrect URL returned:' + nextPage);
        
        controller = preparePage('Drop');
        relatedGratisItem = controller.getGratisLineItems().get(0).Related_Gratis_Item__c;
        String nextPage = controller.cancel().getUrl();
        System.assert(nextPage.contains('/' + relatedGratisItem), 'Incorrect URL returned:' + nextPage);
        
        controller = preparePage('Drop');
        relatedGratisItem = controller.getGratisLineItems().get(0).Related_Gratis_Item__c;
        //nextPage = controller.save().getUrl();
        //System.assert(nextPage.contains('/' + relatedGratisItem), 'Incorrect URL returned:' + nextPage);            
        
    }
    
    /////////////// Data Preparation ////////////////////
    private static GratisLineItemsBatchCon preparePage(String typeStr) {
        Gratis_Item__c grItem = createGratisItem(typeStr);
        // Add parameters to page URL
        System.currentPageReference().getParameters().put(GratisLineItemsBatchCon.GRATIS_ITEM_ID_PARAM, grItem.Id);
        System.currentPageReference().getParameters().put(GratisLineItemsBatchCon.ITEMS_PER_PAGE_PARAM, '2');
        String lineItemRT = getGratisLineItemRecordTypeId(typeStr);
        System.currentPageReference().getParameters().put(grItem.RecordTypeId, lineItemRT);
        
        //initiate controller
        GratisLineItemsBatchCon controller = new GratisLineItemsBatchCon();
        System.assert(controller.getIsInitSuccess(), 'Failed to initialise controller. Most likely query string parameters problem');
        
        System.assert(typeStr == 'Drop'? controller.getIsDrop():!controller.getIsDrop(), 'Incorrect Drop/Order type calculated');
        Gratis_Line_Items__c testLineItem = controller.getGratisLineItems().get(0);
        testLineItem.Title__c = getBookTitle().id;
        testLineItem.Quantity__c = 10;
        
        return controller;
    }
    private static Gratis_Item__c createGratisItem(String typeStr) {
        Gratis_Item__c item = new Gratis_Item__c();
        item.Shipping_Method__c = 'UPS Ground';
        item.Address_Preference__c = 'Contact Home';
        
        ID rtId = getGratisItemRecordTypeId(typeStr);
        System.assert(rtId != null, 'Failed to identify available RecordType by name using substring"' + typeStr + '"');
        item.RecordTypeId = rtId;
        item.Institution__c = getInstitution().id;
        item.Contact__c = getContact().id;
        
        insert item;
        
        return item;        
    }
    
    
    private static Account getInstitution() {
        Account[] accounts = [select Id from Account limit 1];
        System.assert(accounts.size() >=1, 'Please create at least 1 Account(Insitution) for testing');
        return accounts[0];
    }
    private static Contact getContact() {
        Contact[] contacts = [select Id from Contact limit 1];
        System.assert(contacts.size() >=1, 'Please create at least 1 Contact for testing');
        return contacts[0];
    }
    private static Product2 getBookTitle() {
        Product2[] products = [select Id from Product2 limit 1];
        System.assert(products.size() >=1, 'Please create at least 1 Product(Book Title) for testing');
        return products[0];
    }
    
    private static ID getGratisItemRecordTypeId(String typeStr) {
        typeStr = typeStr.toLowerCase();
        final Schema.DescribeSObjectResult describeRes = Gratis_Item__c.sObjectType.getDescribe();
        final List<Schema.RecordTypeInfo> rtInfos = describeRes.getRecordTypeInfos();
        for (Schema.RecordTypeInfo rtypeInfo : rtInfos) {
            if (rtypeInfo.isAvailable() && rtypeInfo.getName().toLowerCase().contains(typeStr))
                return rtypeInfo.getRecordTypeId(); 
        }
        return null;
    }
    private static ID getGratisLineItemRecordTypeId(String typeStr) {
        typeStr = typeStr.toLowerCase();
        final Schema.DescribeSObjectResult describeRes = Gratis_Line_Items__c.sObjectType.getDescribe();
        final List<Schema.RecordTypeInfo> rtInfos = describeRes.getRecordTypeInfos();
        for (Schema.RecordTypeInfo rtypeInfo : rtInfos) {
            if (rtypeInfo.isAvailable() && rtypeInfo.getName().toLowerCase().contains(typeStr))
                return rtypeInfo.getRecordTypeId(); 
        }
        return null;
    }
    
}