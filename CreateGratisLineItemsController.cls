public class CreateGratisLineItemsController
{



    public List<SelectOption> getPricebooks() {
        
        pricebooks = new List<SelectOption>();
        pricebooks.Add(new SelectOption('None', 'None'));
        for (Pricebook2 p: [Select id, Name from Pricebook2 where IsActive = true ORDER BY Name])
        {
        pricebooks.Add(new SelectOption(p.Id, p.Name));
        
        }
        return pricebooks;
    }


    private List<SelectOption> pricebooks = new List<SelectOption>();

    public String selectedPricebook {get; set;}


 public class SelectableProduct
    {        
        public SelectableProduct(Product2 p)
        {
            product = p; 
            isSelected = false;
        } 
        
        public Product2 product{get;set;} 
        public Boolean isSelected{get;set;}  
    }   
     
    //View data that wrapped Gratis line items and few custom attritubes
    public class GratisLineItemWithBookTitle
    {
        private String bookTitle;//book title from selected book title product
        private Gratis_Line_Items__c gratisLineItem ;//gratis line item;
        private Boolean isPreLoad;
        private Boolean isError;
        private String errorMessage;
        private String lastReceived;
        private Boolean alreadyReceived;    
        //constructor  
        
        public GratisLineItemWithBookTitle(Product2 bookTitle,Gratis_Line_Items__c lineItem, Date dateGi, ID ContactId)
        {
            isError = false;
            if(bookTitle == null)
            {
                this.gratisLineItem = lineItem;  
                this.isPreLoad= true;                
                
            }
            else
            {
                
                this.bookTitle = bookTitle.Name;
                lineItem.Date_Sent_Left__c = dateGi;
                lineItem.Quantity__c = 1;
                this.ISBN = bookTitle.ISBN__c;
                lineItem.Title__c = bookTitle.id;  
                this.gratisLineItem = lineItem;  
                this.isPreLoad= false;
                  this.alreadyReceived = false;
                AggregateResult alreadyReceivedSum = [SELECT sum(Quantity__c) Quantity, max(Date_Sent_Left__c) DateSent FROM Gratis_Line_Items__c WHERE Title__c = : bookTitle.id and Related_Gratis_Item__r.Contact__c = :ContactId];
                
                if (alreadyReceivedSum.get('Quantity') != null)
                {
                    this.alreadyReceived = (((Decimal)alreadyReceivedSum.get('Quantity')) > 0);
                }
                
            }                      
        }  
        
        public Boolean needSave()
        {
            if(isPreLoad)
            {                    
                if(ISBN == null || ISBN.length() == 0)
                {
                    this.bookTitle = null;
                    isError = false;
                    errorMessage = '';
                    return false;
                }                
            }
            
            return true;             
        }
                
        public Boolean validate()
        {           
            Boolean result = true; 
            if(isPreLoad)
            {
                //make sure that's booktitle match with the ISBN input
                String arg = ISBN.trim();
                List<Product2> products = [SELECT id,name FROM Product2 WHERE ISBN__c = :arg];
                if(products.size() == 1)
                {
                    Product2 product = products.get(0);
                    bookTitle = product.name;    
                    gratisLineItem.Title__c = product.id;   
                    
                    isError = false;
                    errorMessage = '';            
                }
                else
                {
                    isError = true;
                    errorMessage = 'Invalid ISBN';
                    result = false;
                }
            } 
                 
            if (gratisLineItem.Title__c == null) 
            {
                    gratisLineItem.Title__c.addError('Title is Required field');
                    result = false;
            }
            if (gratisLineItem.Quantity__c == null || gratisLineItem.Quantity__c <1) {
                    gratisLineItem.Quantity__c.addError('Quantity is Required field');
                    result = false;
            }
            if (gratisLineItem.Date_Sent_Left__c == null) {
                    gratisLineItem.Date_Sent_Left__c.addError('Date Sent/Left is Required field');
                    result = false;
            }
            if (gratisLineItem.Status__c == null) {
                    gratisLineItem.Status__c.addError('Status is Required field');
                    result = false;
            }                        
            if(isPreLoad && !result)
            {
                bookTitle  = null;
            }
            return result;                      
        }  
        
            
        
        public String getItemBookTitle(){return bookTitle ;}        
        public Gratis_Line_Items__c getGratisLineItem(){return gratisLineItem ;}
        public Boolean getIsPreLoad(){return isPreLoad;}
        public Boolean getAlreadyReceived(){return alreadyReceived;}
        public String getLastReceived(){return lastReceived;}
        public String ISBN{set;get;}
        public String getErrorMessage(){return errorMessage;}
        public Boolean getIsError(){return isError;}
    }
    
    public static final String GRATIS_ITEM_ID_PARAM = 'gratisItemId';    
    public static final Integer NO_OF_PRELOAD_ITEMS = 10;
    public static final String RT_GRATIS_ITEM_ID = '012200000000s9uAAA'; // '012R0000000CnPTIA0';
    private List<SelectableProduct> lookupProducts;//boook titles product lookuo list
    private List<GratisLineItemWithBookTitle> selectedProducts;//selected cratis line items
    private List<SearchingCriteria> searchingCriterias;//search criterias 
    private String query = '';//query build by search criterias         
    private final ID gratisItemId; //parent Gratis Item
    public ID ContactId;
    private Gratis_Item__c gratisItem;//parent Gratis Item    
    public String advanceFilter{get;set;}//advance filter  
    public Boolean excludeInternationalTitles { get; set; }
        public Boolean limitToMyStock { get; set; }
        
    //constructor     
    public CreateGratisLineItemsController()
    {
    
    
    
           if (WarehouseManagement.userHasWarehouse(UserInfo.getUserId()))
           {
                   limitToMyStock = true;
                }
                else
                {
                    limitToMyStock = false;
                }
    
        // JRS 31/03/2011 - ELT-3-9
        excludeInternationalTitles = false;
        
        //get parent Gratis ID
        gratisItemId = System.currentPageReference().getParameters().get(GRATIS_ITEM_ID_PARAM);
        if (gratisItemId == null) {
            addErrorMessage('Undefined gratisItemId parameter');
            return; 
        }
      
        gratisItem = [select Id, RecordTypeId, RecordType.Name, Date_Sent_Left__c, Contact__c from Gratis_Item__c where id=:gratisItemId];
        ContactId = gratisItem.Contact__c;
        
        if (getGratisLineItemRecordType() == null)
        {
            addErrorMessage('Undefined Default Record Type parameter. Gratis Item RT:' + getGratisRecordType());
            gratisItem = null;
            return; 
        }
        
        if(selectedProducts  == null)
        {
            selectedProducts = new List<GratisLineItemWithBookTitle>();
            for(Integer i = 0; i < NO_OF_PRELOAD_ITEMS;i++)
            {
                selectedProducts.add(new GratisLineItemWithBookTitle(null,createGratisLineItem(), gratisItem.Date_Sent_Left__c, ContactId));
            }  
        }   
    }
    
    public Boolean getRenderSearchResults() {
        return (((lookupProducts == null) || (lookupProducts.size() == 0)) ? false : true);
    }
    
    //Get whether Init proccess succed or not
    public Boolean getIsInitSuccess() 
    {
        return gratisItem != null;      
    }

    //Get whether the target Gratis Item is Drop record type or not
    public Boolean getIsDrop() 
    {   
        if(gratisItem != null && gratisItem.RecordType != null)
        {
            String gratisRTName = gratisItem.RecordType.Name;
            gratisRTName = gratisRTName.toLowerCase();
            return gratisRTName.contains('drop');
        }
        else
        {
            return false;
        }
    }
    
    //Get whether the target Gratis Item is Order record type or not
    public Boolean getIsOrder() 
    {   
        if(gratisItem != null && gratisItem.RecordType != null)
        {
            String gratisRTName = gratisItem.RecordType.Name;
            gratisRTName = gratisRTName.toLowerCase();
            return gratisRTName.contains('order');
        }
        else
        {
            return false;
        }
    }
    
    // determine whether the target Gratis Item is 'Gratis Item' type
    public Boolean getIsRTGratisItem() {
        Boolean isRTGratisItem = false;
        
        if ((gratisItem != null) && (gratisItem.RecordType != null)) {
            String gratisItemId = gratisItem.RecordType.Id;
            isRTGratisItem = gratisItemId.equals(RT_GRATIS_ITEM_ID);
        }
        
        return isRTGratisItem;
    }
    
    // append status items that are record type dependent
    public List<SelectOption> getRecordTypeStatusItems() {     
        List<SelectOption> rtStatusOptions = new List<SelectOption>();
        rtStatusOptions.add(new SelectOption('Left with Institution', 'Left with Institution'));
        rtStatusOptions.add(new SelectOption('Sent by Sales Consultant', 'Sent by Sales Consultant'));
        
        if (getIsRTGratisItem() == true) {
            rtStatusOptions.add(new SelectOption('Sent by Warehouse', 'Sent by Warehouse'));
        }
        
        return rtStatusOptions;
    }
        
    //perform search by defiend searching criterias
    public void Search()
    {        
        List<SearchingCriteria> criterias = getSearchingCriterias();  
        String userOUPCountry = getUserOUPCountry();    
        String whereClause = SearchingCriteria.formWhereClause(criterias,advanceFilter); 
        System.debug('TestwhereClause : ' + whereClause.length());
        
        // JRS 31/03/2011 - ELT-3-9
        String marketInternational = '';
        String marketInternational_LIKE = '';
        if (excludeInternationalTitles == false) {
            marketInternational = 'Market__c = \'INTERNATIONAL\' OR ';
            marketInternational_LIKE = 'Market__c LIKE \'%INTERNATIONAL%\' OR ';
        }
        
        if(whereClause.length() > 0) 
        {
            whereClause  = whereClause + ' AND ((' + marketInternational + ' Market__c = \'' + userOUPCountry  + '\') AND Selectable__c = true) ';// LIMIT 100';
        }
        else
        {
            whereClause  =' WHERE (' + marketInternational_LIKE + ' Market__c = \'' + userOUPCountry  + '\') AND Selectable__c = true ';// LIMIT 100';
        }    
              
              
              if (limitToMyStock && WarehouseManagement.userHasWarehouse(UserInfo.getUserId()))
              {
              
                 
             Warehouse__c w = WarehouseManagement.getWarehouseByUser(UserInfo.getUserId());
              
              whereClause = whereClause +' AND Id IN (SELECT WD_book_title__c from Warehouse_Document_Line__c where Document__r.RecordType.Name = \'Stock\' and Document__r.Warehouse__c= \''+ w.id + '\' and quantity__c > 0)' ; 
           
              }
                           
              
              if (selectedPricebook != 'None')
              {
              whereClause = whereClause +' AND Id IN (SELECT Product2Id from PricebookEntry where Pricebook2Id = \''+selectedPricebook+'\' and  IsActive = true) ORDER BY Name  LIMIT 100' ; 
           
              }
              else
              {
              whereClause = whereClause +' ORDER BY Name LIMIT 100';
              }
              
              
              System.debug(whereClause);
              
        lookupProducts = new List<SelectableProduct>(); 
             
        List<Product2> products = new  List<Product2>(); 
        try
        {
        query = 'SELECT Series__c, Segment__c, Publisher__c,ProductCode,Name, Market__c, Level__c, Id, ISBN__c, Selectable__c FROM Product2' + whereClause ;
        System.debug('xfrs:'+query);
            products  = Database.query(query );
        }
        catch(Exception ex)
        {
         System.debug('xfrs:'+ex);
            products  = null;
        }      
        if(products != null && products.size() > 0)
        {
            for(Product2 p : products) 
            {          
                lookupProducts.add(new SelectableProduct(p));
            }
        }
    }
    
    //Reser searching criterias 
    public void resetSearchingCriterias()
    {
        searchingCriterias = new List<SearchingCriteria>();
        searchingCriterias.add(new SearchingCriteria(1,true,'Name','c','','AND'));
        searchingCriterias.add(new SearchingCriteria(2,true,'Name','c',''));
        searchingCriterias.add(new SearchingCriteria(3));
        searchingCriterias.add(new SearchingCriteria(4));
        searchingCriterias.add(new SearchingCriteria(5,false));    
        
        if(lookupProducts != null)
        {
            lookupProducts.clear();
        }
    }

    //Get Lookup Book titles product
    public List<SelectableProduct> getLookupProducts()
    { 
        if(lookupProducts == null)
        {
            lookupProducts = new List<SelectableProduct>();
        }
        
        return lookupProducts;
    }
    
    //Get searching criterias
    public List<SearchingCriteria> getSearchingCriterias()
    {
        if(searchingCriterias == null)
        {
           resetSearchingCriterias();           
        }
        
        return searchingCriterias;
    }
    
    //Get Query String for searching
    public String getQuery()
    {
        return query;
    }
    
    //Add selecting book titles to selected gratis line items list
    public void addSelectedProducts()
    {             
        for(SelectableProduct sp: getLookupProducts())
        {             
            if(sp.isSelected && !IsProductSelected(sp.product))
            {  
               if(selectedProducts == null || selectedProducts.size() == 0)
               {
                    getSelectedProducts().add(new GratisLineItemWithBookTitle(sp.product,createGratisLineItem(),gratisItem.Date_Sent_Left__c, ContactId));
               }
               else
               {
                    selectedProducts.add(0,new GratisLineItemWithBookTitle(sp.product,createGratisLineItem(), gratisItem.Date_Sent_Left__c, ContactId));
               }              
            }
        }
    }  
    
    public void removeSelectedProduct()
    {
        ID bookTitleID = System.currentPageReference().getParameters().get('title_id');
        
        for(Integer i = 0; i < selectedProducts.size();i++)
        {
            GratisLineItemWithBookTitle g = selectedProducts.get(i);
            if(g.getGratisLineItem().title__c == bookTitleID )
            {
                selectedProducts.remove(i);                
                return;
            }
        }
    }
    
    //Get selected Gratis Line Items
    public List<GratisLineItemWithBookTitle> getSelectedProducts()
    {
        if(selectedProducts  == null)
        {
            selectedProducts = new List<GratisLineItemWithBookTitle>();
        }
        return selectedProducts ;
    }    
    
    //Get searching fields in SelectOption object 
    public List<SelectOption> getFields()
    {
        Map<String, Schema.SObjectField> M = Schema.SObjectType.Product2.fields.getMap();
        //List<String> searchingFields = new List<String>(M.keySet());
        List<String> searchingFields = getSearchingFieldNames();
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        
        for(String s : searchingFields)
        {
            Schema.DescribeFieldResult F;
            F = M.get(s).getDescribe();
            options.add(new SelectOption(F.getName(),F.getLabel()));
        }            
   
        return options;       
    }     
    
    public PageReference save() {
        if (!saveInternal())
                return null;
        
        PageReference gratisPage = new PageReference('/' + gratisItemId);
        gratisPage.setRedirect(true);
        return gratisPage;      
    }
    public PageReference cancel() {
        PageReference gratisPage = new PageReference('/' + gratisItemId);
        gratisPage.setRedirect(true);
        return gratisPage;      
    }
   
    public PageReference saveAndNew() {
        if (!saveInternal())
            return null;
                
        PageReference gratisPage = new PageReference(Page.GratisLineItems2.getUrl() + '?' + constructQueryString());
        gratisPage.setRedirect(true);
        return gratisPage;                     
    }
    
    //Get List of searching field names
    private List<String> getSearchingFieldNames()
    {
        // JRS 31/03/2011 - ELT-3-4 [Add Component Type, Edition]
        return new String[]{ 'Name', 'ISBN__c', 'Level__c', 'Segment__c', 'Series__c', 'Component_Type__c', 'Edition__c', 'Subject__c' };
    }    
        
    //Get Gratis Line Item recird type
    private ID getGratisLineItemRecordType() {
        String gratisRecTypeId = gratisItem.RecordTypeId; 
        //check 18 characters key
        String lineItemRecTypeId = System.currentPageReference().getParameters().get(gratisRecTypeId);
        if (lineItemRecTypeId == null) {
            //check 15 characters key
            lineItemRecTypeId = System.currentPageReference().getParameters().get(getGratisRecordType());
        }
        System.debug('gratisRecTypeId=' + gratisRecTypeId);
        System.debug('lineItemRecTypeId=' + lineItemRecTypeId);
        return lineItemRecTypeId;
    }    
    
    //Check whether Product is selected or not
    private Boolean IsProductSelected(Product2 p)
    {
        for(GratisLineItemWithBookTitle g : getSelectedProducts())
        {
            if(g.GratisLineItem.Title__c == p.id ){return true;}
        }
        return false;
    }
    
    private Map<ID, Gratis_Line_Items__c> getValidatedLineItems()
    {
        Boolean isValid = true;
        final Map<ID, Gratis_Line_Items__c> newBookIds = new Map<ID, Gratis_Line_Items__c>();
        for (GratisLineItemWithBookTitle g : selectedProducts)
        {                        
            if (!g.needSave())
            {
                continue;
            }
            if (!g.validate())
            {
                isValid = false;
            }
            else
            {
                Gratis_Line_Items__c lineItem = g.getGratisLineItem();  
                if (!newBookIds.containsKey(lineItem.title__c))
                {                                   
                   newBookIds.put(lineItem.title__c, lineItem);                                   
                }
                else
                {
                    //duplicates on the page
                    lineItem.addError('Duplicate title on the page');
                    isValid = false;
                }
            }     
        }
        
        return isValid ? newBookIds : null;
    }
    
    //internal save 
    public Boolean saveInternal()
    {   
           try 
           {
                //make sure we do not have duplicates             
                Map<ID, Gratis_Line_Items__c> newBookIds = getValidatedLineItems();
                                              
                 if (newBookIds != null && newBookIds.size() >0)
                 {
                    final List<Gratis_Line_Items__c> existingLineItems = [select Title__r.Name, Title__c 
                                                                          from Gratis_Line_Items__c 
                                                                          where Related_Gratis_Item__c =: gratisItemId and Title__c in: newBookIds.keySet()];
                    if (existingLineItems.size() >0) 
                    {
                            for (Gratis_Line_Items__c existingItem: existingLineItems)
                            {
                                    Gratis_Line_Items__c duplicateItem = newBookIds.get(existingItem.Title__c);
                                    if (duplicateItem != null) 
                                    {
                                            duplicateItem.addError('Duplicate title ' + existingItem.Title__r.Name);
                                    }               
                            }
                            return false;
                    }
           
                    insert newBookIds.values(); 
                    return true;
                }               
                
        }//end try
        catch (Exception e)
        {
            System.debug(e);
            System.currentPageReference().getParameters().put('error', 'noInsert');                 
        }//end catch
        return false;
    }  
            public PageReference cascadeStatus() {
        String status = null;     
        for (Integer i = 0; i < selectedProducts.size(); i++) {
            GratisLineItemWithBookTitle g = selectedProducts.get(i);
            if (g.getItemBookTitle() == null) {
                break;
            }
            if (i == 0) {
                // get the first line status
                status = g.getGratisLineItem().Status__c;
            } else {
                // copy the first line status to remaining fields that have a book title
               
               
                    g.getGratisLineItem().Status__c= status;
                
            }
        }
        return null;
    }
    
    public PageReference cascadeDateSentLeft() {        
        Date dtCopy = null;     
        for (Integer i = 0; i < selectedProducts.size(); i++) {
            GratisLineItemWithBookTitle g = selectedProducts.get(i);
            if (g.getItemBookTitle() == null) {
                break;
            }
            if (i == 0) {
                // get the first line date
                dtCopy = g.getGratisLineItem().Date_Sent_Left__c;
            } else {
                // copy the first line date to remaining fields that have a book title
                // and if item has not yet been assigned a date
             
                    g.getGratisLineItem().Date_Sent_Left__c = dtCopy;
                           }
        }
        return null;
    }
        public PageReference cascadeQuantity() {
        Decimal quantity = null;     
        for (Integer i = 0; i < selectedProducts.size(); i++) {
            GratisLineItemWithBookTitle g = selectedProducts.get(i);
            if (g.getItemBookTitle() == null) {
                break;
            }
            if (i == 0) {
                // get the first line quantity
                quantity = g.getGratisLineItem().Quantity__c;
            } else {
                // copy the first line quantity to remaining fields that have a book title
             
                
                    g.getGratisLineItem().Quantity__c = quantity;
                
            }
        }
        return null;
    }
    
    
    
    
    //Get Gratis Record Type
    private String getGratisRecordType() {
        String gratisRecTypeId = gratisItem.RecordTypeId;
        gratisRecTypeId = gratisRecTypeId.substring(0,15);
        return gratisRecTypeId;
    }
     
    private Gratis_Line_Items__c createGratisLineItem()
    {
        final Gratis_Line_Items__c item = new Gratis_Line_Items__c(Related_Gratis_Item__c = gratisItemId, RecordTypeId=getGratisLineItemRecordType() );
        //init pick lists        
        if (getIsOrder()) {
                item.Status__c = 'Pending';
                item.Drop_or_Order__c = 'Order';

        } else {
                item.Status__c = 'Left with Institution';       
                item.Drop_or_Order__c = 'Drop'; 
                      
        }      
          
        // if (getIsDrop()) {
        //         item.Status__c = 'Left with Institution';       
        //         item.Drop_or_Order__c = 'Drop';
        // } else {
        //         item.Status__c = 'Pending';
        //         item.Drop_or_Order__c = 'Order';
        // }
        
        //Title__c, Quantity__c, RecordTypeId, Status__c, Drop_or_Order__c
        //Orders
        //UPS_Tracking_ID__c, Other_Tracking_ID__c, Order_Number__c
        return item;
    }
    
    //Retrieve User's OUP Contry 
    private String getUserOUPCountry()
    {
        String userID = UserInfo.getUserId();
        User user =  [Select Id, OUP_Country__c From User WHERE Id = :userID limit 1];
        if(user != null)
        {
            return user.OUP_Country__c;
        }
        else
        {
            return '';
        }
    }
    
    //URL Query String for next Batch
    private String constructQueryString() {
        String queryStr = GRATIS_ITEM_ID_PARAM + '=' + gratisItemId + '&' + 
                        getGratisRecordType() + '=' + getGratisLineItemRecordType();
        return queryStr;    
    }
    
    private void addErrorMessage(String msg) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
    }
    
}