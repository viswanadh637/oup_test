/**
* Feature #198: Non-Integration profiles will need access to both Gratis Item and Gratis Item drop record type in the system (Already updated)
* However within the Mass Gratis Creation process Non-Integration profile users should NOT have the opition to select the record type but to be automatically defaulted to Gratis Item - Drop
* @author PhanithChhun
* @category Businese Logic
*/
global with sharing class MassCreationGratisItemsUrlRedirector{
    
    /**
    * Skip the page recordtype selection for Non-Integration profile users
    * @param campaignId , Campaign source
    * @return String, page url
    */
    webservice static String urlRedirector(String campaignId){
        Profile profile = [SELECT Name FROM Profile WHERE Id =: Userinfo.getProfileId()];
        if (profile != null) {
            if(profile.Name.startsWith('Non-Integration')){
               String dropRecTypeId = [Select Id From RecordType where DeveloperName = 'GratisItemDrops' limit 1].Id;
               return '/apex/GratisFromTargetProducts?Id='+campaignId+'&RecordType='+dropRecTypeId+'&ent=01I200000000rQR';
            }        
        }
        return '/setup/ui/recordtypeselect.jsp?ent=01I200000000rQR&retURL=%2F'+campaignId+'&save_new_url=%2Fapex%2FGratisFromTargetProducts?Id=' + campaignId;
    }
}