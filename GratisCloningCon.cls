public with sharing class GratisCloningCon{
    public static final String GRATIS_ID_PARAM = 'giId';
    
    private final ID gratisId;
    private ID gratisIDcreated;
    private final Gratis_Item__c gratis;
    private List<Gratis_Line_Items__c> gratisLineItems;
    
    public GratisCloningCon() {
        gratisId = System.currentPageReference().getParameters().get(GRATIS_ID_PARAM );
        if (gratisId == null) {
            addErrorMessage('Undefined '+GRATIS_ID_PARAM +' parameter');
            return; 
        }
        
        try {
            gratis = [select Contact__c, Institution__c, Date_Sent_Left__c, Inspection_Copies__c, Status__c, Adoption__c, Campaign__c from Gratis_Item__c where id=:gratisId];
                            
        } catch (Exception e) {
            addErrorMessage('Failed to load Gratis Item by id:' + gratisId);
            return;
        }
           
    }

    public Boolean getIsInitSuccess() {
        return gratis != null;      
    }
    
    public Gratis_Item__c getGratis() {
        return gratis;
    }
 
    private static void addErrorMessage(String msg) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
    }
    
    public PageReference process() {
        
     
                  try {
                  
                  if (WarehouseManagement.userHasWarehouse(UserInfo.getUserId()))
                       {
                           Warehouse__c w = WarehouseManagement.getWarehouseByUser(UserInfo.getUserId());
                           gratisLineItems = [select Date_Sent_Left__c, Drop_or_Order__c, Quantity__c, Status__c, Title__c From Gratis_Line_Items__c where Related_Gratis_Item__c =: gratisId AND Title__c IN (SELECT WD_book_title__c from Warehouse_Document_Line__c where Document__r.RecordType.Name = 'Stock' and Document__r.Warehouse__r.Warehouse_Owner__c =: UserInfo.getUserId() and quantity__c > 0)];
                       }
                        else
                       {
                            gratisLineItems = [select Date_Sent_Left__c, Drop_or_Order__c, Quantity__c, Status__c, Title__c From Gratis_Line_Items__c where Related_Gratis_Item__c =: gratisId];
                       }
        } catch (Exception e) {/*ignore*/}
        //create gratis
        Gratis_Item__c o = new Gratis_Item__c (Institution__c = gratis.Institution__c);
        

o.Date_Sent_Left__c= gratis.Date_Sent_Left__c;
o.Contact__c= gratis.Contact__c;
o.Inspection_Copies__c= gratis.Inspection_Copies__c;
o.Status__c  = gratis.Status__c  ;
o.Adoption__c = gratis.Adoption__c ;
o.Campaign__c = gratis.Campaign__c;
//o.RecordType= gratis.RecordType;
insert o;
gratisIDcreated = o.id;
        //create Lines
           List<Gratis_Line_Items__c > gratisLineItemsToCreate = new List<Gratis_Line_Items__c >(); 
                for (Gratis_Line_Items__c item: gratisLineItems) {
                Gratis_Line_Items__c oli = new Gratis_Line_Items__c (Related_Gratis_Item__c = gratisIDcreated);
                oli.Date_Sent_Left__c= item.Date_Sent_Left__c;
                oli.Drop_or_Order__c= item.Drop_or_Order__c;
                oli.Quantity__c= item.Quantity__c;
                oli.Status__c= item.Status__c;
                oli.Title__c= item.Title__c;
                 //oli.RecordType = item.RecordType ;
                gratisLineItemsToCreate.add(oli);
                           
        }   
    
        Database.insert(gratisLineItemsToCreate);
                                                
        final PageReference gratisPage = new PageReference('/' + gratisIDcreated);
        gratisPage.setRedirect(true);
        return gratisPage ;
    }
       
 }