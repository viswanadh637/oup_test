public class SelectCampaignTargetProductsCon {
    
    public static final Integer NO_OF_PRELOAD_ITEMS = 6;
    public static final String CAMPAIGN_ID_PARAM    = 'Campaign_Id';
    public static final String RETURN_URL_PARAM     = 'retURL';
    public static final String CANCEL_URL_PARAM     = 'cancelURL';
    private final ID campaignId;
    private final String retURL;
    private final String cancelURL;
    
    private List<SelectableProduct> lookupProducts;               // book titles product lookup list
    private List<TargetProduct> selectedProducts;
    private List<SearchingCriteria> searchingCriterias;                 
    private String query = '';                                    // query build by search criterias
    private Campaign campaign;
    private Boolean isActiveCampaign;
             
    public String filterAdvance { get; set; }
    public Boolean excludeInternationalTitles { get; set; }
    
    public String searchField { get; set; }
    public Boolean sortAsc { get; set; }
        
    public SelectCampaignTargetProductsCon() {
        
        try
        {
            campaignId  = System.currentPageReference().getParameters().get(CAMPAIGN_ID_PARAM);
            retURL      = System.currentPageReference().getParameters().get(RETURN_URL_PARAM);
            cancelURL   = System.currentPageReference().getParameters().get(CANCEL_URL_PARAM);
            sortAsc     = true;
            searchField = 'search_byISBN';
            excludeInternationalTitles = false;
            
            campaign = [SELECT c.Target_Product__r.Id, c.Target_Product__r.Name, c.Target_Product__r.ISBN__c, 
                               c.Target_Product_2__r.Id, c.Target_Product_2__r.Name, c.Target_Product_2__r.ISBN__c, 
                               c.Target_Product_3__r.Id, c.Target_Product_3__r.Name, c.Target_Product_3__r.ISBN__c,  
                               c.Target_Product_4__r.Id, c.Target_product_4__r.Name, c.Target_product_4__r.ISBN__c, 
                               c.Target_Product_5__r.Id, c.Target_Product_5__r.Name, c.Target_Product_5__r.ISBN__c, 
                               c.Target_Product_6__r.Id, c.Target_Product_6__r.Name, c.Target_Product_6__r.ISBN__c,
                               c.IsActive
                        FROM Campaign c 
                        WHERE Id =: campaignId];
            
            isActiveCampaign = campaign.IsActive;
            
            if (selectedProducts == null) {
                selectedProducts = new List<TargetProduct>();
                for (Integer i = 0; i < NO_OF_PRELOAD_ITEMS; i++) {
                    if (i == 0) { selectedProducts.add(new TargetProduct(campaign.Target_Product__r)); }
                    if (i == 1) { selectedProducts.add(new TargetProduct(campaign.Target_Product_2__r)); }
                    if (i == 2) { selectedProducts.add(new TargetProduct(campaign.Target_Product_3__r)); }
                    if (i == 3) { selectedProducts.add(new TargetProduct(campaign.Target_Product_4__r)); }
                    if (i == 4) { selectedProducts.add(new TargetProduct(campaign.Target_Product_5__r)); }
                    if (i == 5) { selectedProducts.add(new TargetProduct(campaign.Target_Product_6__r)); }    
                }  
            }            
        } catch (Exception ex) {
            // System.debug('EXCEPTION[\n'+ex.getMessage()+'\n]\n');
        }               
    }
    
    public Boolean getRenderSearchResults() {
        return (((lookupProducts == null) || (lookupProducts.size() == 0)) ? false : true);
    }
    
    public PageReference searchByField() {
        Search();
        return null;    
    }    
    
    // perform search by defiend searching criterias
    public void Search() {
        
        List<SearchingCriteria> criterias = getSearchingCriterias();  
        String userOUPCountry = getUserOUPCountry();    
        String whereClause = SearchingCriteria.formWhereClause(criterias, filterAdvance);
        Integer resultSetLimit = 100;

        String marketInternational = '';
        if (excludeInternationalTitles == false) {
            marketInternational = 'Market__c = \'INTERNATIONAL\' OR ';
        }
        
        // determine sort by field and order 
        String orderByClause = 'ORDER BY ';
        if (searchField == 'search_byName') {
            orderByClause += 'Name';
        } else if (searchField == 'search_byISBN') {
            orderByClause += 'ISBN__c';
        } else if (searchField == 'search_byPublisher') {
            orderByClause += 'Publisher__c';
        } else if (searchField == 'search_byMarket') {
            orderByClause += 'Market__c';
        } else if (searchField == 'search_bySegment') {
            orderByClause += 'Segment__c';
        } else if (searchField == 'search_bySeries') {
            orderByClause += 'Series__c';
        }
        
        // asc or desc?
        orderByClause += (sortAsc == true) ? ' ASC' : ' DESC';
        
        if (whereClause.length() > 0) {
            whereClause  = whereClause + ' AND (' + marketInternational + ' Market__c = \'' + userOUPCountry  + '\') AND Selectable__c = true ' + orderByClause + ' LIMIT ' + resultSetLimit;
        } else {
            whereClause  =' WHERE (' + marketInternational + ' Market__c = \'' + userOUPCountry + '\') AND Selectable__c = true ' + orderByClause + ' LIMIT ' + resultSetLimit;
        }
              
        lookupProducts = new List<SelectableProduct>(); 

        List<Product2> products = new List<Product2>();        
        try {
            query = 'SELECT Series__c, Segment__c, Publisher__c, ProductCode, Name, Market__c, Level__c, Id, ISBN__c, Selectable__c FROM Product2' + whereClause;
system.debug('Search::01 query:='+query);            
            products = Database.query(query);
        } catch (DmlException ex) {
            products = null;
        } catch (Exception ex) {
            products = null;
        }
         
        if ((products != null) && (products.size() > 0)) {
            for (Product2 p : products) {          
                lookupProducts.add(new SelectableProduct(p));
            }
        }
    }
    
    public void resetSearchingCriterias() {
        searchingCriterias = new List<SearchingCriteria>();
        searchingCriterias.add(new SearchingCriteria(1, true, 'Name', 'c', '', 'AND'));
        searchingCriterias.add(new SearchingCriteria(2, true, 'Name', 'c', ''));
        searchingCriterias.add(new SearchingCriteria(3));
        searchingCriterias.add(new SearchingCriteria(4));
        searchingCriterias.add(new SearchingCriteria(5, false));    
        
        if (lookupProducts != null) {
            lookupProducts.clear();
        }
    }

    // Get Lookup Book titles product
    public List<SelectableProduct> getLookupProducts() { 
        if (lookupProducts == null) {
            lookupProducts = new List<SelectableProduct>();
        }        
        return lookupProducts;
    }
    
    public List<SearchingCriteria> getSearchingCriterias() {
        if(searchingCriterias == null) {
           resetSearchingCriterias();           
        }        
        return searchingCriterias;
    }
    
    public String getQuery() {
        return query;
    }
    
    public String getPageTitle() {
        String pageTitle = 'Select Target Products';        
        return pageTitle;
    }
    
    // Add selecting book titles to selected items list
    public void addSelectedProducts() { 
        
        try {
            
            // get the number of selected/populated target products
            Integer selectedTargetProducts = getSelectedProductsSize();
            Integer selectedBookTitles = 0;
            
            // iterate through the search list and count the number of selected book titles
            for (SelectableProduct sp: getLookupProducts()) {
                if ((sp.isSelected) && (!IsProductSelected(sp.product))) {
                    selectedBookTitles++;
                }
            }
            
            // determine whether there are enough empty slots for the addition book titles
            if (selectedBookTitles > (NO_OF_PRELOAD_ITEMS - selectedTargetProducts)) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'More titles are selected than there are empty Target product fields.'));
                return;
            }
             
            // enough room so append book titles           
            for (SelectableProduct sp: getLookupProducts()) {
                if ((sp.isSelected) && (!IsProductSelected(sp.product))) {
                    if ((selectedProducts == null) || (selectedProducts.size() == 0)) {
                        getSelectedProducts().add(new TargetProduct(sp.product));
                    } else {
                        selectedProducts.add(getSelectedProductsSize(), new TargetProduct(sp.product));
                        // remove an empty field if applicable                      
                        for (Integer i = 0; i < selectedProducts.size(); i++) {
                            TargetProduct tp = selectedProducts.get(i);
                            if (tp.ID == null) {
                                selectedProducts.remove(i);
                                break;     
                            }
                        }                                             
                    }              
                } else if (sp.isSelected == true) {
                    // Book title has already been selected
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Book title ' + sp.product.Name + ' has already been chosen.'));
                }   
            }            
        } catch (Exception ex) {
            System.debug('EXCEPTION[\n'+ex.getMessage()+'\n]\n');
        } 
    }  
    
    public void removeSelectedProduct()
    {
        ID title_id = System.currentPageReference().getParameters().get('title_id');
        
        try {
            
            for (Integer i = 0; i < selectedProducts.size(); i++) {
                TargetProduct tp = selectedProducts.get(i);
                if (tp.ID == title_id) {
                    selectedProducts.remove(i);
                    // add an empty field
                    selectedProducts.add(new TargetProduct(null));
                    return;
                }
            }
        } catch (Exception ex) {
            // System.debug('EXCEPTION[\n'+ex.getMessage()+'\n]\n');
        }        
    }
        
    // Get selected Target Product(s)
    public List<TargetProduct> getSelectedProducts() {
        if (selectedProducts == null) {
            selectedProducts = new List<TargetProduct>();
        }
        return selectedProducts;
    }    
    
    // Get searching fields in SelectOption object 
    public List<SelectOption> getFields() {
        Map<String, Schema.SObjectField> M = Schema.SObjectType.Product2.fields.getMap();
        //List<String> searchingFields = new List<String>(M.keySet());
        List<String> searchingFields = getSearchingFieldNames();
        List<SelectOption> selectOption = new List<SelectOption>();
        selectOption.add(new SelectOption('', '--None--'));
        
        for (String s : searchingFields) {
            Schema.DescribeFieldResult F;
            F = M.get(s).getDescribe();
            selectOption.add(new SelectOption(F.getName(), F.getLabel()));
        }            
  
        return selectOption;       
    }     
    
    public PageReference save() {
        if (saveInternal() == false) {
            return null;
        }
        return new PageReference(retURL);
    }
    
    public PageReference cancel() {
        return new PageReference(cancelURL);
    }
    
    // internal save 
    public Boolean saveInternal()
    {   
        Boolean status = false;
        Boolean insertValues = true;     
        
        try 
        {
            // functionality can only be run against inactive Campaigns
            if (isActiveCampaign == true) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Active campaign!'));
            } else {
                // make sure we do not have duplicates
                List<ID> bookIds = getValidatedItems();
                if ((bookIds != null) && (bookIds.size() > 0)) {
                    
                    // any other validation ...
                    // setTargetProductId
                    if (insertValues == true) {
                        Integer indx = 0;
                        // iterate through the add/amend book titles
                        for (; indx < bookIds.size(); indx++) {
                            setTargetProductId(bookIds.get(indx), indx);                            
                        }
                        // iterate and clear reference for remaining fields if applicable
                        for (; indx < NO_OF_PRELOAD_ITEMS; indx++) {
                            setTargetProductId(null, indx);
                        }
                        
                        update campaign;
                        status = true;
                    }
                } else {
                    // no book titles were selected, so iterate and clear references
                    for (Integer indx = 0; indx < NO_OF_PRELOAD_ITEMS; indx++) {
                        setTargetProductId(null, indx);
                    }
                    update campaign;
                    status = true;
                }               
            }           
        } catch (DmlException ex) {
            System.debug(ex);            
            System.currentPageReference().getParameters().put('error', 'noInsert');
            ApexPages.addMessages(ex);
        } catch (Exception e) {
            System.debug(e);
            System.currentPageReference().getParameters().put('error', 'noInsert');                 
        }
        
        return status;
    }
    
    private Boolean setTargetProductId(ID id, Integer index) {
        Boolean status = false;
        
        try {
            if (index == 0) { campaign.Target_Product__c = id; }
            if (index == 1) { campaign.Target_Product_2__c = id; }
            if (index == 2) { campaign.Target_Product_3__c = id; }
            if (index == 3) { campaign.Target_Product_4__c = id; }
            if (index == 4) { campaign.Target_Product_5__c = id; }
            if (index == 5) { campaign.Target_Product_6__c = id; }
            status = true;
        } catch (Exception ex) {                        
        }   
        
        return status;
    }
    
    private Integer getSelectedProductsSize() {
        // return the number of populated target products
        Integer tpSize = 0;
        
        for (Integer i = 0; i < selectedProducts.size(); i++) {
            TargetProduct tp = selectedProducts.get(i);
            if (tp.ID != null) {
                tpSize++;                    
            }
        }    
        
        return tpSize;
    }
    
    private List<ID> getValidatedItems()
    {
        Boolean isValid = true;
        Boolean alreadyExists = false;
        
        final List<ID> bookIds = new List<ID>();
        for (TargetProduct tp : selectedProducts) {                        
            if (!tp.needSave()) {
                continue;
            }
            if (!tp.validate()) {
                isValid = false;
            } else {
                alreadyExists = false;
                Product2 item = new Product2(Id = tp.ID);               
                // iterate through the list of bookIds and check that 
                // no duplicate book ids exist
                for (ID tmpID : bookIds) {
                    if (tp.ID == tmpID) {
                        alreadyExists = true;
                        break;
                    }
                }               
                if (alreadyExists == false) {
                    bookIds.add(tp.ID);
                } else {
                    // duplicates on the page
                    item.addError('Duplicate title on the page');
                    isValid = false;
                }   
            }     
        }
        
        return isValid ? bookIds : null;
    }
    
    // Check whether Product is selected or not
    private Boolean IsProductSelected(Product2 p) {
        for (TargetProduct tp : getSelectedProducts()) {
            if (tp.ID == p.id) {
                return true;
            }           
        }
        return false;
    }
    
    private List<String> getSearchingFieldNames() {
        // JRS 31/03/2011 - ELT-3-4 [Add Component Type, Edition]
        return new String[] { 'Name', 'ISBN__c', 'Level__c', 'Segment__c', 'Series__c', 'Component_Type__c', 'Edition__c' };
    }    
    
    private String getUserOUPCountry() {
        String userID = UserInfo.getUserId();
        User user = [SELECT Id, OUP_Country__c FROM User WHERE Id = :userID LIMIT 1];
        if (user != null) {
            return user.OUP_Country__c;
        } else {
            return '';
        }
    }
        
    private void addErrorMessage(String msg) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
    }

        
    // Inner Classe(s)
    
    public class TargetProduct {
        
        public String getID() { return id; } 
        public String getBookTitle() { return bookTitle; } 
        Public Boolean getIsPreLoad() { return isPreLoad; }
        public String ISBN{ get; set; }
        public String getErrorMessage() { return errorMessage; }
        public Boolean getIsError() { return isError; }
        
        private ID id;
        private String bookTitle; // book title from selected book title product
        private Boolean isPreLoad;
        private Boolean isError;
        private String errorMessage;
        
        public TargetProduct(Product2 p) {
            
            try
            {
                isError = false;
            
                if (p == null) {
                    isPreLoad       = true;
                } else {
                    id              = p.id;
                    bookTitle       = p.Name;
                    ISBN            = p.ISBN__c;                    
                    isPreLoad       = false;
                }                        
            } catch (Exception ex) {
                // System.debug('EXCEPTION[\n'+ex.getMessage()+'\n]\n');
            }
        }        
        
        public Boolean needSave()
        {
            if (isPreLoad == true) {
                if ((ISBN == null) || (ISBN.length() == 0)) {
                    this.bookTitle = null;
                    isError = false;
                    errorMessage = '';
                    return false;
                }
            }
            
            return true;             
        }
                
        public Boolean validate()
        {   
            Boolean status = true; 
            
            if (isPreLoad == true) {
                // ensure sure that the booktitle match with the ISBN input
                String arg = ISBN.trim();
                List<Product2> products = [SELECT id, name FROM Product2 WHERE ISBN__c = :arg];
                if (products.size() == 1) {
                    Product2 product      = products.get(0);
                    bookTitle             = product.name;
                    id                    = product.id;
                    isError               = false;
                    errorMessage          = '';            
                } else {
                    isError      = true;
                    errorMessage = 'Invalid ISBN';
                    status       = false;
                }
            }
                        
            if (isPreLoad && !status) {
                bookTitle  = null;
            }
            
            return status;                     
        }
    }
     
    public class SelectableProduct {   
        public Product2 product { get; set; } 
        public Boolean isSelected { get; set; }
            
        public SelectableProduct(Product2 p) {
            product = p;
            isSelected = false;
        }          
    }
}