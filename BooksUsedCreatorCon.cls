/* 2.   Automated Creation of Books Used from Adoptions */
public with sharing class BooksUsedCreatorCon {

    public List<OpportunityLineItem> getOpportunityLineItems() {
        return opportunityLineItems;
    }


    public boolean getIsWon() {
        return isWon();
    }

    public static final String OPPORTUNITY_ID_PARAM = 'oppId';
    
    private final ID opportunityId;
    private final Opportunity opportunity;
    private List<Books_Used__c> existingBooksUsed;
    private List<OpportunityLineItem> opportunityLineItems;
    
    public BooksUsedCreatorCon () {
        opportunityId = System.currentPageReference().getParameters().get(OPPORTUNITY_ID_PARAM);
        if (opportunityId == null) {
            addErrorMessage('Undefined '+OPPORTUNITY_ID_PARAM+' parameter');
            return; 
        }
        try {
            opportunity = [select Id, Name, IsWon, IsClosed, StageName, AccountId, Adoption_date__c, OwnerId, CloseDate
                            ,Renewal_Date__c, Competitor_Title_Adopted__c, CampaignId, Campaign.Name, CurrencyIsoCode, Total_Quantity__c, Limit_to_Contact_Roles__c 
                            from Opportunity where id=:opportunityId];
                            
        } catch (Exception e) {
            addErrorMessage('Failed to load Adoption by id:' + opportunityId);
            return;
        }
    }
    /*
        @return true if there are existing Books Used associated with current Adoption
    */
    public Boolean getHasExistingBooksUsed () {
        if (!getIsInitSuccess())
            return true;
        if (existingBooksUsed == null) {
            //check if there are existing Books Used
            existingBooksUsed = [select Id, Book_Used__c, Name, Book_Used__r.Name from Books_Used__c where Created_From_Opportunity__c =: opportunityId];    
        }
        if (existingBooksUsed.size()>0)
            return true;
                
        return false;       
    }
    public List<Books_Used__c> getExistingBooksUsed() {
        return existingBooksUsed;
    }
    /*
        @return true if opportunity initialisation has succeeded,
        this does not mean that Opp-ty is valid for processing though
    */
    public Boolean getIsInitSuccess() {
        return opportunity != null;      
    }
    public Boolean getWrongOpportunityStage() {
        return getIsInitSuccess() && !isWon() && !isLost();
    }
    /*
        init failed or (succeeded but opportunity is not valid for processing)
    */
    public Boolean getIsNothingToProcess () {
        return !isWon() && !isLost() || (isLost() && getHasExistingBooksUsed()) || (isLost() && opportunity.Competitor_Title_Adopted__c == null);
    }
    /*
        init succeeded but opportunity is not valid for processing
    */
    public Boolean getNoCompetitorTitleForLost() {
        return getIsInitSuccess() && isLost() && opportunity.Competitor_Title_Adopted__c == null;
    }
    
    
    public Opportunity getOpportunity() {
        return opportunity;
    }
    /*
        @return true if no books/product2 attached to current opportunity
    */
    public Boolean getNoBooksAttached () {
        if (getIsInitSuccess() && (isWon() || isLost())) {
            //retrieve attached Products
            opportunityLineItems = [select PricebookEntry.Product2Id, PricebookEntry.Id, PricebookEntryId, OpportunityId, Quantity,
                                        UnitPrice, ListPrice, PricebookEntry.Product2.Name
                                        from OpportunityLineItem where OpportunityId =: opportunityId];
            return opportunityLineItems == null || opportunityLineItems.size()<1;
        }
        return false;
        
    }
     public Boolean getNoBooksAttachedCampaign () {
        if (getIsInitSuccess()) {
            //retrieve attached Products
            opportunityLineItems = [select PricebookEntry.Product2Id, PricebookEntry.Id, PricebookEntryId, OpportunityId, Quantity,
                                        UnitPrice, ListPrice , PricebookEntry.Product2.Name
                                        from OpportunityLineItem where OpportunityId =: opportunityId];
            return opportunityLineItems == null || opportunityLineItems.size()<1;
        }
        return false;
    }
    
    public PageReference startProcess() {
        if (isWon()) {
            return processWonOpportunity();         
        } else if (isLost()) {
            return processLostOpportunity();            
        }
        return null;
        
    }
    public Boolean getCanStartProcess() {
        if (getIsNothingToProcess() || getNoBooksAttached())
            return false;
        return true;        
    }
    private static void addErrorMessage(String msg) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
    }
    private Boolean isWon() {
        return opportunity != null && opportunity.IsWon;
    }
    private Boolean isLost() {
        if (opportunity == null)
            return false;
        return !isWon() && opportunity.IsClosed;
        /*
        if (opportunity == null || isWon()) {
            return false;
        }
        String stageName = opportunity.StageName.toLowerCase();
        return stageName == 'lost' || stageName.contains(' lost');
        */
    }   
    /*
        For "Won" Adoptions, the Book Titles to be included on the Books Used records 
        will be defined by the Book Titles held against the Adoption.
    */
    private PageReference processWonOpportunity() {
        System.debug('entered processWonOpportunity');
        if (getIsNothingToProcess())
            return null;
        
        if (getNoBooksAttached())
            return null;
        
        System.debug('Processing WON opportunity');
        //load primary contact role
        OpportunityContactRole primaryContactRole = null;
        try {
            primaryContactRole = [select OpportunityId, ContactId From OpportunityContactRole where OpportunityId =: opportunityId and IsPrimary = true limit 1];
        } catch (Exception e) {/*ignore*/}
        //create Books Used
        final List<Books_Used__c> bookUsedToInsert = new List<Books_Used__c>(); 
         final List<Books_Used__c> bookUsedToUpdate = new List<Books_Used__c>(); 
        for (OpportunityLineItem item: opportunityLineItems) {
        boolean exists = false;
         Books_Used__c bookUsed = new Books_Used__c(Created_From_Opportunity__c = opportunityId);
        if (getHasExistingBooksUsed () )
        {
        for (Books_Used__c existingBookUsed: existingBooksUsed)
            {
            if (item.PricebookEntry.Product2Id == existingBookUsed.Book_Used__c) 
            {
            exists = true;
            bookUsed.Id = existingBookUsed.Id;
            }
            }
        }
           
            bookUsed.Adoption_Date__c = (opportunity.Adoption_date__c == null)? System.today():opportunity.Adoption_date__c;
            bookUsed.Book_Used__c = item.PricebookEntry.Product2Id;
            //bookUsed.Class Size ???
            //bookUsed.Competitor_Title__c = 'No'; //Field has been removed
            bookUsed.CurrencyIsoCode = opportunity.CurrencyIsoCode;
           if (!exists) bookUsed.Institution__c = opportunity.AccountId;
            //Product2 specific  
            bookUsed.Number_used__c = item.Quantity;
            // JRS 31/03/2011 - ELT-3-2
            // bookUsed.OwnerId = opportunity.OwnerId;
            bookUsed.Price__c = (item.UnitPrice == null || item.UnitPrice == 0.0)? item.ListPrice : item.UnitPrice;
            bookUsed.Purchase_Date__c = bookUsed.Adoption_Date__c + 1;//Ensure that the purchase date is after the adoption date: [Purchase_Date__c]
            bookUsed.Renewal_Date__c = opportunity.Renewal_Date__c;
            bookUsed.Source__c = (opportunity.CampaignId == null)? '' : opportunity.Campaign.Name;
            bookUsed.Status__c = 'Issue';//changed from 'Recommended' by request from Scott as of 01/10/2008
            //Teacher - If a primary Contact is flagged their name will appear. Otherwise the field will be blank.
            bookUsed.Teacher__c = primaryContactRole == null? null : primaryContactRole.ContactId;
            
            if (exists)
            {
            bookUsedToUpdate.add(bookUsed);
            }
            else
            {
            bookUsedToInsert.add(bookUsed);
            }
        }       
        if (bookUsedToInsert.size()>0) {
            try {
                insert bookUsedToInsert;
            } catch (Exception e) {
                addErrorMessage('Failed to insert Book Used.' + e.getMessage());
                System.debug('Failed to insert Book Used.' + e.getMessage());
                return null;    
            }

        }
           if (bookUsedToUpdate.size()>0) {
            try {
                update bookUsedToUpdate;
            } catch (Exception e) {
                addErrorMessage('Failed to update Book Used.' + e.getMessage());
                System.debug('Failed to update Book Used.' + e.getMessage());
                return null;    
            }

        }
        
        autoPopulatePrimaryCampaignSource();
                                        
        final PageReference accountPage = new PageReference('/' + opportunity.AccountId);
        accountPage.setRedirect(true);
        return accountPage;
    }
    /*
        For "Lost" Adoptions, the Book Titles to be included on the Books Used record(s) will be defined by "Competitor Title Adopted" 
    */
    private PageReference processLostOpportunity() {
        System.debug('entered processLostOpportunity');
        if (getIsNothingToProcess())
            return null;
        
        if (getNoBooksAttached() || opportunity.Competitor_Title_Adopted__c == null)
            return null;
        
        // Create new sharing object for the custom object Job.
        System.debug('Processing LOST opportunity');
        //load primary contact role
        OpportunityContactRole primaryContactRole = null;
        try {
            primaryContactRole = [select OpportunityId, ContactId From OpportunityContactRole where OpportunityId =: opportunityId and IsPrimary = true limit 1];
        } catch (Exception e) {/*ignore*/}
            
        //create Book Used for Lost
        Books_Used__c bookUsed = new Books_Used__c(Created_From_Opportunity__c = opportunityId);
        bookUsed.Adoption_Date__c = (opportunity.Adoption_date__c == null)? System.today():opportunity.Adoption_date__c;
        bookUsed.Book_Used__c = opportunity.Competitor_Title_Adopted__c;
        //bookUsed.Class Size ???
        //bookUsed.Competitor_Title__c = 'Yes'; ////Field has been removed      
        bookUsed.CurrencyIsoCode = opportunity.CurrencyIsoCode;
        bookUsed.Institution__c = opportunity.AccountId;
        
        //Product2 specific  
        bookUsed.Number_used__c = opportunity.Total_Quantity__c;
        // JRS 31/03/2011 - ELT-3-2
        //bookUsed.OwnerId = opportunity.OwnerId;
        bookUsed.Price__c = 0.01;//(item.UnitPrice == null || item.UnitPrice == 0.0)? item.ListPrice : item.UnitPrice;

        bookUsed.Purchase_Date__c = bookUsed.Adoption_Date__c + 1;//Ensure that the purchase date is after the adoption date: [Purchase_Date__c]
        bookUsed.Renewal_Date__c = opportunity.Renewal_Date__c;
        bookUsed.Source__c = (opportunity.CampaignId == null)? '' : opportunity.Campaign.Name;      
                    
        bookUsed.Status__c = 'Issue';//changed from 'Recommended' by request from Scott as of 01/10/2008
        //Teacher - If a primary Contact is flagged their name will appear. Otherwise the field will be blank.
        bookUsed.Teacher__c = primaryContactRole == null? null : primaryContactRole.ContactId;
        
        try {
            insert bookUsed;
        } catch (Exception e) {
            addErrorMessage('Failed to insert Book Used.' + e.getMessage());
            System.debug('Failed to insert Book Used.' + e.getMessage());
            return null;    
        }
        
        autoPopulatePrimaryCampaignSource();
        
        final PageReference accountPage = new PageReference('/' + opportunity.AccountId);
        accountPage.setRedirect(true);
        return accountPage;
    }
    
    
    public PageReference populateCampaignSource()
    {
    autoPopulatePrimaryCampaignSource();
    final PageReference opportunityPage = new PageReference('/' + opportunityId);
        opportunityPage .setRedirect(true);
        return opportunityPage ;
    
    }
    
    
    private void autoPopulatePrimaryCampaignSource() {
        try {
            if (opportunity.CampaignId != null) {
                // nothing to do [already defined]
                return;
            }
            
            // 0. populate primary campaign source [CampaignId] field
            Set<ID> contactIds = new Set<ID>();
            
            // 1a. get contacts from Adoption
            contactIds = getContactsFromContactRoles();
            
            // determine whether to check contacts within 'Positions Held'
            if (opportunity.Limit_to_Contact_Roles__c == false) {
                // if (contactIds.isEmpty() == true) {
                    // 1b. get contacts associated via Positions Held to the Institution related to this Adoption
                    Set<ID> contactPHIds = getContactsFromPositionHeld();
                    contactIds.addAll(contactPHIds);
                // }
            }
            if (contactIds.isEmpty() == true) {
                // nothing to do
                return;
            }
            
            // 2a. Get Book Title/Series (Product) list      
            Set<string> series = getSeriesFromAdoptionBookTitles();

            // 3. Campaign Member Status + Target Products
            List<CampaignMember> campaignMembers = getCampaignMembersFromContactIdsSeries(contactIds, series);

            ID primaryCampaignSourceId = null;
            
            // 4. iterate campaign member(s)
            if (campaignMembers.isEmpty() == false) {
                for (CampaignMember cmbr : campaignMembers) {
                    // first item is the most recent
                    primaryCampaignSourceId = cmbr.Campaign.Id;
        
                    break;                  
                }
            }
            
            
               // 4b. Create all influences as primary)
            if (campaignMembers.isEmpty() == false) {
                for (CampaignMember cmbr : campaignMembers) {
                    // first item is the most recent
  
                    Opportunity opportunity = [SELECT o.CampaignId FROM Opportunity o WHERE o.Id = :opportunityId];
                     opportunity.CampaignId = cmbr.Campaign.Id;
                        database.update(opportunity);
                        
                }
            }
            // 5. update opportunity record with the right primary campaign
            if (primaryCampaignSourceId != null) {
                // retrieve opportunity to update
                Opportunity opportunity = [SELECT o.CampaignId FROM Opportunity o WHERE o.Id = :opportunityId];
                // set field
                opportunity.CampaignId  = primaryCampaignSourceId;
                // update record
                database.update(opportunity);
            }            
        } catch (system.exception ex) {
            // ...          
        }
    }
    
    private Set<ID> getContactsFromContactRoles() {
        Set<ID> contactIds = new Set<ID>();     
        try {
            // iterate contact role(s)
            for (OpportunityContactRole role : [SELECT o.OpportunityId, o.ContactId
                                                FROM OpportunityContactRole o
                                                WHERE o.opportunityid = :opportunity.Id]) {
                contactIds.add(role.ContactId);
            }
        } catch (system.exception ex) {
            // ...         
        }
        return contactIds;      
    }
            
    private Set<ID> getContactsFromPositionHeld() {
        Set<ID> contactIds = new Set<ID>();     
        try {
            // 1. get contacts associated via Positions Held to the Institution related to this Adoption
            for (Positions_Held__c ph : [SELECT p.Institution__c, p.Contact__c 
                                         FROM Positions_Held__c p
                                         WHERE p.Institution__c = :opportunity.AccountId AND 
                                               p.Inactive__c = false]) {
                contactIds.add(ph.Contact__c);
            }
        } catch (system.exception ex) {
            // ...         
        }
        return contactIds;      
    }
    
    private Set<string> getSeriesFromAdoptionBookTitles() {
        Set<string> series = new Set<string>();
        try {
            // iterate adoption book titles                     
            for (OpportunityLineItem ol : [SELECT o.PricebookEntry.Product2.Series__c, o.PricebookEntry.Product2Id, o.PricebookEntry.Pricebook2Id, o.PricebookEntry.Name, 
                                                  o.PricebookEntry.Id, o.PricebookEntryId, o.OpportunityId
                                           FROM OpportunityLineItem o
                                           WHERE o.OpportunityId = :opportunityId]) {
                // append to container
                series.add(ol.PricebookEntry.Product2.Series__c);                                                   
            }  
        } catch (system.exception ex) {
            // ...         
        }   
        return series;  
    }
        
    private List<CampaignMember> getCampaignMembersFromContactIdsSeries(final Set<ID> contactIds, final Set<string> series) {
        List<CampaignMember> campaignMembers = new List<CampaignMember>();
        try {
            date dtLimit = date.newinstance(system.now().year(), system.now().month(), system.now().day()).addMonths(-24);
           
            // Campaign Member Status + Target Products
            for (CampaignMember cmbr : [SELECT c.Status, c.ContactId, c.HasResponded, c.CampaignId,
                                               c.Campaign.Target_Product__c, c.Campaign.Target_Product_2__c, 
                                               c.Campaign.Target_Product_3__c, c.Campaign.Target_Product_4__c, 
                                               c.Campaign.Target_Product_5__c, c.Campaign.Target_Product_6__c, 
                                               c.Campaign.Name, c.Campaign.CreatedDate, c.Campaign.EndDate
                                        FROM CampaignMember c
                                        WHERE c.ContactId IN :contactIds AND 
                                              c.Campaign.EndDate >= :dtLimit AND
                                              c.HasResponded = true AND
                                              ( (c.Campaign.Target_Product__r.Series__c IN :series) OR 
                                                (c.Campaign.Target_Product_2__r.Series__c IN :series) OR  
                                                (c.Campaign.Target_Product_3__r.Series__c IN :series) OR 
                                                (c.Campaign.Target_Product_4__r.Series__c IN :series) OR 
                                                (c.Campaign.Target_Product_5__r.Series__c IN :series) OR 
                                                (c.Campaign.Target_Product_6__r.Series__c IN :series) )                                              
                                        ORDER BY c.Campaign.EndDate DESC]) {
                // append to container
                campaignMembers.add(cmbr);
            }
           
        } catch (system.exception ex) {
            // ...         
        }
        return campaignMembers;
    }
                    
}