/*
    This is the controller for the leadConvertCoreComponent
*/
public with sharing class leadConvertCoreComponentController extends ComponentControllerBase {
    
    // prefix for the label of existing accounts
    private final string EXISTING = 'Attach to existing: ';
    
    // checkbox on the component indicating if there will be an email sent to the owner 
    public boolean sendOwnerEmail {get; set;}
    
    // This will hold the Opportunity for the Opportunity name on the comonent 
    public Opportunity opportunityID {get; set;}
    
    // Checkbox on the component indicating if an Opportunity should be created
    public Boolean doNotCreateOppty {get; set;}
    
    public Boolean overwriteEmailAddress {get; set;}
    public String Job_type {get; set;}
    public String Level_of_Influence {get; set;}
    public String Subject {get; set;}

        
    private transient List<Schema.PicklistEntry> phType= Positions_Held__c.Job_Type__c.getDescribe().getPicklistValues();
    private transient List<Schema.PicklistEntry> phLevel = Positions_Held__c.Level_of_Influence__c.getDescribe().getPicklistValues();
        private transient List<Schema.PicklistEntry> phSubject = Positions_Held__c.Subject__c.getDescribe().getPicklistValues();
    // This will hold the owner of Lead
    
    
    
      public List<SelectOption> PHTypeOption {
        get {
            if(PHTypeOption == null) {
                PHTypeOption = new List<SelectOption>();
                
                for (Schema.PicklistEntry status : phType)
                {
                    PHTypeOption .add(new SelectOption(status.getLabel(), status.getLabel()));
                } 
            }
            return PHTypeOption ;
        }
        set;
    }
    
     public List<SelectOption> PHInfluenceOption {
        get {
            if(PHInfluenceOption == null) {
                PHInfluenceOption = new List<SelectOption>();
                
                for (Schema.PicklistEntry status : phLevel)
                {
                    PHInfluenceOption.add(new SelectOption(status.getLabel(), status.getLabel()));
                } 
            }
            return PHInfluenceOption;
        }
        set;
    }
    
    public List<SelectOption> PHSubjectOption{
        get {
            if(PHSubjectOption== null) {
                PHSubjectOption= new List<SelectOption>();
                
                for (Schema.PicklistEntry status : phSubject)
                {
                    PHSubjectOption.add(new SelectOption(status.getLabel(), status.getLabel()));
                } 
            }
            return PHSubjectOption;
        }
        set;
    }
    
    
    public Contact contactID {
        get {
            if (contactId == null) {
                contactID = new Contact(OwnerId = leadConvert.ownerId);
            }
            return contactId;
        }
        set;
    }
    
    //THis is set by the <apex:attribute> and is the lead to convert
    public Lead leadConvert {
        get; 
        set {
            //the first time this is set, the select list of Accounts will be populated 
            if (accounts == null) {
                system.debug('leadConvert set to ' + value);
                
                leadConvert = value;
                
                //populate the Account dropdown based on the lead
                populateAccounts(); 
                
            }
        }
    }
    
    // the list of accounts in the select list
    public List<SelectOption> accounts {get; set;}
    
    // the selected account in the select list of accounts
    public string selectedAccount {get; set;}
    
        // the list of accounts in the select list
    public List<SelectOption> contacts {get; set;}
    
    // the selected account in the select list of accounts
    public string selectedContact {get; set;}
    
    public Boolean isDuplicate {get; set;}
    
    
    //Constructor
    public leadConvertCoreComponentController() {
        // create a new Opportunity which will hold the Opportuniy name set by the user
        opportunityId = new Opportunity();
        doNotCreateOppty = true;
        // set the selected Account to NONE by default
        selectedAccount = 'NONE';
           selectedContact = 'NONE';
         
         
    }
    
    
   
    
    // Find an Account using SOSL based on the given company name
    private Account [] findCompany (string companyName, string city) {
        
        //perform the SOSL query
        List<List<SObject>> searchList = [
            FIND :companyName
            IN NAME FIELDS 
            RETURNING 
            Account(
                Id, 
                Name, ShippingStreet, ShippingCity WHERE ShippingCity =:city ORDER BY Name LIMIT 200
            )
        ];
        
       
        List <Account> accountsFound = new List<Account>();
        
        for (List <sobject> sObjs : searchList) {
            
            for (sObject s : sObjs) {
                
                //add the account that was found to the list of found accounts
                accountsFound.add((Account) s);
            }   
        }
        
        
             //perform the SOSL query
         searchList = [
            FIND :city
            IN ALL FIELDS 
            RETURNING 
            Account(
                Id, 
                Name, ShippingStreet, ShippingCity WHERE ShippingCity =:city LIMIT 200
            )
        ];
        
        
        for (List <sobject> sObjs : searchList) {
            
            for (sObject s : sObjs) {
                
                //add the account that was found to the list of found accounts
                accountsFound.add((Account) s);
            }   
        }
        
        
        
        
        
        // return the list of found accounts
        return accountsFound;
    }
    
    
    // Find an Account using SOSL based on the given company name
    private Contact[] findDuplicates (string email, string Name) {
        
        //perform the SOSL query
        List<List<SObject>> searchList = [
            FIND :email
            IN EMAIL FIELDS 
            RETURNING 
            Contact(
                Id, 
                Name, Email, Secondary_Email__c, Account.Name ORDER BY Name LIMIT 200
            )
        ];
        
       
        List <Contact> contactsFound = new List<Contact>();
        
        for (List <sobject> sObjs : searchList) {
            
            for (sObject s : sObjs) {
                
                //add the account that was found to the list of found accounts
                contactsFound .add((Contact) s);
            }   
        }
        
        
             //perform the SOSL query
         searchList = [
            FIND :name
            IN Name FIELDS 
            RETURNING 
           Contact(
                Id, 
                Name, Email, Secondary_Email__c, Account.Name where email <>: email and Secondary_Email__c <>:email ORDER BY Name LIMIT 200
            )
        ];
        
        
        for (List <sobject> sObjs : searchList) {
            
            for (sObject s : sObjs) {
                
                //add the account that was found to the list of found accounts
                contactsFound.add((Contact) s);
            }   
        }
        
        
        
        
        
        // return the list of found accounts
        return contactsFound;
    }
    
    
    
    //populate the list of Accounts in the dropdown
    private void populateAccounts() {
        
        if (leadConvert != null) {
                
            string company = leadConvert.Company;
            string city = leadConvert.City;
            
            // find any accounts that match the SOSL query in the findCompany() method  
            Account [] accountsFound = findCompany(company + '*', city);
            
            accounts = new List<selectOption>();
            
            if (accountsFound != null && accountsFound.size() > 0) {
                
                // if there is at least 1 account found add a NONE option and not a Create New Account option - new institutions have to be created manually
                accounts.add(new SelectOption('NONE', '-None-'));
                
               // accounts.add(new SelectOption('NEW', 'Create New Account: ' + company ));
                
                // for each account found, add an option to attach to the existing account
                for (Account a : accountsFound) {
                    
                    accounts.add(new SelectOption(a.Id, EXISTING + a.Name + ' ' + a.ShippingStreet + ' ' + a.ShippingCity));
                }
                
            }
            
            else {
                
                // if no accounts matched then simply add a Create New Account option
                //accounts.add(new SelectOption('NEW', 'Create New Account: ' + company ));
                accounts.add(new SelectOption('NONE', '-Please find or create the institution manually-'));
                system.debug('no account matches on company ' + company);
                
            }
            
             Contact [] contactsFound = findDuplicates(leadConvert.Email, LeadConvert.Name);
             contacts = new  List<selectOption>();
             
                   if (contactsFound != null && contactsFound.size() > 0) {
                   
                   contacts.add(new SelectOption('NONE', '-Possible duplicates found-')); 
                    for (Contact c  : contactsFound ) {
                    
                    contacts.add(new SelectOption(c.Id, c.Name + ' - ' + c.Account.Name + ' - ' + c.email+ ';' + c.Secondary_Email__c));
                }
                   }
                   else
                   {
                     contacts.add(new SelectOption('NONE', '-No duplicates found-')); 
                   
                   }
                   
            
            
        }
        
        else system.debug('leadConvert = null');
            
    }
    
    // when the selected account in the select list of accounts changes this method is called 
    public PageReference accountChanged() {
       
        // if either the NONE option or the Create New Account option is selected, the Opportuniy Name is set to the lead's company
        if (selectedAccount != 'NEW' && selectedAccount != 'NONE')           
         {
            // find the account's Id and Name that was selected and set the Opportuity name to that Account
            Account [] a = [
                SELECT Id, Name 
                FROM Account WHERE Id = :selectedAccount];
                              
        }
        return null;
    }
    
    //this gets called when an existing accout gets looked up via the lookup magnifying glass
    public PageReference accountLookedUp() {
        system.debug('!!! Account looked up --> ' + contactId.AccountId );
        
        //find the Id and Nmae of the Account that was looked up        
        Account [] a = [
            SELECT Id, Name 
            FROM Account WHERE Id = :contactId.AccountId];
        
        if (a.size() > 0) {
            
            // add the locked up account to the slect list
            accounts.add(new SelectOption(a[0].Id, EXISTING + a[0].Name));
            
            // set the selected account to the one that was just looked up by default
            selectedAccount = a[0].Id;
            
            // set the Opportunity name to the account's name that was looked up
            // opportunityId.Name = a[0].Name + '-';
            
            system.debug('accounts --> ' + accounts);
        }
        
        return null;
    }
    
    // set up the Lead Status pick list
    public List<SelectOption> LeadStatusOption {
        
        get {
            
            
            if(LeadStatusOption == null) {
                
                LeadStatusOption = new List<SelectOption>();
                
                //get the lead statuses
                LeadStatus [] ls = [select MasterLabel from LeadStatus where IsConverted=true order by SortOrder];
                
                // if there is more than 1 lead status option, add a NONE option  
                if (ls.size() > 1) {
                    LeadStatusOption.add(new SelectOption('NONE', '-None'));
                }
                
                // add the rest of the lead status options
                for (LeadStatus convertStatus : ls){
                    LeadStatusOption.add(new SelectOption(convertStatus.MasterLabel, convertStatus.MasterLabel));
                } 
                
            }
            
            return LeadStatusOption;
        }
        set;
    }
    
}