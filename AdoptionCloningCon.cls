public with sharing class AdoptionCloningCon{
  /*testing*/
    public static final String OPPORTUNITY_ID_PARAM = 'oppId';
    
    private final ID opportunityId;
    private ID opportunityIDcreated;
    private final Opportunity opportunity;
    private List<OpportunityContactRole> existingRoles;
    private List<OpportunityLineItem> opportunityLineItems;
    
    public AdoptionCloningCon() {
        opportunityId = System.currentPageReference().getParameters().get(OPPORTUNITY_ID_PARAM);
        if (opportunityId == null) {
            addErrorMessage('Undefined '+OPPORTUNITY_ID_PARAM+' parameter');
            return; 
        }
        
        try {
            opportunity = [select Name, StageName, AccountId, Adoption_date__c, CloseDate
                            ,Renewal_Date__c, CurrencyIsoCode, Limit_to_Contact_Roles__c , Pricebook2Id, Probability, Type
                            from Opportunity where id=:opportunityId];
                            
        } catch (Exception e) {
            addErrorMessage('Failed to load Adoption by id:' + opportunityId);
            return;
        }
           
    }

    public Boolean getIsInitSuccess() {
        return opportunity != null;      
    }
    
    public Opportunity getOpportunity() {
        return opportunity;
    }
 
    private static void addErrorMessage(String msg) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
    }
    
    public PageReference process() {
        
          try {
            existingRoles = [select ContactId, IsPrimary, Role From OpportunityContactRole where OpportunityId =: opportunityId];
            
        } catch (Exception e) {/*ignore*/}
        
                  try {
            opportunityLineItems = [select PricebookEntryId, Quantity, PricebookEntry.UnitPrice From OpportunityLineItem where OpportunityId =: opportunityId and PricebookEntry.IsActive = true];
            
        } catch (Exception e) {/*ignore*/}
        //create Opportunity
        Opportunity o = new Opportunity(AccountId= opportunity.AccountId);
        
        o.Name= 'Cloned: ' + opportunity.Name;
o.StageName= 'Prospect';
o.Adoption_date__c= opportunity.Adoption_date__c;
o.CloseDate= opportunity.CloseDate;
o.Renewal_Date__c= opportunity.Renewal_Date__c;
o.CurrencyIsoCode= opportunity.CurrencyIsoCode;
o.Limit_to_Contact_Roles__c= opportunity.Limit_to_Contact_Roles__c;
o.Pricebook2Id= opportunity.Pricebook2Id;
o.Type= opportunity.Type;
insert o;
opportunityIDcreated = o.id;
        //create Lines
           List<OpportunityLineItem> opportunityLineItemsToCreate = new List<OpportunityLineItem>(); 
                for (OpportunityLineItem item: opportunityLineItems) {
                OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = opportunityIDcreated);
                oli.PricebookEntryId = item.PricebookEntryId;
                oli.Quantity = item.Quantity;
                oli.UnitPrice = item.PricebookEntry.UnitPrice;
                
                opportunityLineItemsToCreate.add(oli);
                 
            //bookUsed.Price__c = (item.UnitPrice == null || item.UnitPrice == 0.0)? item.ListPrice : item.UnitPrice;
           
        }   
        //create Roles
        List<OpportunityContactRole> opportunityContactRolesToCreate = new List<OpportunityContactRole>(); 
        
        for (OpportunityContactRole item: existingRoles ) {
           OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId = opportunityIDcreated);
           ocr.ContactId = item.ContactId;
           ocr.IsPrimary = item.IsPrimary;
           ocr.Role = item.Role;
           opportunityContactRolesToCreate.add(ocr);
        }       
        
        Database.insert(opportunityContactRolesToCreate);
        Database.insert(opportunityLineItemsToCreate);
                                                
        final PageReference opportunityPage = new PageReference('/' + opportunityIDcreated +'/e?retURL='+opportunityIDcreated);
        opportunityPage.setRedirect(true);
        return opportunityPage ;
    }
       
 }