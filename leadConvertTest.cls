@isTest(SeeAllData=true)
private class leadConvertTest{
  
  public static testMethod void testStandardProcess() {
        
  
        Test.setCurrentPageReference(Page.leadConvertPage);
        //test missing Lead ID
        Lead l = new Lead();
        l.FirstName = 'a';
        l.LastName = 'a';
        l.Street = 'a';
        l.City = 'Kraków';
        l.PostalCode = 'a';
        l.Company = 'Szkoła Podstawowa';
        l.Email = 'email@domain.com';
        database.insert(l);
        
        l= [SELECT Id, Status, OwnerId, Name, Company, City, FirstName, LastName, Email FROM Lead WHERE email = 'email@domain.com'][0];
        ApexPages.StandardController sc = new ApexPages.standardController(l); 
        leadConvertController controller = new leadConvertController(sc);
        controller.setComponentController(new leadConvertCoreComponentController());
        ComponentControllerBase  mycontroller = controller.getMyComponentController();
        List<SelectOption> lista = controller.myComponentController.PHTypeOption;
        lista = controller.myComponentController.PHInfluenceOption;
        lista = controller.myComponentController.PHSubjectOption;
         lista = controller.myComponentController.LeadStatusOption;
        controller.myComponentController.LeadConvert = l;
        controller.myComponentController.overwriteEmailAddress = true;
        controller.convertLead();
        controller.myComponentController.selectedAccount = [Select id from Account LIMIT 1][0].Id;
        controller.myComponentController.sendOwnerEmail = false;
        controller.convertLead();

        
    }
    }