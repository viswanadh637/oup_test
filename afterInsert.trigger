trigger afterInsert on Warehouse__c (before insert, after insert) {
/*testing*/

if (Trigger.isBefore)
{

for (Warehouse__c w : Trigger.new)
{
if (WarehouseManagement.userHasWarehouse(w.Warehouse_Owner__c))
{
Warehouse__c wexisting = WarehouseManagement.getWarehouseByUser(w.Warehouse_Owner__c);
w.Warehouse_Owner__c.addError('This user is already assigned to a Warehouse ID: '+wexisting.id);
}
}
            
}
// If the trigger is not a before trigger, it must be an after trigger.
else
{

for (Warehouse__c w: Trigger.new)
{

//creating new stock document
Warehouse_Document__c wd = new Warehouse_Document__c ();
wd.Warehouse__c = w.id;
wd.RecordTypeId =  [Select Id from RecordType where DeveloperName = 'Stock'][0].Id;
wd.Date__c = Date.Today();
wd.Status__c = 'New';
Database.SaveResult sr = database.insert(wd);
//approving the stock document
if(sr.isSuccess())
{
 // create the new approval request to submit
 Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
 req.setProcessDefinitionNameOrId('Warehouse_Document_Approval');
 req.setSkipEntryCriteria(true);
 
 req.setComments('Automatically submitted for approval on creation.');
 req.setObjectId(sr.getId());
 // submit the approval request for processing
 Approval.ProcessResult result = Approval.process(req);
 
 
  List<Id> newWorkItemIds = result.getNewWorkitemIds();
        
        // Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest req2 = 
            new Approval.ProcessWorkitemRequest();
        req2.setComments('Approving request.');
        req2.setAction('Approve');
        req2.setNextApproverIds(new Id[] {'005200000010XWk'});
        
        // Use the ID from the newly created item to specify the item to be worked
        req2.setWorkitemId(newWorkItemIds.get(0));
        
        // Submit the request for approval
        Approval.ProcessResult result2 =  Approval.process(req2);
}
//end if succesfully created document
}
//end iteration
}
//end is after
}