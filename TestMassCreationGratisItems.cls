/**
*   For testing
*   This class will cover Business logic for 4 classes : MassCreationGratisItemsUrlRedirector, MassCreationGratisItemsCon, BatchCreateGratisItems, BatchCheckHelper
*   let's create some Gratis_Items and Gratis Lineitems
*   @author PhanithChhun
*   @category Test Logic
*/
@IsTest
public class TestMassCreationGratisItems{
    public final static Integer NUM_CONTACTS = 10;
    public final static Integer NUM_PRODUCTS = 6;
    public final static Integer NO_PER_PAGE = 20;
    
    public static Campaign campaign;
    public static List<CampaignMember> campaignMembers = new List<CampaignMember>();
    public static String recTypeId_GratisItemOrders;
    
    public static Campaign campaign2;
    public static List<CampaignMember> campaignMembers2 = new List<CampaignMember>();
    public static String recTypeId_GratisItemDrops;
    
    /** 
    *   Setup required datas for Business
    *   method initialize the associate CampaignMember for a Campaign
    */
    public static void setupTestData(){
        
        // Record Type for gratis Item
        Set<String> usedRecordTypeNames = new Set<String>{'GratisItemDrops','GratisItemOrders'};
        for(RecordType rt: [Select SobjectType,DeveloperName From RecordType where DeveloperName IN: usedRecordTypeNames And SobjectType = 'Gratis_Item__c']){
            if(rt.DeveloperName.equals('GratisItemDrops')){
                recTypeId_GratisItemOrders = rt.Id;
            }else if(rt.DeveloperName.equals('GratisItemOrders')){
                recTypeId_GratisItemDrops = rt.Id;
            }
        }
        
        // create territory
        OUP_Territory__c territory = new OUP_Territory__c(Name = 'Test Territory', OUP_Country__c = 'Brazil', Territory__c = 'ABC');
        insert territory;
        
        // create accounts
        List<Account> accounts = new List<Account>(); 
        for (Integer i = 0; i < NUM_CONTACTS; i++) {
            accounts.add(new Account(Name = 'Test Account'+i, Territory__c = territory.Id, ShippingCity = 'London', 
                                     ShippingPostalCode = 'LDN1', ShippingStreet = 'Brook'));           
        } 
        insert accounts;
        
        // create contacts      
        List<Contact> contacts = new List<Contact>(); 
        for (Integer i = 0; i < NUM_CONTACTS; i++) {
            contacts.add(new Contact(AccountId = accounts[i].Id, LastName = 'Test Contact'+i));         
        } 
        insert contacts;
        
        // create products
        List<Product2> products = new List<Product2>(); 
        for (Integer i = 0; i < NUM_PRODUCTS; i++) {
            products.add(new Product2(Name='Product'+i, Series__c='Series'+i));
        }
        insert products;

        // create campaign
        List<Campaign> lcamp = new List<Campaign>();
        campaign = new Campaign(Name='Test Campaign', Campaign_Objectives__c = 'Test Business Logic', 
                                    Target_Product__c = products[0].Id, Target_Product_2__c = products[1].Id,
                                    Target_Product_3__c = products[2].Id, Target_Product_4__c = products[3].Id, 
                                    Target_Product_5__c = products[4].Id, Target_Product_6__c = products[5].Id);
        lcamp.add(campaign); 
        campaign2 = new Campaign(Name='Test Campaign2', Campaign_Objectives__c = 'Test Business Logic', 
                                    Target_Product__c = products[0].Id, Target_Product_2__c = products[1].Id,
                                    Target_Product_3__c = products[2].Id, Target_Product_4__c = products[3].Id, 
                                    Target_Product_5__c = products[4].Id, Target_Product_6__c = products[5].Id);
        lcamp.add(campaign2);                          
        insert lcamp;
        
        // create campaign members
        List<CampaignMember> lallMember = new List<CampaignMember>();
        campaignMembers = new List<CampaignMember>();
        campaignMembers2 = new List<CampaignMember>();
        for (Integer i = 0; i < NUM_CONTACTS; i++) {
            campaignMembers.add(new CampaignMember(CampaignId = campaign.Id, ContactId = contacts[i].Id));
            campaignMembers2.add(new CampaignMember(CampaignId = campaign2.Id, ContactId = contacts[i].Id));
        } 
        lallMember.addAll(campaignMembers);
        lallMember.addAll(campaignMembers2);
        insert lallMember;
    }
    
    /** 
    *   Test method : testMassCreationGratisItemsCon method covers the logic and exspected results (using testData) 
    *   @category Testing Method
    */
    public static testMethod void testMassCreationGratisItemsUrlRedirector(){
        //Init test Data
        setupTestData();
        String proId = [select Id from Profile where Name Like '%Non-Integration%' limit 1].Id;
        User non_integrationUser = new User(alias = 'u111', email='test1111@test.com', emailencodingkey='UTF-8',
                            languagelocalekey='en_US', localesidkey='en_US', FirstName = 'test1111' , lastname='U11', 
                            profileid = proId, timezonesidkey='America/Los_Angeles', CommunityNickname = 'u111',
                            isActive = true, username='testusername1111@test.com'); 
        insert non_integrationUser;                      
        Test.startTest();
        	//Run as admin
        	String urlReturn1 = MassCreationGratisItemsUrlRedirector.urlRedirector(campaign.Id);
        	//Redirect to Record Type selection Page
        	System.assertEquals('/setup/ui/recordtypeselect.jsp?ent=01I200000000rQR&retURL=%2F'+campaign.Id+'&save_new_url=%2Fapex%2FGratisFromTargetProducts?Id=' + campaign.Id, urlReturn1);
        	
        	//Run as Non-Integration User
            System.runAs(non_integrationUser){
            	String urlReturn = MassCreationGratisItemsUrlRedirector.urlRedirector(campaign.Id);
            	String dropRecTypeId = [Select Id From RecordType where DeveloperName = 'GratisItemDrops' limit 1].Id;
            	//Redirect to Mass create gratis page
            	System.assertEquals('/apex/GratisFromTargetProducts?Id='+campaign.Id+'&RecordType='+dropRecTypeId+'&ent=01I200000000rQR', urlReturn);
            }
        Test.stopTest();
    }
    
    /** 
    *   Test method : testMassCreationGratisItemsCon method covers the logic and exspected results (using testData) 
    *   @category Testing Method
    */
    public static testMethod void testMassCreationGratisItemsCon(){
        //Init test Data
        setupTestData();
        
        Test.startTest();
            //***Page Run as All Members Mode
            PageReference pg = Page.GratisFromTargetProducts;
            pg.getParameters().put(MassCreationGratisItemsCon.PARAM_RECORDTYPE, recTypeId_GratisItemOrders);
            Test.setCurrentPageReference(pg);
            ApexPages.StandardController standCon = new ApexPages.StandardController(campaign);
            MassCreationGratisItemsCon controller = new MassCreationGratisItemsCon(standCon);
            controller.autoCreateGratisStatusToMember();
            controller.gratisItemMgr.selectedTargetProducts = new List<String>();
            controller.gratisItemMgr.retrieveSentCampaignMemberInOtherCampaign('', 0);
            controller.createGratisType = MassCreationGratisItemsCon.CREATE_GRATIS_TYPE_ALL_MEMBERS;
            controller.mainMenuSelection();
            
            //Getter methods called from page : Not Business Logic
            controller.getCreateGratisTypeAllMembersId();
            controller.getCreateGratisTypeAddBatchId();
            controller.getCreateGratisTypeMemberStatus();
            controller.getTargetProducts();
            
            //All target products of campaign are selected
            controller.selectedTargetProducts = new List<String>{campaign.Target_Product__c , campaign.Target_Product_2__c, campaign.Target_Product_3__c, 
                                                                campaign.Target_Product_4__c, campaign.Target_Product_5__c, campaign.Target_Product_6__c};
            controller.pollingBy = controller.CREATE_ALL_STEP1;
            //Create Gratis Item with all target products selected and All members
            controller.createGratis();
            controller.pollReviewList();
            List<Gratis_Item__c> lgi = [Select RecordTypeId, Contact__c, (Select Title__c From Gratis_Line_Items__r) From Gratis_Item__c Where Campaign__c =: campaign.Id];
            //Test: number of Member equals to number of created Gratis Items
            System.assertEquals(campaignMembers.size() , lgi.size());
            Map<String, Gratis_Item__c> mCreatedGi = new Map<String, Gratis_Item__c>();
            for(Gratis_Item__c gi : lgi){
                mCreatedGi.put(gi.Contact__c, gi);
            }
            //Test: every member has its own Gratis item and line Items
            Set<String> createdLineItemProduct = new Set<String>(controller.selectedTargetProducts);
            Set<String> cpmbIds = new Set<String>();
            for(CampaignMember cpmb : campaignMembers){
                cpmbIds.add(cpmb.Id);
                System.assert(mCreatedGi.containsKey(cpmb.ContactId),'Member must has its Gratis Item');
                Gratis_Item__c gi = mCreatedGi.get(cpmb.ContactId);
                System.assert(gi.RecordTypeId == recTypeId_GratisItemOrders ,'Gratis Item Record Type must be Order ');
                for(Gratis_Line_Items__c gli : gi.Gratis_Line_Items__r){
                    System.assert(createdLineItemProduct.contains(gli.Title__c),'Products must be created follow by selected products');
                }
            }
            
            //Test: every member status update to 'Gratis Sent'
            for(CampaignMember cpmb : [Select Status From CampaignMember Where Id IN: cpmbIds]){
                System.assertEquals(MassCreationGratisItemsCon.MEMBER_STATUS_TOUPDATE, cpmb.Status);
            }
            
            //***Page Run as Selected Mode && By Status Mode 
            PageReference pg2 = Page.GratisFromTargetProducts;
            pg2.getParameters().put(MassCreationGratisItemsCon.PARAM_RECORDTYPE, recTypeId_GratisItemDrops);
            Test.setCurrentPageReference(pg2);
            ApexPages.StandardController standCon2 = new ApexPages.StandardController(campaign2);
            MassCreationGratisItemsCon controller2 = new MassCreationGratisItemsCon(standCon2);
            controller2.autoCreateGratisStatusToMember();
            controller2.createGratisType = MassCreationGratisItemsCon.CREATE_GRATIS_TYPE_MEMBERS_STATUS;
            controller2.mainMenuSelection();
            
            //Set Input filter status = 'Sent'
            controller2.snapshotCMTP = controller2.getSnapshotCMTP();
            controller2.statusValue = 'Sent';
            controller2.doSearchByStatus();
            BatchCheckHelper.isAvailableDML(10);
            //Mover Methods, and display data called from page : Not Business Logic
            controller2.lvwNavigateNext();
            controller2.lvwNavigatePrevious();
            controller2.rolStateDefined();
            controller2.getNextPageNotifier();
            controller2.getDisplayNavigator();
            controller2.getIsPolling();
            controller2.snapshotCMTP.get(0).getDisplayName();
            controller2.snapshotCMTP.get(0).getTargetProductSize();
            controller2.snapshotCMTP.get(0).getTargetProduct1();
            controller2.snapshotCMTP.get(0).getTargetProduct2();
            controller2.snapshotCMTP.get(0).getTargetProduct3();
            controller2.snapshotCMTP.get(0).getTargetProduct4();
            controller2.snapshotCMTP.get(0).getTargetProduct5();
            controller2.snapshotCMTP.get(0).getTargetProduct6();
            
            List<MassCreationGratisItemsCon.SelectableCMTP> snapshotCMTP = controller2.snapshotCMTP;
            List<CampaignMember> selectedCmpms = new List<CampaignMember>();
            Set<String> createdLineItemProduct2 = new Set<String>();
            //Select Product 1 and product 2 for display page 1
            for(MassCreationGratisItemsCon.SelectableCMTP rec : snapshotCMTP){
                selectedCmpms.add(rec.campaignMember);
                rec.targetProducts.get(1).isSelected = true;
                rec.targetProducts.get(2).isSelected = true;
                createdLineItemProduct2.add(rec.targetProducts.get(1).Id);
                createdLineItemProduct2.add(rec.targetProducts.get(2).Id);
            }
            
            //Create Gratis Item with selected target products of CampaignMember 1 and 2
            controller2.createGratis();
            
            //Create Gratis Item All members step2 same process to Selected Mode : the result will check on controller2
            controller.createGratis();
            
            List<Gratis_Item__c> lgi2 = [Select RecordTypeId, Contact__c, (Select Title__c From Gratis_Line_Items__r) From Gratis_Item__c Where Campaign__c =: campaign2.Id];
            //Test: number of selectedCmpms equals to number of created Gratis Items
            System.assertEquals(selectedCmpms.size() , lgi2.size());
            Map<String, Gratis_Item__c> mCreatedGi2 = new Map<String, Gratis_Item__c>();
            for(Gratis_Item__c gi : lgi2){
                mCreatedGi2.put(gi.Contact__c, gi);
            }
            //Test: every selectedCmpms has its own Gratis item and line Items
            Set<String> cpmbIds2 = new Set<String>();
            for(CampaignMember cpmb : selectedCmpms){
                cpmbIds2.add(cpmb.Id);
                System.assert(mCreatedGi2.containsKey(cpmb.ContactId),'Member must has its Gratis Item');
                Gratis_Item__c gi = mCreatedGi2.get(cpmb.ContactId);
                System.assert(gi.RecordTypeId == recTypeId_GratisItemDrops ,'Gratis Item Record Type must be Order ');
                for(Gratis_Line_Items__c gli : gi.Gratis_Line_Items__r){
                    System.assert(createdLineItemProduct2.contains(gli.Title__c),'Products must be created follow by selected products');
                }
            }
            
            //Test: every member status update to 'Gratis Sent'
            for(CampaignMember cpmb : [Select Status From CampaignMember Where Id IN: cpmbIds2]){
                System.assertEquals(MassCreationGratisItemsCon.MEMBER_STATUS_TOUPDATE, cpmb.Status);
            }
            
        Test.stopTest();
    }
    
    /** 
    *   Test method : testMassCreationGratisItemsCon method covers the logic and exspected results (using testData) 
    *   @category Testing Method
    */
    public static testMethod void testMassCreationGratisItemsCon_Batch_Apex(){
        //Init test Data
        setupTestData();
        
        Test.startTest();
            
            //***Page Run as Selected Mode FORCE_BATCH
            PageReference pg2 = Page.GratisFromTargetProducts;
            pg2.getParameters().put(MassCreationGratisItemsCon.PARAM_RECORDTYPE, recTypeId_GratisItemDrops);
            pg2.getParameters().put(MassCreationGratisItemsCon.PARAM_FORCE_BATCH, '1');
            Test.setCurrentPageReference(pg2);
            ApexPages.StandardController standCon2 = new ApexPages.StandardController(campaign2);
            MassCreationGratisItemsCon controller2 = new MassCreationGratisItemsCon(standCon2);
            controller2.autoCreateGratisStatusToMember();
            controller2.snapshotCMTP = controller2.getSnapshotCMTP();
            controller2.createGratisType = MassCreationGratisItemsCon.CREATE_GRATIS_TYPE_ALL_MEMBERS;
            controller2.mainMenuSelection();
            
            //Select Product 1 and product 2 for display page 1
            controller2.selectedTargetProducts.add(campaign2.Target_Product__c);
            controller2.selectedTargetProducts.add(campaign2.Target_Product_2__c);
            
            //Create Gratis Item with selected target products of CampaignMember 1 and 2
            controller2.createGratis();
            controller2.pollingBy = controller2.CREATE_ALL_STEP1;
            controller2.pollReviewList();
            
        Test.stopTest();
        
        List<Gratis_Item__c> lgi2 = [Select RecordTypeId, Contact__c, (Select Title__c From Gratis_Line_Items__r) From Gratis_Item__c Where Campaign__c =: campaign2.Id];
        //Test: number of campaignMembers2 equals to created Gratis Items 
        System.assertEquals(campaignMembers2.size() , lgi2.size());
        Map<String, Gratis_Item__c> mCreatedGi2 = new Map<String, Gratis_Item__c>();
        for(Gratis_Item__c gi : lgi2){
            mCreatedGi2.put(gi.Contact__c, gi);
        }
        
        //Test: every member status update to 'Gratis Sent'
        for(CampaignMember cpmb : [Select Status From CampaignMember Where ContactId IN: mCreatedGi2.keyset() And CampaignId =: campaign2.Id ]){
            System.assertEquals(MassCreationGratisItemsCon.MEMBER_STATUS_TOUPDATE, cpmb.Status);
        }
    }
}