@isTest
private class CreateBooksUsedConTests {
    
// Test Contoller
        
    public static testMethod void getQuery() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        createBooksUsedCon.getQuery();  
    }
    
    public static testMethod void saveTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        createBooksUsedCon.save();  
    }
    
    public static testMethod void cancelTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        createBooksUsedCon.cancel();
    }   
        
    public static testMethod void saveInternalTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        System.assert(createBooksUsedCon.saveInternal() == false);
    }
        
    public static testMethod void getRenderSearchResultsTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        Boolean status = createBooksUsedCon.getRenderSearchResults();
        System.assert(status == false);
    }
    
    public static testMethod void initialSearchingCriteriasTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        List<SearchingCriteria> criterias = createBooksUsedCon.getSearchingCriterias();   
        System.assertEquals(5, criterias.size());               // ensure that's 5 empty criterias loaded
        System.assertEquals('Name', criterias.get(0).field);    // ensure first criteria default to book title field
        System.assertEquals('Name', criterias.get(1).field);    // ensure first criteria default to ISBN field  
    }
    
    public static testMethod void getFieldsTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        List<SelectOption> selectOption = createBooksUsedCon.getFields();
        System.assert(selectOption != null);
    }
        
    public static testMethod void getLookupProductsTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        List<CreateBooksUsedCon.SelectableProduct> lookupProducts = createBooksUsedCon.getLookupProducts();     
        System.assert(lookupProducts != null);
    }
    
    public static testMethod void getSearchingCriteriasTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        List<SearchingCriteria> searchingCriterias = createBooksUsedCon.getSearchingCriterias();        
        System.assert(searchingCriterias != null);
    }
    
    public static testMethod void searchByFieldTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        System.assert(createBooksUsedCon.searchByField() == null);
    }
    
    public static testMethod void searchTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        createBooksUsedCon.getPricebooks();
        createBooksUsedCon.Search();
        createBooksUsedCon.searchField = 'search_byName';
        createBooksUsedCon.Search();
        createBooksUsedCon.searchField = 'search_byISBN';
        createBooksUsedCon.Search();
        createBooksUsedCon.searchField = 'search_byPublisher';
        createBooksUsedCon.Search();      
         createBooksUsedCon.searchField = 'search_byMarket';
        createBooksUsedCon.Search(); 
         createBooksUsedCon.searchField = 'search_bySegment';
        createBooksUsedCon.Search(); 
          createBooksUsedCon.searchField = 'search_bySeries';
        createBooksUsedCon.Search(); 
                  
    }
        
    public static testMethod void addSelectedProductsTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        createBooksUsedCon.addSelectedProducts();
    }
    
    public static testMethod void getPageTitleTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        String pageTitle = createBooksUsedCon.getPageTitle();
        System.assert(pageTitle.length() > 0);
    }
    
    public static testMethod void removeSelectedProductTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        createBooksUsedCon.removeSelectedProduct();
    }
    
    public static testMethod void cascadeAdoptionDateTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        PageReference page = createBooksUsedCon.cascadeAdoptionDate();
        System.assert(page == null);
    }
    
    public static testMethod void cascadeRenewalDateTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        PageReference page = createBooksUsedCon.cascadeRenewalDate();
        System.assert(page == null);
    }
    
    public static testMethod void cascadePurchaseDateTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        PageReference page = createBooksUsedCon.cascadePurchaseDate();
        System.assert(page == null);
    }
    
    public static testMethod void contollerWithoutParameters() {
        CreateBooksUsedCon createBooksUsedCon = preparePageWithoutParameters();
        System.assert(createBooksUsedCon != null);
    }
        
    public static testMethod void contollerWithAccountParameters() {
        CreateBooksUsedCon createBooksUsedCon = preparePageWithAccountParameters();
        System.assert(createBooksUsedCon != null);
        
        createBooksUsedCon.resetSearchingCriterias();
        List<SearchingCriteria> criterias = createBooksUsedCon.getSearchingCriterias();
        System.assertEquals(criterias.size(), 5);
        
        System.assertEquals(criterias.get(0).index, 1);
        System.assertEquals(criterias.get(0).field, 'Name');
        System.assertEquals(criterias.get(0).operator, 'c');
        System.assertEquals(criterias.get(0).logic, 'AND');
        System.assertEquals(criterias.get(0).value, '');
        
        System.assertEquals(criterias.get(1).index, 2);
        System.assertEquals(criterias.get(1).field, 'Name');
        System.assertEquals(criterias.get(1).operator, 'c');
        System.assertEquals(criterias.get(1).value, '');          
             createBooksUsedCon.selectedPricebook = 'None';
        createBooksUsedCon.Search();
        List<CreateBooksUsedCon.SelectableProduct> lookupProducts = createBooksUsedCon.getLookupProducts();
        
        for (CreateBooksUsedCon.SelectableProduct product : lookupProducts) {
            System.assertEquals(product.isSelected, false);
            product.isSelected = true;
            System.assertEquals(product.isSelected, true);
            product.isSelected = false;
            System.assertEquals(product.isSelected, false);
        }
        
        System.assert(lookupProducts.size() >= 1, 'Please create at least 1 Account(Insitution) for testing');
        lookupProducts.get(0).isSelected = true;
        
        createBooksUsedCon.addSelectedProducts();
        List<CreateBooksUsedCon.BooksUsed> selectedProduct = createBooksUsedCon.getSelectedProducts();
        
        System.assertEquals(1 + 10 /* CreateBooksUsedCon.NO_OF_PRELOAD_ITEMS */, selectedProduct.size()); // check the list size should be 1 only
        System.assertEquals(lookupProducts.get(0).product.id, selectedProduct.get(0).getBooksUsed().Book_Used__c); // ensure both object refer to the same instance
        
        createBooksUsedCon.addSelectedProducts();
        System.assertEquals(1 + 10 /* CreateBooksUsedCon.NO_OF_PRELOAD_ITEMS */, selectedProduct.size()); // check the list size should be 1 only
        
        selectedProduct.get(0).needSave();
        selectedProduct.get(0).validate();
        
        // new added item will appear at the end of the list
        Books_Used__c lineItem = selectedProduct.get(0).getBooksUsed();
        lineItem.Number_used__c = 1;
        lineItem.Adoption_Date__c = System.today();
        lineItem.Renewal_Date__c = System.today() + 1;
        
        String nextPage = createBooksUsedCon.save().getUrl();
        nextPage = createBooksUsedCon.cancel().getUrl();        
    }
    
// Test BooksUsed
    
    public static testMethod void booksUsedPreLoadItemsTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        Test.startTest();
        testPreLoadItems(createBooksUsedCon);
        Test.stopTest();
    }
    
    public static testMethod void booksUsedTest() {
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        Books_Used__c bu = createBooksUsed();
        CreateBooksUsedCon.BooksUsed booksUsed01 = new CreateBooksUsedCon.BooksUsed(null, bu);
        System.assert(!booksUsed01.needSave());
                
        CreateBooksUsedCon.BooksUsed booksUsed02 = new CreateBooksUsedCon.BooksUsed(getBookTitle(), bu);
        System.assert(booksUsed02.needSave());      
    }
    
    public static testMethod void createBooksUsedTest() {       
        Books_Used__c booksUsed = createBooksUsed();
        System.assert(booksUsed != null);       
    }
        
// Test SelectableProduct

    public static testMethod void selectableProductTest() {
        Product2 p = new Product2();
        CreateBooksUsedCon.SelectableProduct sp = new CreateBooksUsedCon.SelectableProduct(p);
        sp.isSelected = true;
        System.assert(true, sp.Product === p);
        System.assert(true, sp.isSelected); 
    }
    
// Test SearchingCriteria
    
    public static testMethod void searchingCriteriaTest01() {        
        SearchingCriteria s0 = new SearchingCriteria();
        SearchingCriteria s1 = new SearchingCriteria(2, false, 'name', 'e', 'John');
        SearchingCriteria s2 = new SearchingCriteria(3, false, 'id', 's', 'ABC12', 'OR'); 
        SearchingCriteria s3 = new SearchingCriteria(4);
        SearchingCriteria s4 = new SearchingCriteria(5, true, 'id', 'q', 'CBA321', 'AND');       
        
        List<SearchingCriteria> criterias = new SearchingCriteria[]{s0, s1, s2, s3, s4};
        
        String query = SearchingCriteria.formWhereClause(criterias);  
            
        System.assertEquals(' WHERE (name LIKE \'%John\' AND ( id LIKE \'ABC12%\' OR id = \'CBA321\'))', query);  
    }
    
    public static testMethod void searchingCriteriaTest02() {        
        // Test - Incomplete criterias
        SearchingCriteria s0 = new SearchingCriteria();
        SearchingCriteria s1 = new SearchingCriteria(2, false, 'name', 'e', 'John');
        SearchingCriteria s2 = new SearchingCriteria(3, false, 'id', 's', 'ABC12', 'OR');         
        SearchingCriteria s4 = new SearchingCriteria(4, true, 'id', 'q', 'CBA321', 'AND'); 
        SearchingCriteria s3 = new SearchingCriteria(5);      
        
        List<SearchingCriteria> criterias = new SearchingCriteria[]{s0, s1, s2, s3, s4};
        
        String query = SearchingCriteria.formWhereClause(criterias);  
            
        System.assertEquals(' WHERE (name LIKE \'%John\' AND ( id LIKE \'ABC12%\' OR id = \'CBA321\'))', query);  
    }
    
    public static testMethod void searchingCriteriaTest03() {        
        // Test - Advanced Filter Conditions
        SearchingCriteria s0 = new SearchingCriteria();
        SearchingCriteria s1 = new SearchingCriteria(2, false, 'name', 'e', 'John');
        SearchingCriteria s2 = new SearchingCriteria(3, false, 'id', 's', 'ABC12', 'OR');         
        SearchingCriteria s4 = new SearchingCriteria(4, true, 'id', 'q', 'CBA321', 'AND'); 
        SearchingCriteria s3 = new SearchingCriteria(5);      
        
        List<SearchingCriteria> criterias = new SearchingCriteria[]{s0, s1, s2, s3, s4};
        
        String query = SearchingCriteria.formWhereClause(criterias, '2 AND (3 OR 4)');  
            
        System.assertEquals(' WHERE (name LIKE \'%John\' AND (id LIKE \'ABC12%\' OR id = \'CBA321\'))', query);  
    }
    
    public static testMethod void searchingCriteriaWhereClauseTest() {
        String f = 'Name';
        String v = 'ABC';
        
        System.assertEquals('Name = \'ABC\'', SearchingCriteria.getWhereClause(f,'q',v).builderWhereClause());
        System.assertEquals('Name != \'ABC\'', SearchingCriteria.getWhereClause(f,'n',v).builderWhereClause());
        System.assertEquals('Name LIKE \'ABC%\'', SearchingCriteria.getWhereClause(f,'s',v).builderWhereClause());
        System.assertEquals('Name LIKE \'%ABC\'', SearchingCriteria.getWhereClause(f,'e',v).builderWhereClause());
        System.assertEquals('Name LIKE \'%ABC%\'', SearchingCriteria.getWhereClause(f,'c',v).builderWhereClause());
        System.assertEquals('(NOT Name LIKE \'%ABC%\')', SearchingCriteria.getWhereClause(f,'k',v).builderWhereClause());
        System.assertEquals('Name IN(ABC)', SearchingCriteria.getWhereClause(f,'u',v).builderWhereClause());
        System.assertEquals('Name NOT IN(ABC)', SearchingCriteria.getWhereClause(f,'x',v).builderWhereClause());
    }
    
// Test General
    
    public static testMethod void getBookTitleTest() {
        Test.startTest();
        Product2 product2 = getBookTitle();
        Test.stopTest();
    }
    
// Data Preparation

    private static CreateBooksUsedCon preparePageWithoutParameters() {      
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        System.assert(createBooksUsedCon != null);
        return createBooksUsedCon;
    }
    
    private static CreateBooksUsedCon preparePageWithAccountParameters() {
        // Add parameters to page URL
        System.currentPageReference().getParameters().put('Account_Id' /*CreateBooksUsedCon.ACCOUNT_ID_PARAM*/, getInstitution().Id);
            
        CreateBooksUsedCon createBooksUsedCon = new CreateBooksUsedCon();
        System.assert(createBooksUsedCon != null);
        return createBooksUsedCon;
    }
    
    private static Books_Used__c createBooksUsed() {
        Books_Used__c booksUsed = new Books_Used__c(Institution__c = getInstitution().Id);
        System.assert(booksUsed != null, 'Please create at least 1 BookUsed for testing');
        return booksUsed;
    }
    
    private static Account getInstitution() {
        Account[] accounts = [select Id from Account limit 1];
        System.assert(accounts.size() >= 1, 'Please create at least 1 Account(Insitution) for testing');
        return accounts[0];
    }
    
    private static Product2 getBookTitle() {
        Product2[] products = [select Id from Product2 limit 1];
        System.assert(products.size() >= 1, 'Please create at least 1 Product(Book Title) for testing');
        return products[0];
    }
    
    private static void testPreLoadItems(CreateBooksUsedCon controller) {
        List<CreateBooksUsedCon.BooksUsed> booksUsed = controller.getSelectedProducts();
        for (CreateBooksUsedCon.BooksUsed bu : booksUsed) {
            if (bu.getIsPreLoad() == true) {
                System.assertEquals(null, bu.ISBN);
                System.assertEquals(null, bu.getBookTitle());               
            }
        }       
    }
}