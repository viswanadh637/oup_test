trigger warehouseUserValidation on Warehouse_User__c (before insert, before update) 
{

// verify uniqueness of the user to warehouse relation
if (Trigger.isInsert)
{
    for (Warehouse_User__c w : Trigger.new)
    {
        if (WarehouseManagement.userHasWarehouse(w.User__c))
        {
        Warehouse__c wexisting = WarehouseManagement.getWarehouseByUser(w.User__c);
        w.User__c.addError('This user is already assigned to a Warehouse ID: '+wexisting.id);
        }
        else
        {
        //manual sharing on adding a additional user to Warehouse 
        Warehouse__Share ws = new Warehouse__Share();
        ws.ParentId = w.Warehouse__c;
        ws.UserOrGroupId = w.User__c;
        ws.AccessLevel ='Edit';
        database.insert(ws);

        }
        
    }
    
    
    
}
//end is insert

// verify uniqueness of the user to warehouse relation
if (Trigger.isUpdate)
{
    for (Warehouse_User__c w : Trigger.new)
    {
        if (WarehouseManagement.userHasWarehouse(w.User__c))
        {
        Warehouse__c wexisting = WarehouseManagement.getWarehouseByUser(w.User__c);
            if (wexisting.id <> w.Warehouse__c)
            {
            w.User__c.addError('This user is already assigned to a Warehouse ID: '+wexisting.id);
            }
        }
    }
}

}