trigger afterUpdate on Warehouse_Document__c (after update) {

For (Warehouse_Document__c wd: trigger.new)
{

//ensure the change is in the status field.
If(trigger.oldMap.get(wd.id).Status__c!= 'Approved' && wd.Status__c == 'Approved' && wd.RecordTypeId != [Select Id from RecordType where DeveloperName = 'Gratis'][0].Id)
{

Warehouse__c wh = [Select id, Warehouse_Owner__c from Warehouse__c where id=:wd.Warehouse__c];


List<Warehouse_Document_Line__c> documentList = new List<Warehouse_Document_Line__c>();
documentList =  [select id, WD_book_title__c, quantity__c from Warehouse_Document_Line__c where Document__c =:wd.id];

//inventory
if(wd.RecordTypeId ==  [Select Id from RecordType where DeveloperName = 'Inventory'][0].Id)
{
 WarehouseManagement.updateStock(wh.Warehouse_Owner__c ,documentList ,'update');
}
//receipt
if(wd.RecordTypeId == [Select Id from RecordType where DeveloperName = 'Receipt'][0].Id)
{
 WarehouseManagement.updateStock(wh.Warehouse_Owner__c ,documentList ,'add');
}
//receipt
if(wd.RecordTypeId == [Select Id from RecordType where DeveloperName = 'Shipment'][0].Id)
{
 WarehouseManagement.updateStock(wh.Warehouse_Owner__c ,documentList ,'substract');
 
 if (wd.Type__c == 'Transfer - out')
 {
  WarehouseManagement.createTransfer(wd, documentList);
 }
 
}
}
//end Approved check
}
//end loop

}