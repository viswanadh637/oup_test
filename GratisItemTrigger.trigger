trigger GratisItemTrigger on Gratis_Item__c (before delete) {
 //Definitions
   ID ownerId;
   ID sortingId;
   ID oldsortingId;
   List<Warehouse_Document__c> wdlist;
   Warehouse_Document__c wd;
   List<Warehouse_Document_Line__c> createList = new List<Warehouse_Document_Line__c>();
   Warehouse__c w;
   Map<ID, ID> gratisItemsMapForUpdate = new Map<ID, ID>();
   //only delete because only then lines are affected
   if (Trigger.IsDelete)
   {
       Set<ID> ids = Trigger.oldMap.keySet();
       List<Gratis_Line_Items__c> glis = 
       [SELECT Id, Date_Sent_Left__c,Title__c, Quantity__c, Related_Gratis_Item__c, Related_Gratis_Item__r.Id, Related_Gratis_Item__r.RecordTypeId, Related_Gratis_Item__r.OwnerId, Related_Gratis_Item__r.Warehouse_Document__c FROM Gratis_Line_Items__c 
       WHERE Related_Gratis_Item__c in :ids and Related_Gratis_Item__r.Warehouse_Document__c != null 
       and Related_Gratis_Item__r.Warehouse_Document__r.Status__c != 'Approved' //select only those that are not approved
       and Related_Gratis_Item__r.RecordTypeId != '012200000000Wit' //select only drops 
       ORDER BY Related_Gratis_Item__r.Warehouse_Document__c]; //order by doc id
       for (Gratis_Line_Items__c gli: glis)
       {
           //checking the owner
           sortingId = gli.Related_Gratis_Item__r.Warehouse_Document__c;
           ownerId = gli.Related_Gratis_Item__r.OwnerId;
           //managing multiple owners in bulk create
           if (oldsortingId==null || sortingId != oldsortingId)
           {
                //update what was until now
                if (oldsortingId!=null)
                {
                WarehouseManagement.updateDocument(ownerId,createList,'add', wd);
               // WarehouseManagement.updateStock(ownerId,createList,'substract');
                createList =  new List<Warehouse_Document_Line__c>();
                }
                wdlist = [select id, RecordType.Name, Warehouse__r.OwnerId from Warehouse_Document__c where id=:sortingId];
                oldsortingId = sortingId;
                wd = wdlist[0];
           }
           
           if (wd != null)
           {
               //move lines and update stock
               Warehouse_Document_Line__c wdl = new Warehouse_Document_Line__c();
               wdl.Document__c = wd.id;
               wdl.WD_book_title__c = gli.Title__c;
               wdl.Quantity__c = 0 - gli.Quantity__c;
               createList.Add(wdl);
           }
       }
         //end iteration through trigger OLD
         if (wd != null)
           {
           WarehouseManagement.updateDocument(ownerId,createList,'add', wd);
           //WarehouseManagement.updateStock(ownerId,createList,'substract');
           createList =  new List<Warehouse_Document_Line__c>();
           }
   }
   //end is delete
   
   
}