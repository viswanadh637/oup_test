trigger triggerUpdateStockAfterShipment on Warehouse_Document_Line__c (before delete, after update, after insert) {
//NOT AFTER SHIPMENT - DOCUMENT NAME CHANGED TO 'GRATIS' 
//definitions
ID sortingId;//sorting by Owner to update stock - there has to be stock as there is an Warehouse containing warehouse doc with lines.
ID oldsortingId;
Set<Id> newIds = new Set<Id>();
Set<Id> oldIds = new Set<Id>();
List<Warehouse_Document_Line__c> createList = new List<Warehouse_Document_Line__c>();
//end definitions
    if (Trigger.IsDelete)
    {
    oldIds = Trigger.oldMap.KeySet();
    List<Warehouse_Document_Line__c> wdllist = 
    [SELECT Id, Document__r.Status__c, Document__r.Warehouse__r.Warehouse_Owner__c, Document__r.CreatedDate, Document__r.RecordType.Name, Document__c, WD_book_title__c, Quantity__c 
    FROM Warehouse_Document_Line__c WHERE id in :oldIds and Document__r.Status__c != 'Approved' and Document__r.RecordType.Name = 'Gratis' order by Document__r.Warehouse__r.Warehouse_Owner__c];
    //select all wdls from not approved Shipments that were deleted
     for (Warehouse_Document_Line__c gli: wdllist)
       {
           //checking the owner
           sortingId = gli.Document__r.Warehouse__r.Warehouse_Owner__c;
           //managing multiple owners in bulk delete
           if (oldsortingId==null || sortingId != oldsortingId)
           {
                //update what was until now
                if (oldsortingId!=null)
                {
                WarehouseManagement.updateStock(oldsortingId,createList,'add');
                createList =  new List<Warehouse_Document_Line__c>();
                }
           }
           oldsortingId = sortingId;
           
           //update stock
           Warehouse_Document_Line__c wdl = new Warehouse_Document_Line__c();
           wdl.WD_book_title__c = gli.WD_book_title__c;
           system.debug(gli.Quantity__c);
           wdl.Quantity__c = gli.Quantity__c;
           createList.Add(wdl);
       }
       //end iteration through trigger old
    
    //Add error to those that are deleted after Approval    
    //Get the list of deleted Warehouse_Document_Line__c
    wdllist = [SELECT Id FROM Warehouse_Document_Line__c WHERE id in :oldIds and Document__r.Status__c = 'Approved'];
    For (Warehouse_Document_Line__c wdl: wdllist )
    {
       //Getting the actual record from the Trigger - workaround of the error 'SObject row does not allow errors'.
        Warehouse_Document_Line__c actualRecord = Trigger.oldMap.get(wdl.Id);
        actualRecord.addError('Cannot delete or edit line of an approved document');
    }
    //end iteration
}
//end isDelete  
   
   
    if (Trigger.IsUpdate)
    {
    newIds = Trigger.newMap.KeySet();
    List<Warehouse_Document_Line__c> wdllist = 
    [SELECT Id, Document__r.Status__c, Document__r.Warehouse__r.Warehouse_Owner__c, Document__r.CreatedDate, Document__r.RecordType.Name, Document__c, WD_book_title__c, Quantity__c 
    FROM Warehouse_Document_Line__c 
    WHERE id in :newIds 
    and Document__r.Status__c != 'Approved' //not approved
    and Document__r.RecordType.Name = 'Gratis' //that is Gratis doc
    order by Document__r.Warehouse__r.Warehouse_Owner__c];
    //select all wdls from not approved Shipments that were updated
     for (Warehouse_Document_Line__c gli: wdllist)
       {
           //checking the owner 
           sortingId = gli.Document__r.Warehouse__r.Warehouse_Owner__c;
           //managing multiple owners in bulk update
           if (oldsortingId==null || sortingId != oldsortingId)
           {
                //update what was until now
                if (oldsortingId!=null)
                {
                WarehouseManagement.updateStock(oldsortingId,createList,'add');
                createList =  new List<Warehouse_Document_Line__c>();
                }
           }
           oldsortingId = sortingId;
           
           //update stock
           Warehouse_Document_Line__c wdl = new Warehouse_Document_Line__c();
           Warehouse_Document_Line__c wdlOld = Trigger.oldMap.get(gli.id);
           wdl.WD_book_title__c = gli.WD_book_title__c;
           system.debug(gli.Quantity__c);
           wdl.Quantity__c = wdlOld.Quantity__c - gli.Quantity__c;
           createList.Add(wdl);
       }
        //end iteration through trigger new
    }
    //end isUpdate
    
     if (Trigger.IsInsert)
    {
    newIds = Trigger.newMap.KeySet();
    List<Warehouse_Document_Line__c> wdllist = 
    [SELECT Id, Document__r.Status__c, Document__r.Warehouse__r.Warehouse_Owner__c, Document__r.CreatedDate, Document__r.RecordType.Name, Document__c, WD_book_title__c, Quantity__c 
    FROM Warehouse_Document_Line__c 
    WHERE id in :newIds 
    and Document__r.Status__c != 'Approved' //not approved
    and Document__r.RecordType.Name = 'Gratis' //that is gratis
    order by Document__r.Warehouse__r.Warehouse_Owner__c];
    //select all wdls from not approved Shipments that were inserted
     for (Warehouse_Document_Line__c gli: wdllist)
       {
           //checking the owner 
           sortingId = gli.Document__r.Warehouse__r.Warehouse_Owner__c;
           //managing multiple owners in bulk update
           if (oldsortingId==null || sortingId != oldsortingId)
           {
                //update what was until now
                if (oldsortingId!=null)
                {
                WarehouseManagement.updateStock(oldsortingId,createList,'add');
                createList =  new List<Warehouse_Document_Line__c>();
                }
           }
           oldsortingId = sortingId;
           
           //update stock
           Warehouse_Document_Line__c wdl = new Warehouse_Document_Line__c();
           wdl.WD_book_title__c = gli.WD_book_title__c;
           system.debug(gli.Quantity__c);
           wdl.Quantity__c = 0-gli.Quantity__c;
           createList.Add(wdl);
       }
        //end iteration through trigger new
        
        
    //Add error to those that are inserted after Approval    
    //Get the list of deleted Warehouse_Document_Line__c
    wdllist = [SELECT Id FROM Warehouse_Document_Line__c WHERE id in :newIds and Document__r.Status__c = 'Approved'];
    For (Warehouse_Document_Line__c wdl: wdllist )
    {
       //Getting the actual record from the Trigger - workaround of the error 'SObject row does not allow errors'.
        Warehouse_Document_Line__c actualRecord = Trigger.oldMap.get(wdl.Id);
        actualRecord.addError('Cannot delete or edit line of an approved document');
    }
    //end iteration
    }
    //end isCreate
        system.debug(createList.size());
    if (createList.size() > 0)
    {
    system.debug(oldsortingId);
    //update stock after leaving iterations
    WarehouseManagement.updateStock(oldsortingId,createList,'add');
    createList =  new List<Warehouse_Document_Line__c>();
    } 
       
    }