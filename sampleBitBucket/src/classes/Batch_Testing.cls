global class Batch_Testing implements Database.Batchable<SObject>
    {
        global final string query;
        global final string s_object;
        global final string field;
        global final string field_value;
        
        global Batch_Testing(String q, String s, String f, String v)
        {
            query=q;
            s_object=s;
            field=f;
            field_value=v;
        }
global Database.QueryLocator Start(Database.BatchableContext BC)
    {
         return Database.getQueryLocator(query);
    }
global void Execute(Database.BatchableContext BC, List<SObject> scope)
    {
        for(SObject o:scope)
        {
            o.put(field,field_value);
        }
        update scope;
    }
global void Finish(Database.BatchableContext BC)
    {
    
    }
}