@isTest(SeeAllData=true)
private class GratisCloningConTest{
  
  public static testMethod void testWithErrorsInURL() {
        Test.setCurrentPageReference(Page.GratisCloning);
        //test missing GRATIS_ID_PARAM
        GratisCloningCon controller = new GratisCloningCon();
        System.assert(!controller.getIsInitSuccess(), 'Failed to identify that process can not be executed');
    }
    
     public static testMethod void test()
    {
        createInstitution();
        createInstitutionContacts();       
        createProducts();
        
        createGratis(institution.Id);
         // assign book titles to adoption
        appendBooktitlesToGratis(gratis.Id);
        Test.setCurrentPageReference(Page.GratisCloning);
        
        Test.setCurrentPageReference(Page.GratisCloning);
        Test.startTest();
        System.currentPageReference().getParameters().put(GratisCloningCon.GRATIS_ID_PARAM, gratis.Id);
        GratisCloningCon controller = new GratisCloningCon();
        System.assert(controller.getIsInitSuccess(), 'Failed to initialise controller. Most likely query string parameters problem');
        System.assert(controller.getGratis() != null, 'Failed to initialise controller. Can not load Gratis');
        String nextPage = controller.process().getUrl();
        Test.stopTest();  
    }
   
    // Test Primary Campaign Source - Data
    
    private static final integer NUM_PRODUCTS             = 5;
    private static final string[] bookTitleSeries         = new string[]{ 'A', 'B', 'C' };
    private static final string CAMPAIGN_STATUS_RESPONDED = 'Responded';
    private static final string CAMPAIGN_STATUS_SENT      = 'Sent';
    
    public static OUP_Territory__c territory;
    public static Account institution;
    public static List<Contact> institutionContacts;
    public static List<Positions_Held__c> positionsHeld; 
    public static List<Product2> products;
    public static List<Gratis_Line_Items__c> bookTitles;
    public static Gratis_Item__c gratis;
        
    private static void createInstitution() {
        final string RECORD_TYPE_INSTITUTION = '012200000000X1HAAU';
        
        // create territory
        territory = new OUP_Territory__c(Name = 'Test-Territory', OUP_Country__c = 'UK', Territory__c = 'TEST-123');
        database.insert(territory);
        
        // create accounts
        institution = new Account(Name = 'Test Account - 123', Territory__c = territory.Id, 
                                  ShippingCity = 'London', ShippingPostalCode = 'LDN1', ShippingStreet = 'Brook',
                                  RecordTypeId = RECORD_TYPE_INSTITUTION);     
        database.insert(institution);
    }

    private static void createInstitutionContacts() {
        final Integer NUM_CONTACTS           = 5;
        final string RECORD_TYPE_INSTITUTION = '012200000000YrhAAE';
        
        // create contacts      
        institutionContacts = new List<Contact>(); 
        for (Integer i = 0; i < NUM_CONTACTS; i++) {
            institutionContacts.add(new Contact(LastName = 'Test Contact ' + i, AccountId = institution.Id, 
                                                Territory__c = territory.Id, RecordTypeId = RECORD_TYPE_INSTITUTION));         
        } 
        database.insert(institutionContacts);
    }
    
    
    private static void createProducts() {      
        products = new List<Product2>();
        
        // create products / book titles
        for (string sr : bookTitleSeries) {
            for (integer i = 0; i < NUM_PRODUCTS; i++) {
                products.add(new Product2(Name = 'Product - ' + i, Series__c = 'Series - ' + sr));
            }
        }
        database.insert(products); 
 
    }
    
    
    private static void createGratis(final ID accountId) {
        gratis = new Gratis_Item__c();
        gratis.Institution__c=  accountId;
        gratis.Date_Sent_Left__c = system.today();
        gratis.Contact__c= institutionContacts[0].Id;
        
        database.insert(gratis );
    }
    
    private static void appendBooktitlesToGratis(final ID gratisId ) {
        List<Gratis_Line_Items__c> book_titles = new List<Gratis_Line_Items__c>();
        if (products != null) { 
            for (Product2 p: products ) {
                // create and append book title
                book_titles.add(new Gratis_Line_Items__c(Related_Gratis_Item__c= gratisId,
                                                        Quantity__c = 1, Title__c= p.Id, Date_Sent_Left__c = Date.Today()));
            }
        }
        if (book_titles.isEmpty() == false) {
            database.insert(book_titles);
        }
    }
    
    
}