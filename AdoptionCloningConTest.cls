@isTest(SeeAllData=true)
private class AdoptionCloningConTest{
  
  public static testMethod void testWithErrorsInURL() {
        Test.setCurrentPageReference(Page.AdoptionCloning);
        //test missing OPPORTUNITY_ID_PARAM
        AdoptionCloningCon controller = new AdoptionCloningCon();
        System.assert(!controller.getIsInitSuccess(), 'Failed to identify that process can not be executed');
    }
    
    public static testMethod void testWithContactRoles()
    {
        
        createInstitution();
        createInstitutionContacts();       
        createProducts();

        
        createAdoption(institution.Id);
        
                // assign book titles to adoption
        appendBooktitlesToAdoption(adoption.Id, bookTitles);
        createContactRole(adoption.Id, institutionContacts[1].Id);
        Test.setCurrentPageReference(Page.AdoptionCloning);
        
        
        Test.startTest();
        System.currentPageReference().getParameters().put(AdoptionCloningCon.OPPORTUNITY_ID_PARAM, adoption.Id);
        AdoptionCloningCon controller = new AdoptionCloningCon();
        System.assert(controller.getIsInitSuccess(), 'Failed to initialise controller. Most likely query string parameters problem');
        System.assert(controller.getOpportunity() != null, 'Failed to initialise controller. Can not load Opportunity');
        String nextPage = controller.process().getUrl();
        System.assert(nextPage.contains('/e?retURL='), 'Incorrect URL returned:' + nextPage);
        Test.stopTest();        
    }
     public static testMethod void testWithoutContactRoles()
    {
        createInstitution();
        createInstitutionContacts();       
        createProducts();
        
        createAdoption(institution.Id);
         // assign book titles to adoption
        appendBooktitlesToAdoption(adoption.Id, bookTitles);
        Test.setCurrentPageReference(Page.AdoptionCloning);
        
        Test.setCurrentPageReference(Page.AdoptionCloning);
        Test.startTest();
        System.currentPageReference().getParameters().put(AdoptionCloningCon.OPPORTUNITY_ID_PARAM, adoption.Id);
        AdoptionCloningCon controller = new AdoptionCloningCon();
        System.assert(controller.getIsInitSuccess(), 'Failed to initialise controller. Most likely query string parameters problem');
        System.assert(controller.getOpportunity() != null, 'Failed to initialise controller. Can not load Opportunity');
        String nextPage = controller.process().getUrl();
        System.assert(nextPage.contains('/e?retURL='), 'Incorrect URL returned:' + nextPage);
        Test.stopTest();  
    }
   
    // Test Primary Campaign Source - Data
    
    private static final integer NUM_PRODUCTS             = 5;
    private static final string[] bookTitleSeries         = new string[]{ 'A', 'B', 'C' };
    private static final string CAMPAIGN_STATUS_RESPONDED = 'Responded';
    private static final string CAMPAIGN_STATUS_SENT      = 'Sent';
    
    public static OUP_Territory__c territory;
    public static Account institution;
    public static List<Contact> institutionContacts;
    public static List<Positions_Held__c> positionsHeld;
    public static List<OpportunityContactRole> contactRole;     
    public static List<Product2> products;
    public static List<PricebookEntry> bookTitles;
    public static Opportunity adoption;
    public static List<Campaign> campaigns;
        
    private static void createInstitution() {
        final string RECORD_TYPE_INSTITUTION = '012200000000X1HAAU';
        
        // create territory
        territory = new OUP_Territory__c(Name = 'Test-Territory', OUP_Country__c = 'UK', Territory__c = 'TEST-123');
        database.insert(territory);
        
        // create accounts
        institution = new Account(Name = 'Test Account - 123', Territory__c = territory.Id, 
                                  ShippingCity = 'London', ShippingPostalCode = 'LDN1', ShippingStreet = 'Brook',
                                  RecordTypeId = RECORD_TYPE_INSTITUTION);     
        database.insert(institution);
    }

    private static void createInstitutionContacts() {
        final Integer NUM_CONTACTS           = 5;
        final string RECORD_TYPE_INSTITUTION = '012200000000YrhAAE';
        
        // create contacts      
        institutionContacts = new List<Contact>(); 
        for (Integer i = 0; i < NUM_CONTACTS; i++) {
            institutionContacts.add(new Contact(LastName = 'Test Contact ' + i, AccountId = institution.Id, 
                                                Territory__c = territory.Id, RecordTypeId = RECORD_TYPE_INSTITUTION));         
        } 
        database.insert(institutionContacts);
    }
    
    private static void createContactRole(final ID adoptionId, final ID contactId) {
        final string ROLE_NAME = 'Evaluator';

        contactRole = new List<OpportunityContactRole>();
        contactRole.add(new OpportunityContactRole(OpportunityId = adoptionId, ContactId = contactId, Role = ROLE_NAME, IsPrimary = true));

        database.insert(contactRole);           
    }
    
    private static void createProducts() {      
        products = new List<Product2>();
        
        // create products / book titles
        for (string sr : bookTitleSeries) {
            for (integer i = 0; i < NUM_PRODUCTS; i++) {
                products.add(new Product2(Name = 'Product - ' + i, Series__c = 'Series - ' + sr));
            }
        }
        database.insert(products); 
        
        // assign products to price book entries
        createPriceBookEntries(NUM_PRODUCTS * bookTitleSeries.size());        
        // iterate price book entries and assign product
        for (integer i = 0; i < bookTitles.size(); i++) { 
            bookTitles[i].Product2Id = products[i].Id;          
        }
        // create book titles
        database.insert(bookTitles);
    }
    
    private static void createPriceBookEntries(final integer size) {
        // create price book entries
        Pricebook2 pbStandard  = [SELECT Id FROM Pricebook2 where id = '01s200000005U0P'];
        
        bookTitles             = new List<PricebookEntry>();
        
        decimal price          = 5.0;
        for (integer i = 1; i <= size; i++) {
            bookTitles.add(new PricebookEntry(Pricebook2Id = pbStandard.id, UnitPrice = (price * i), isActive = true));
        }
    }
    
    private static void createAdoption(final ID accountId) {
        adoption                           = new Opportunity();
        adoption.Name                      = 'Test Adoption';
        adoption.AccountId                 = accountId;
        adoption.CloseDate                 = system.today();
        adoption.StageName                 = 'Prospect';
        
        database.insert(adoption);
    }
    
    private static void appendBooktitlesToAdoption(final ID adoptionId, final List<PricebookEntry> price_book_titles) {
        List<OpportunityLineItem> book_titles = new List<OpportunityLineItem>();
        if (price_book_titles != null) { 
            for (PricebookEntry price_book_title : price_book_titles) {
                // create and append book title
                book_titles.add(new OpportunityLineItem(OpportunityId = adoptionId, UnitPrice = price_book_title.UnitPrice, 
                                                        Quantity = 1, PricebookEntryId = price_book_title.Id));
            }
        }
        if (book_titles.isEmpty() == false) {
            database.insert(book_titles);
        }
    }
    
    
}