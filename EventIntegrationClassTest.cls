@isTest
private class EventIntegrationClassTest {

public static Campaign campaign;

    public static testMethod void testWithErrorsInURL() 
    {
        Test.setCurrentPageReference(Page.EventIntegration);
        //test missing CAMPAIGN_URL_PARAM
        EventIntegrationClass controller = new EventIntegrationClass();
        //System.assert(controller.canProceed, 'Failed to identify that process can not be executed');
    }
      
      public static testMethod void exceptionTesting()
      {
        Test.setCurrentPageReference(Page.EventIntegration);
        System.currentPageReference().getParameters().put(EventIntegrationClass.CAMPAIGN_URL_PARAM,'aaa');
        EventIntegrationClass controller = new EventIntegrationClass();
        
        Test.setCurrentPageReference(Page.EventIntegration);
    System.currentPageReference().getParameters().put(EventIntegrationClass.CAMPAIGN_URL_PARAM, '701W000000000Dt');
    controller = new EventIntegrationClass();
        controller.match();
        
        controller.getEvent();
        
        //controller.eventId('{"ids":"alek","success":true}');
        //controller.eventName('{"ids":[51751315],"success":true}');
      }
      
      public static testMethod void testStandardProcess() 
      {    
        createStandardCampaign();  
        test.startTest();
        Test.setCurrentPageReference(Page.EventIntegration);
      System.currentPageReference().getParameters().put(EventIntegrationClass.CAMPAIGN_URL_PARAM, campaign.Id);
      EventIntegrationClass controller = new EventIntegrationClass();
      controller.identifier = '1';
      controller.createSignature('a', 'b');
      //controller.buildWebServiceRequest('http://www.amiando.com/api/event/ID');
      //controller.eventid = controller.eventId('{"ids":[51751315],"success":true}');
      //controller.eventName = controller.eventName('{"event":{"eventURL":"http://www.amiando.com/DJTMNIT","lastModified":"2012-06-28T13:04:04","selectedDate":"2012-03-13T09:30:11","visibility":"private","location":"(LV) Riga Technical University, Main Hall","street":"Kaļķu 1","publishSearchEngines":true,"hostId":292535100,"eventType":"EVENTS","country":"LV","city":"Riga","id":51751315,"title":"Oxford University Press ELT Conference Riga 2012","timezone":"Europe/Helsinki","organisatorDisplayName":"Oxford University Press","creationTime":"2012-02-03T10:24:13","longitude":24.1053501,"latitude":56.9469992,"language":"en","products":[],"identifier":"DJTMNIT","selectedEndDate":"2012-03-13T15:00:11"},"success":true}');
      controller.verifyIfMatched();
      //System.assert(controller.eventId != '', 'event ID empty');
       controller.useCategories= false;
      controller.match();
      controller.getTicketTypes();
      controller.getSessions();
       controller.getEvent();
       
      controller.useCategories= true;
      
      controller.types = new List<SelectOption>();
      controller.types.add(new SelectOption('1', 'one'));
      controller.location = 'two';
      
      controller.categories = new List<SelectOption>();
      controller.categories.add(new SelectOption('1', 'one'));
      controller.category = 'two';
      
      controller.match();
      
      controller.location = '1';
      controller.category = '1';
      controller.match();
      
     //different aspects of a campaign
      campaign.Amiando_ID__c = '51751315';
      campaign.Category_ID__c = '111';
      campaign.Category_Name__c = 'aaa';
      controller.verifyIfMatched();
      
      campaign.Location_ID__c= 111;
      campaign.Location__c = 'aaa';
        controller.verifyIfMatched();
      
      campaign.Category_ID__c = null;
 controller.verifyIfMatched();
 
      //System.assert(campaign.Amiando_ID__c == '51751315', 'Campaign not matched');
      //System.assert(campaign.Run_Amiando_Integration__c, 'Run Amiando Integration not clicked');
      database.delete(campaign);  
      test.stopTest();
      }
    
     
    
     public static testMethod void testMatchedCancelProcess() 
     {
      
        createMatchedCampaign();  
        test.startTest();
        Test.setCurrentPageReference(Page.EventIntegration);
      System.currentPageReference().getParameters().put(EventIntegrationClass.CAMPAIGN_URL_PARAM, campaign.Id);
      EventIntegrationClass controller = new EventIntegrationClass();
      //System.assert(!controller.canProceed, 'Failed to identify that process can proceed');
      //System.assert(controller.isConnected, 'Failed to identify that process can not be executed because of the matched campaign');
      controller.btnNoWarning();
      //System.assert((System.currentPageReference() == new Pagereference('/'+campaign.Id)), 'Not redirected'); 
      database.delete(campaign);  
      test.stopTest();
      }
      
        public static testMethod void testMatchedRemoveProcess() 
     {
      
        createMatchedCampaign();  
        test.startTest();
        
        Test.setCurrentPageReference(Page.EventIntegration);
      System.currentPageReference().getParameters().put(EventIntegrationClass.CAMPAIGN_URL_PARAM, campaign.Id);
      EventIntegrationClass controller = new EventIntegrationClass();
      
      //System.assert(!controller.canProceed, 'Failed to identify that process can proceed');
      //System.assert(controller.isConnected, 'Failed to identify that process can not be executed because of the matched campaign');
      controller.btnRemoveWarning();
               
      //System.assert((System.currentPageReference() == new Pagereference('/'+campaign.Id)), 'Not redirected'); 
      database.delete(campaign);  
      controller.btnRemoveWarning();
      test.stopTest();
      }
      
      
      
       public static testMethod void testMatchedOverwriteProcess()
      {
          
        createMatchedCampaign();  
        test.startTest();
        Test.setCurrentPageReference(Page.EventIntegration);
      System.currentPageReference().getParameters().put(EventIntegrationClass.CAMPAIGN_URL_PARAM, campaign.Id);
      EventIntegrationClass controller = new EventIntegrationClass();
      //System.assert(controller.isConnected, 'Failed to identify that process can not be executed because of the matched campaign');
      controller.btnYesWarning();
      //System.assert(!controller.isConnected, 'Failed to go to the overwrite procedure');
      database.delete(campaign);  
      test.stopTest();
    }
    
    public static testMethod void testMatchedAndNotMatched()
    {
      createMatchedCampaign();  
      createStandardCampaign();  
       Test.setCurrentPageReference(Page.EventIntegration);
      System.currentPageReference().getParameters().put(EventIntegrationClass.CAMPAIGN_URL_PARAM, campaign.Id);
      EventIntegrationClass controller = new EventIntegrationClass();
        //controller.eventid = controller.eventId('{"ids":[51751315],"success":true}');
        controller.verifyIfMatched();
    }
    
        private static void createStandardCampaign() 
        {
          
      campaign                           = new Campaign();
      campaign.Name                      = 'Test Campaign';      
      campaign.StartDate                 = system.today();
      campaign.EndDate            = system.today();
      campaign.Campaign_Objectives__c    = 'Objectives';
      campaign.OUP_Country__c            = 'Poland';
      campaign.Type = 'Webinar';
      campaign.RecordTypeId = '012200000000ekJ';
      database.insert(campaign);
      
      }
      
      private static void createMatchedCampaign() 
        {
          
      campaign                           = new Campaign();
      campaign.Name                      = 'Test Campaign';      
      campaign.StartDate                 = system.today();
      campaign.EndDate            = system.today();
      campaign.Campaign_Objectives__c    = 'Objectives';
      campaign.OUP_Country__c            = 'Poland';
      campaign.Type = 'Webinar';
      campaign.RecordTypeId = '012200000000ekJ';
      campaign.Amiando_ID__c = '51751315';
      campaign.Amiando_Event_Name__c = 'Event Name';
      database.insert(campaign);
      
      }
    
    
}