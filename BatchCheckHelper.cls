/**  Testing Viswa
*	Helper Class is used to check for batch job free slot 
* 	@author PhanithChhun
* 	@category Helper
*/
public class BatchCheckHelper {
	
     	public static Set<String> batchClassNames = new Set<String>{'BatchCreateGratisItems'};
     	
     	/**
	    *   Method for check apex job
	    *   @return true if apex job is free
	    */
	    public static Boolean isBatchAvailable(){
	        List<AsyncApexJob> lapex = [Select a.Id, a.ApexClass.Name, a.ApexClassId From AsyncApexJob a where ApexClass.Name in: batchClassNames and Status='Queued'];
	        return (lapex.size()<5);
	    }
    
     	/**
	    *   Method for check limit DML Statement
	    *   @param numRecords (number of records that will process)
	    *   @return true if DML Statement not limited
	    */
	    public static Boolean isAvailableDML(Integer numRecords){
	        //get available DML row
	        Integer avaiDML = Limits.getLimitDmlRows() - Limits.getDmlRows();
	        
	        if(avaiDML - numRecords <= 2000) return false;
	        return true;
	    }
}