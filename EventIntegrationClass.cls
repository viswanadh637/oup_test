public class EventIntegrationClass
{     
        public String exMessage { get; set; }
        public static final String CAMPAIGN_URL_PARAM = 'Id';
        public String identifier { get; set; }
        public Boolean useCategories { get; set; }
        public Boolean disableCategoriesChoice {get; set;}
        public Integer eventId {get; set;}
        public string eventName {get; set;}
        public string campaignName {get; set;}
         public string campaignNameLinked {get; set;}
        public string campaignIdLinked {get; set;}
        public boolean canMatch {get; set;}
        public boolean canProceed {get; set;}
        public boolean isConnected {get; set;}
        public boolean showLinkedCamp {get; set;}
        private final ID campaignId;
        public List<SelectOption> categories {get; set;}
        public List<SelectOption> types {get; set;}
         public List<String> categoriesIds {get; set;}
public List<String> categoriesUsed {get; set;}
        public String category {get; set;}
          public String location {get; set;}
        public String ticketCategories {get; set;}
        
        //constructor
        public EventIntegrationClass()
        {
                   try 
                   {
                    canProceed = true;
                    campaignId = System.currentPageReference().getParameters().get(CAMPAIGN_URL_PARAM);
                        if (campaignId != null)
                        {
                        List<Campaign> camps = new List<campaign>();
                        camps = [Select Id, Name, Amiando_ID__c, Amiando_Event_Name__c  FROM Campaign c WHERE c.Id =: campaignId];
                        campaignName = camps[0].Name;
                            if (camps[0].Amiando_ID__c == null)
                            {
                            isConnected = false;
                            }
                            else 
                            {
                            eventName = camps[0].Amiando_Event_Name__c;
                            isConnected = true;
                            }
                        }
                    canMatch = false;
                    }
                    catch (Exception ex)
                    {
                    canProceed = false;     
                    exMessage = ex.getMessage();      
                    }
        }


//method for removal of associated Amiando Event
public PageReference btnRemoveWarning() {
try 
{
        List<Campaign> camps = new List<campaign>();
  camps = [Select Id, Name FROM Campaign c WHERE c.Id =: campaignId];
 camps[0].Amiando_ID__c = '';
  camps[0].Run_Amiando_Integration__c = false;
  camps[0].Amiando_Event_Name__c = '';
  camps[0].Category_ID__c = '';
  camps[0].Category_Name__c = '';
  camps[0].Location_ID__c = null;
  camps[0].Location__c = '';
  
  update camps;
  return new PageReference('/'+campaignId);
   }
          catch (Exception ex)
          {
          canProceed = false;     
          exMessage = ex.getMessage();      
           return null;     
          }
 }

        //method used to start the process even when a campaign is already matched
        public void btnYesWarning() 
        {
            eventName = '';
            isConnected = false;
        }

        //method used to cancel process when a campaign is already matched
        public PageReference btnNoWarning() 
        {
             return new PageReference('/'+campaignId);
        }

        //matching campaign with Amiando Event
        public PageReference match()
        {
             try 
             {
                List<Campaign> camps = new List<campaign>();
                camps = [Select Id, Name FROM Campaign c WHERE c.Id =: campaignId];
                camps[0].Amiando_ID__c = String.valueOf(eventId);
                camps[0].Run_Amiando_Integration__c = true;
                camps[0].Amiando_Event_Name__c = eventName;
                if(useCategories)
                {
                
               
                Boolean check = true;
                for (SelectOption so: categories)
                {
                if (so.getValue() == category)
                {
                   camps[0].Category_Name__c = so.getLabel();
                   camps[0].Category_ID__c = category;
                   check = false;
                }
                }
               
                if (check)
                 {
                camps[0].Category_ID__c = null;
                camps[0].Category_Name__c = '';
                }
                check = true;
                for (SelectOption so: types)
                {
                if (so.getValue() == location)
                {
                   camps[0].Location__c = so.getLabel();
                   camps[0].Location_ID__c = System.Integer.valueOf(location);
                   check = false;
                }
                }
               if (check)
                {
                camps[0].Location_ID__c = null;
                camps[0].Location__c = '';
                }
                }
                else
                {
                camps[0].Category_ID__c = null;
                camps[0].Category_Name__c = '';
                camps[0].Location_ID__c = null;
                camps[0].Location__c = '';
                }
                update camps;
                return new PageReference('/'+campaignId);
             }
             catch (Exception ex)
             {
                 canProceed = false;     
                 exMessage = ex.getMessage(); 
                 return null;     
             }
        
        }
        //method used for getting Amiando Event details - not to use in tests as it calls out
        public void getEvent()
        {
            try 
            {
                eventId = System.Integer.valueOf(identifier);
                string timestamp = datetime.now().formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
                
                developerSymOnlineComApi10Soap.Symphony_x0020_API_x0020_V1_0Soap stub = new developerSymOnlineComApi10Soap.Symphony_x0020_API_x0020_V1_0Soap();
                developerSymOnlineComApi10Soap.AuthenticationHeader ah = new developerSymOnlineComApi10Soap.AuthenticationHeader();
                stub.timeout_x=60000;
                
                //Setting header values yyyy-MM-ddTHH:mm:ss.fffZ
                
                ah.Timestamp = timestamp;
                ah.Operation = 'GetEventDetails';
                ah.Signature = createSignature('GetEventDetails', timestamp);
                ah.APIKey = 'KppKPBdPkqChQ8ULdJ8cjHPUu5BMW7aZmNkA3NEhKXWKDFSQEtXDnxgjxwMe6QbU';
                stub.AuthenticationHeader = ah;
                
                developerSymOnlineComApi10Soap.EventDetailsResponse edr = stub.GetEventDetails(eventId);
                eventName  = edr.Title;
                
                 
                if (eventName == '')
                {
                    canProceed = false;     
                    exMessage = 'Verify if your Event Identifier is valid: ' + identifier;    
                }
                else
                {
                verifyIfMatched();
                if (useCategories)
                {
                getTicketTypes();
                getSessions();        
                }
                }                    
             }
             catch (Exception ex)
             {
             canProceed = false;     
             exMessage = ex.getMessage();      
             }
         }
 
    
    public void getTicketTypes()
    {
            try 
            {
             eventId = System.Integer.valueOf(identifier);
                 types = new List<SelectOption>();
                  string timestamp = datetime.now().formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
                 
                developerSymOnlineComApi10Soap.Symphony_x0020_API_x0020_V1_0Soap stub = new developerSymOnlineComApi10Soap.Symphony_x0020_API_x0020_V1_0Soap();
                developerSymOnlineComApi10Soap.AuthenticationHeader ah = new developerSymOnlineComApi10Soap.AuthenticationHeader();
                      stub.timeout_x=60000;
                
                                //Setting header values yyyy-MM-ddTHH:mm:ss.fffZ
               
                ah.Timestamp = timestamp;
                ah.Operation = 'GetTicketTypes';
                ah.Signature = createSignature('GetTicketTypes', timestamp);
                ah.APIKey = 'KppKPBdPkqChQ8ULdJ8cjHPUu5BMW7aZmNkA3NEhKXWKDFSQEtXDnxgjxwMe6QbU';
                stub.AuthenticationHeader = ah;
                
                developerSymOnlineComApi10Soap.ArrayOfTicketTypeResponse edr = stub.GetTicketTypes(eventId);
                                 
               
          
           for (developerSymOnlineComApi10Soap.TicketTypeResponse cat : edr.TicketTypeResponse) 
           {
            types.add(new SelectOption(System.String.valueOf(cat.TicketTypeId), cat.Name));
            }
            
            if (types.size() == 0)
            {canProceed =  false; exMessage = 'No more event categories available for this event';}
            else
            {canMatch = true;}
            
            }
            catch (Exception ex)
            {
            }
     }
    
    public void getSessions()
    {
            try 
            {
             eventId = System.Integer.valueOf(identifier);
             string timestamp = datetime.now().formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        categories = new List<SelectOption>();
                
                developerSymOnlineComApi10Soap.Symphony_x0020_API_x0020_V1_0Soap stub = new developerSymOnlineComApi10Soap.Symphony_x0020_API_x0020_V1_0Soap();
                developerSymOnlineComApi10Soap.AuthenticationHeader ah = new developerSymOnlineComApi10Soap.AuthenticationHeader();
                
                
                //Setting header values yyyy-MM-ddTHH:mm:ss.fffZ
                
                ah.Timestamp = timestamp;
                ah.Operation = 'GetBookableSessions';
                ah.Signature = createSignature('GetBookableSessions', timestamp);
                ah.APIKey = 'KppKPBdPkqChQ8ULdJ8cjHPUu5BMW7aZmNkA3NEhKXWKDFSQEtXDnxgjxwMe6QbU';
                stub.AuthenticationHeader = ah;
                
                developerSymOnlineComApi10Soap.ArrayOfBookableSessionResponse edr = stub.GetBookableSessions(eventId);
                                 
                      
           for (developerSymOnlineComApi10Soap.BookableSessionResponse cat : edr.BookableSessionResponse) 
           {
                
                   categories.add(new SelectOption(System.String.valueOf(cat.BookableSessionId), cat.Title));
            }
            
            if (categories.size() == 0)
            {canProceed =  false; exMessage = 'No more event categories available for this event';}
            else
            {canMatch = true;}
            
            }
            catch (Exception ex)
            {
            }
     }
     
    public void verifyIfMatched()
    {
        //getting all campaigns with the Amiando ID found by the user
       List<Campaign> camps = new List<campaign>();
       camps = [Select Id, Name, Category_ID__c, Category_Name__c, Location_ID__c, Location__c FROM Campaign c WHERE c.Amiando_ID__c =: identifier];
       
        //if there are none is ok to match            
        if (camps.size() == 0)
        { 
            canMatch = true;
            showLinkedCamp =false;
            disableCategoriesChoice = false;
        } 
        //if there is a camp with this Amiando ID do not allow to match and show the campaign with this ID
        else 
        {
         Boolean usedCategories = false;
         categoriesUsed = new List<String>();
         for(Campaign camp : camps) 
         {
         if (camp.Category_ID__c != null && camp.Location_ID__c != null)
         {
          disableCategoriesChoice = true;
               useCategories = true;
               usedCategories = true;
               canMatch = false;
               categoriesUsed.add(camp.Category_Name__c +' ' + camp.Location__c);
         }
             else if (camp.Category_ID__c != null)
              {
               disableCategoriesChoice = true;
               useCategories = true;
               usedCategories = true;
               canMatch = false;
               categoriesUsed.add(camp.Category_Name__c);
              }
              
              else if (camp.Location_ID__c != null)
              {
               disableCategoriesChoice = true;
               useCategories = true;
               usedCategories = true;
               canMatch = false;
               categoriesUsed.add(camp.Location__c);
              }
         }
        if (!usedCategories)
        {
        disableCategoriesChoice = true;
        useCategories = false;
        canMatch = false;
        showLinkedCamp = true;
        campaignNameLinked = camps[0].Name;
        campaignIdLinked = camps[0].Id;
        }
        }
    }
    
    public string createSignature(string operation, string timestamp)
    {
    
    Blob mac = Crypto.generateMac('HMacSHA1', Blob.valueOf(operation + timestamp), Blob.valueOf('PmzfEXxtHFxtRLW2cv9K2gAreWehaG2L')); 
    return EncodingUtil.base64Encode(mac);
    
    }
    
}