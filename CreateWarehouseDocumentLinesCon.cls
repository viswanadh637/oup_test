public class CreateWarehouseDocumentLinesCon {

    public Boolean isError { get; set; }
    
    public static final Integer NO_OF_PRELOAD_ITEMS               = 10;
    public static final String DOCUMENT_ID_PARAM             = 'Document_Id';
    private final ID documentId;
    
    private List<SelectableProduct> lookupProducts;               // book titles product lookup list
    private List<DocumentLines> selectedProducts;
    private List<SearchingCriteria> searchingCriterias;                 
    private String query = '';                                    // query build by search criterias         
    public String filterAdvance { get; set; }
    public Boolean excludeInternationalTitles { get; set; }
    
    public String searchField { get; set; }
    public Boolean sortAsc { get; set; }
         public List<SelectOption> getPricebooks() {
        
        pricebooks.Add(new SelectOption('None', 'None'));
        for (Pricebook2 p: [Select id, Name from Pricebook2 where IsActive = true ORDER BY Name])
        {
        pricebooks.Add(new SelectOption(p.Id, p.Name));
        
        }
        return pricebooks;
    }


    private List<SelectOption> pricebooks = new List<SelectOption>();

    public String selectedPricebook {get; set;}
    public CreateWarehouseDocumentLinesCon () {
        
       try
        {
        isError = false;
            documentId = System.currentPageReference().getParameters().get(DOCUMENT_ID_PARAM);
            excludeInternationalTitles = false;
            
            Warehouse_Document__c wd = [Select Id, RecordType.Name, Status__c from Warehouse_Document__c where id=:documentId];
            
            
            if (wd.RecordType.Name != 'Stock' && wd.Status__c != 'Approved')
            {
            
            if (selectedProducts == null) {
                selectedProducts = new List<DocumentLines>();
                for (Integer i = 0; i < NO_OF_PRELOAD_ITEMS;i++) {
                    selectedProducts.add(new DocumentLines(null, createDocumentLines()));                
                }  
            }
            
            sortAsc     = true;
            searchField = 'search_byName';  
            }
            else
            {
            isError = true;
            }
                      
        } catch (Exception ex) {
            // System.debug('EXCEPTION[\n'+ex.getMessage()+'\n]\n');
            
        }               
    }
    
    public Boolean getRenderSearchResults() {
     System.debug('render?');
        return (((lookupProducts == null) || (lookupProducts.size() == 0)) ? false : true);
    }
    
    public PageReference searchByField() {
        Search();
        return null;    
    }    
    
    // perform search by defiend searching criterias
    public void Search() {
        
        System.debug('Search');
        List<SearchingCriteria> criterias = getSearchingCriterias();  
        String userOUPCountry = getUserOUPCountry();    
        String whereClause = SearchingCriteria.formWhereClause(criterias, filterAdvance);
        Integer resultSetLimit = 100;

        String marketInternational = '';
        if (excludeInternationalTitles == false) {
            marketInternational = 'Market__c = \'INTERNATIONAL\' OR ';
        }
        
        // determine sort by field and order 
        String orderByClause = ' ORDER BY ';
        if (searchField == 'search_byName') {
            orderByClause += 'Name';
        } else if (searchField == 'search_byISBN') {
            orderByClause += 'ISBN__c';
        } else if (searchField == 'search_byPublisher') {
            orderByClause += 'Publisher__c';
        } else if (searchField == 'search_byMarket') {
            orderByClause += 'Market__c';
        } else if (searchField == 'search_bySegment') {
            orderByClause += 'Segment__c';
        } else if (searchField == 'search_bySeries') {
            orderByClause += 'Series__c';
        } else {orderByClause += 'Name';}
       
        
        // asc or desc?
        orderByClause += (sortAsc == true) ? ' ASC' : ' DESC';
        
        
        
                if(whereClause.length() > 0) 
        {
            whereClause  = whereClause + ' AND ((' + marketInternational + ' Market__c = \'' + userOUPCountry  + '\')) AND Selectable__c = true ';// LIMIT 100';
        }
        else
        {
            whereClause  =' WHERE (' + marketInternational + ' Market__c = \'' + userOUPCountry  + '\') AND Selectable__c = true ';// LIMIT 100';
        }    
              
              if (selectedPricebook != 'None' && selectedPricebook != null)
              {
              whereClause = whereClause +' AND Id IN (SELECT Product2Id from PricebookEntry where Pricebook2Id = \''+selectedPricebook+'\' and IsActive = true) ' + orderByClause + ' LIMIT ' + resultSetLimit; 
           
              }
              else
              {
              whereClause = whereClause + orderByClause + ' LIMIT ' + resultSetLimit;
              }
              system.debug(whereClause);
        
              
        lookupProducts = new List<SelectableProduct>(); 

        List<Product2> products = new List<Product2>();        
        try {
            query = 'SELECT Series__c, Segment__c, Publisher__c, ProductCode, Name, Market__c, Level__c, Id, ISBN__c, Selectable__c FROM Product2' + whereClause;
        
            products = Database.query(query);
        } catch (DmlException ex) {
                products = null;
                system.debug(ex);
        } catch (Exception ex) {
            products = null;
         system.debug(ex);
        }
         
        if ((products != null) && (products.size() > 0)) {
            for (Product2 p : products) {          
                lookupProducts.add(new SelectableProduct(p));
            }
        }
    }
    
    public void resetSearchingCriterias() {
        searchingCriterias = new List<SearchingCriteria>();
        searchingCriterias.add(new SearchingCriteria(1, true, 'Name', 'c', '', 'AND'));
        searchingCriterias.add(new SearchingCriteria(2, true, 'Name', 'c', ''));
        searchingCriterias.add(new SearchingCriteria(3));
        searchingCriterias.add(new SearchingCriteria(4));
        searchingCriterias.add(new SearchingCriteria(5, false));    
        
        if (lookupProducts != null) {
            lookupProducts.clear();
        }
    }

    // Get Lookup Book titles product
    public List<SelectableProduct> getLookupProducts() { 
        if (lookupProducts == null) {
            lookupProducts = new List<SelectableProduct>();
        }        
        return lookupProducts;
    }
    
    public List<SearchingCriteria> getSearchingCriterias() {
        if(searchingCriterias == null) {
           resetSearchingCriterias();           
        }        
        return searchingCriterias;
    }
    
    public String getQuery() {
        return query;
    }
    
    public String getPageTitle() {
        String pageTitle = 'Document Lines';
        
        if (documentId != null) {
            Warehouse_Document__c doc = [SELECT name, RecordType.Name FROM Warehouse_Document__c WHERE Id = :documentId];
            pageTitle += ' - ' + doc.Name + ' ' + doc.RecordType.Name;
        }
        
        return pageTitle;
    }
    
    // Add selecting book titles to selected items list
    public void addSelectedProducts() {             
        for (SelectableProduct sp: getLookupProducts()) {
            if ((sp.isSelected) && (!IsProductSelected(sp.product))) {  
               if ((selectedProducts == null) || (selectedProducts.size() == 0)) {
                       getSelectedProducts().add(new DocumentLines(sp.product, createDocumentLines()));
               } else {
                       selectedProducts.add(0, new DocumentLines(sp.product, createDocumentLines()));
               }              
            } 
        }
    }  
    
    public void removeSelectedProduct()
    {
        ID bookTitleID = System.currentPageReference().getParameters().get('title_id');
        
        for (Integer i = 0; i < selectedProducts.size(); i++) {
            DocumentLines bu = selectedProducts.get(i);
            if (bu.getDocumentLines().WD_book_title__c == bookTitleID) {
                selectedProducts.remove(i);                
                return;
            }                    
        }
    }
    
    // Get selected Book Title(s)
    public List<DocumentLines> getSelectedProducts() {
        if (selectedProducts == null) {
            selectedProducts = new List<DocumentLines>();
        }
        return selectedProducts;
    }    
    
    // Get searching fields in SelectOption object 
    public List<SelectOption> getFields() {
        Map<String, Schema.SObjectField> M = Schema.SObjectType.Product2.fields.getMap();
        //List<String> searchingFields = new List<String>(M.keySet());
        List<String> searchingFields = getSearchingFieldNames();
        List<SelectOption> selectOption = new List<SelectOption>();
        selectOption.add(new SelectOption('', '--None--'));
        
        for (String s : searchingFields) {
            Schema.DescribeFieldResult F;
            F = M.get(s).getDescribe();
            selectOption.add(new SelectOption(F.getName(), F.getLabel()));
        }            
  
        return selectOption;       
    }     
    
    public PageReference save() {
        if (saveInternal() == false) {
            return null;
        }
        return redirectToSourcePage(); 
    }
    
    public PageReference cancel() {
        return redirectToSourcePage();       
    }
    
    // internal save 
    public Boolean saveInternal()
    {   
        Boolean status = false;
        Boolean insertValues = true;     
        
        try 
        {
            // make sure we do not have duplicates             
            Map<ID, Warehouse_Document_Line__c> newBookIds = getValidatedLineItems();
            if ((newBookIds != null) && (newBookIds.size() > 0)) {                
                
                // Lines records can not be created for Book titles that already appear 
                // against the Document
                
                if (documentId != null) {
                    List<Warehouse_Document_Line__c> existingDocumentLines = null;
                    if (documentId != null) {
                        existingDocumentLines = [SELECT WD_book_title__r.Name, WD_book_title__c, Id
                                                                       FROM Warehouse_Document_Line__c
                                                                       WHERE Document__c =: documentId AND WD_book_title__c IN: newBookIds.keySet()];
                    } 
                        
                    if (existingDocumentLines.size() > 0) {
                        for (Warehouse_Document_Line__c existingItem: existingDocumentLines){
                            Warehouse_Document_Line__c duplicateItem = newBookIds.get(existingItem.WD_book_title__c);
                            if (duplicateItem != null)  {
                                    duplicateItem.addError('Duplicate title ' + existingItem.WD_book_title__r.Name);
                                    insertValues = false;                                    
                            } 
                        }
                    }    
                        
                    if (insertValues == true) {
                        insert newBookIds.values();
                        status = true;
                    }
                }        
            }
        } catch (DmlException ex) {
            System.debug(ex);            
            System.currentPageReference().getParameters().put('error', 'noInsert');
            ApexPages.addMessages(ex);
        } catch (Exception e) {
            System.debug(e);
            System.currentPageReference().getParameters().put('error', 'noInsert');                 
        }
        
        return status;
    }
     
    private Warehouse_Document_Line__c createDocumentLines()
    {
        final Warehouse_Document_Line__c DocumentLines = new Warehouse_Document_Line__c(Document__c = documentId);
        return DocumentLines;
    }
    
    private Map<ID, Warehouse_Document_Line__c> getValidatedLineItems()
    {
        Boolean isValid = true;
        
        final Map<ID, Warehouse_Document_Line__c> newBookIds = new Map<ID, Warehouse_Document_Line__c>();
        for (DocumentLines bu : selectedProducts) {                        
            if (!bu.needSave()) {
                continue;
            }
            if (!bu.validate()) {
                isValid = false;
            } else {
                Warehouse_Document_Line__c lineItem = bu.getDocumentLines();
                if (!newBookIds.containsKey(lineItem.WD_book_title__c)) {                                   
                   newBookIds.put(lineItem.WD_book_title__c, lineItem);                                   
                } else {
                    // duplicates on the page
                    lineItem.addError('Duplicate title on the page');
                    isValid = false;
                }
            }     
        }
        
        return isValid ? newBookIds : null;
    }
    
    // Check whether Product is selected or not
    private Boolean IsProductSelected(Product2 p) {
        for (DocumentLines bu : getSelectedProducts()) {
            if (bu.getDocumentLines().WD_book_title__c == p.id) {
                return true;
            }            
        }
        return false;
    }
    
    private PageReference redirectToSourcePage() {
        String reference = 'home/home.jsp';
            
        if (documentId != null) {
            reference = documentId;     // reference = '003/o';
        }
            
        PageReference pageReference = new PageReference('/'+reference);
        pageReference.setRedirect(true);
        return pageReference;
    }
    
    private List<String> getSearchingFieldNames() {
        // JRS 31/03/2011 - ELT-3-4 [Add Component Type, Edition]
        return new String[] { 'Name', 'ISBN__c', 'Level__c', 'Segment__c', 'Series__c', 'Publisher__c', 'Component_Type__c', 'Edition__c', 'Subject__c' };
    }    
    
    private String getUserOUPCountry() {
        String userID = UserInfo.getUserId();
        User user = [SELECT Id, OUP_Country__c FROM User WHERE Id = :userID LIMIT 1];
        if (user != null) {
            return user.OUP_Country__c;
        } else {
            return '';
        }
    }
    
    private void addErrorMessage(String msg) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
    }

        
    // Inner Classe(s)
    
    public class DocumentLines {
        
        public String getBookTitle() { return bookTitle; } 
        Public Warehouse_Document_Line__c getDocumentLines() { return documentline; }
        Public Boolean getIsPreLoad() { return isPreLoad; }
        public String ISBN{ get; set; }
        public String getErrorMessage() { return errorMessage; }
        public Boolean getIsError() { return isError; }                
        private String bookTitle; // book title from selected book title product
        private Warehouse_Document_Line__c documentline;    
        private Boolean isPreLoad;
        private Boolean isError;
        private String errorMessage;
        
        public DocumentLines(Product2 bt, Warehouse_Document_Line__c bu) {
            
            try
            {
                isError = false;
            
                if (bt == null) {
                    documentline        = bu;                
                    isPreLoad       = true;
                } else {
                    bookTitle       = bt.Name;
                    ISBN            = bt.ISBN__c;
                    bu.WD_book_title__c = bt.Id;
                    documentline        = bu;
                    isPreLoad       = false;
                }                        
            } catch (Exception ex) {
            // System.debug('EXCEPTION[\n'+ex.getMessage()+'\n]\n');
            }
        }        
        
        public Boolean needSave()
        {
            if (isPreLoad == true) {
                if ((ISBN == null) || (ISBN.length() == 0)) {
                    this.bookTitle = null;
                    isError = false;
                    errorMessage = '';
                    return false;
                }
            }
            
            return true;             
        }
                
        public Boolean validate()
        {   
            Boolean status = true; 
            
            if (isPreLoad == true) {
                // ensure sure that's booktitle match with the ISBN input
                String arg = ISBN.trim();
                List<Product2> products = [SELECT id, name FROM Product2 WHERE ISBN__c = :arg];
                if (products.size() == 1) {
                    Product2 product      = products.get(0);
                    bookTitle             = product.name;    
                    documentline.WD_book_title__c = product.id;
                    isError               = false;
                    errorMessage          = '';            
                } else {
                    isError      = true;
                    errorMessage = 'Invalid ISBN';
                    status       = false;
                }
            }
            
            if ((documentline.Quantity__c == null) || (documentline.Quantity__c < 0)) {
                documentline.Quantity__c.addError('Number Used is Required field');
                status = false;
            }
            
            if (isPreLoad && !status) {
                bookTitle  = null;
            }
            
            return status;                     
        }
    }
     
    public class SelectableProduct {   
        public Product2 product { get; set; } 
        public Boolean isSelected { get; set; }
            
        public SelectableProduct(Product2 p) {
            product = p;
            isSelected = false;
        }          
    }
}