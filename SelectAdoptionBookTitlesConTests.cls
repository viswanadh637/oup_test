@isTest
private class SelectAdoptionBookTitlesConTests {
	
// Test Contoller
		
	public static testMethod void getQuery() {
		SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = new SelectAdoptionBookTitlesCon(getStandardController());
		selectAdoptionBookTitlesCon.getQuery();	
    }
    
	public static testMethod void saveTest() {
		SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = new SelectAdoptionBookTitlesCon(getStandardController());
		selectAdoptionBookTitlesCon.save();	
    }
    
    public static testMethod void cancelTest() {
    	SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = new SelectAdoptionBookTitlesCon(getStandardController());
    	selectAdoptionBookTitlesCon.cancel();
    }   
    	
	public static testMethod void saveInternalTest() {
		SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = new SelectAdoptionBookTitlesCon(getStandardController());
		System.assert(selectAdoptionBookTitlesCon.saveInternal() == false);
	}
		
	public static testMethod void getRenderSearchResultsTest() {
    	SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = new SelectAdoptionBookTitlesCon(getStandardController());
    	Boolean status = selectAdoptionBookTitlesCon.getRenderSearchResults();
    	System.assert(status == false);
    }
    
    public static testMethod void initialSearchingCriteriasTest() {
        SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = new SelectAdoptionBookTitlesCon(getStandardController());
        List<SearchingCriteria> criterias = selectAdoptionBookTitlesCon.getSearchingCriterias();   
        System.assertEquals(5, criterias.size());				// ensure that's 5 empty criterias loaded
        System.assertEquals('Name', criterias.get(0).field);	// ensure first criteria default to book title field
        System.assertEquals('Name', criterias.get(1).field);	// ensure first criteria default to ISBN field  
    }
    
    public static testMethod void getFieldsTest() {
    	SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = new SelectAdoptionBookTitlesCon(getStandardController());
    	List<SelectOption> selectOption = selectAdoptionBookTitlesCon.getFields();
    	System.assert(selectOption != null);
    }
    	
	public static testMethod void getLookupProductsTest() {
		SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = new SelectAdoptionBookTitlesCon(getStandardController());
		List<SelectAdoptionBookTitlesCon.SelectableProduct> lookupProducts = selectAdoptionBookTitlesCon.getLookupProducts();    	
    	System.assert(lookupProducts != null);
    }
	
	public static testMethod void getSearchingCriteriasTest() {
		SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = new SelectAdoptionBookTitlesCon(getStandardController());
		List<SearchingCriteria> searchingCriterias = selectAdoptionBookTitlesCon.getSearchingCriterias();    	
    	System.assert(searchingCriterias != null);
    }
	
    public static testMethod void searchTest() {
    	SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = new SelectAdoptionBookTitlesCon(getStandardController());
    	selectAdoptionBookTitlesCon.Search();
    	selectAdoptionBookTitlesCon.searchField = 'search_byName';
    	selectAdoptionBookTitlesCon.Search();
    	selectAdoptionBookTitlesCon.searchField = 'search_byISBN';
    	selectAdoptionBookTitlesCon.Search();
    	selectAdoptionBookTitlesCon.searchField = 'search_byMarket';
    	selectAdoptionBookTitlesCon.Search();    	    	
    }
        
    public static testMethod void addSelectedProductsTest() {
    	SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = new SelectAdoptionBookTitlesCon(getStandardController());
    	selectAdoptionBookTitlesCon.addSelectedProducts();
    }
    
    public static testMethod void getPageTitleTest() {
    	SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = new SelectAdoptionBookTitlesCon(getStandardController());
    	String pageTitle = selectAdoptionBookTitlesCon.getPageTitle();
    	System.assert(pageTitle.length() > 0);
    }
    
    public static testMethod void removeSelectedProductTest() {
    	SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = new SelectAdoptionBookTitlesCon(getStandardController());
    	selectAdoptionBookTitlesCon.removeSelectedProduct();
    }
        
	public static testMethod void contollerWithoutParametersTest() {
		SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = preparePageWithoutParameters();
		System.assert(selectAdoptionBookTitlesCon != null);
	}
	    
    public static testMethod void contollerWithParametersTest() {
    	SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = preparePageWithParameters();
    	System.assert(selectAdoptionBookTitlesCon != null);
    	
    	// selectAdoptionBookTitlesCon.resetSearchingCriterias();
    	List<SearchingCriteria> criterias = selectAdoptionBookTitlesCon.getSearchingCriterias();
    	System.assertEquals(criterias.size(), 5);
    	
    	System.assertEquals(criterias.get(0).index, 1);
        System.assertEquals(criterias.get(0).field, 'Name');
        System.assertEquals(criterias.get(0).operator, 'c');
        System.assertEquals(criterias.get(0).logic, 'AND');
        System.assertEquals(criterias.get(0).value, '');
        
        System.assertEquals(criterias.get(1).index, 2);
        System.assertEquals(criterias.get(1).field, 'Name');
        System.assertEquals(criterias.get(1).operator, 'c');
        System.assertEquals(criterias.get(1).value, '');          
        
        selectAdoptionBookTitlesCon.Search();
        List<SelectAdoptionBookTitlesCon.SelectableProduct> lookupProducts = selectAdoptionBookTitlesCon.getLookupProducts();
        
        for (SelectAdoptionBookTitlesCon.SelectableProduct product : lookupProducts) {
        	System.assertEquals(product.isSelected, false);
        	product.isSelected = true;
            System.assertEquals(product.isSelected, true);
            product.isSelected = false;
            System.assertEquals(product.isSelected, false);
        }
        
        System.assert(lookupProducts.size() >= 1, 'Please create at least 1 Product for testing');
        lookupProducts.get(0).isSelected = true;
        
        selectAdoptionBookTitlesCon.addSelectedProducts();
        List<SelectAdoptionBookTitlesCon.AdoptionBookTitle> selectedProduct = selectAdoptionBookTitlesCon.getSelectedProducts();
        
        System.assertEquals(1 + 10 /* SelectAdoptionBookTitlesCon.NO_OF_PRELOAD_ITEMS */, selectedProduct.size()); // check the list size should be 1 only
        
        selectAdoptionBookTitlesCon.addSelectedProducts();
        System.assertEquals(1 + 10 /* SelectAdoptionBookTitlesCon.NO_OF_PRELOAD_ITEMS */, selectedProduct.size()); // check the list size should be 1 only
        
        selectedProduct.get(0).needSave();
        selectedProduct.get(0).validate();
        
        String nextPage = selectAdoptionBookTitlesCon.cancel().getUrl();        
    }
    
// Test AdoptionBookTitle

	public static testMethod void adoptionBookTitlePreLoadItemsTest() {
		SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = new SelectAdoptionBookTitlesCon(getStandardController());
		Test.startTest();
		testPreLoadItems(selectAdoptionBookTitlesCon);
		Test.stopTest();
	}

	public static testMethod void adoptionBookTitleTest() {
		SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = new SelectAdoptionBookTitlesCon(getStandardController());
		
		Opportunity op = [SELECT Id, o.Pricebook2Id FROM Opportunity o LIMIT 1];
		ID priceBookId = op.Pricebook2Id;
            
		OpportunityLineItem oi = createAdoptionBookTitle();
		SelectAdoptionBookTitlesCon.AdoptionBookTitle adoptionBookTitle01 = new SelectAdoptionBookTitlesCon.AdoptionBookTitle(null, oi, priceBookId, oi.CurrencyIsoCode);
		System.assert(!adoptionBookTitle01.needSave());
		
		adoptionBookTitle01.getErrorMessage();
    	adoptionBookTitle01.getIsError();    	
    	OpportunityLineItem opportunityLineItem = adoptionBookTitle01.getAdoptionBookTitle();
    	Boolean isPreLoad = adoptionBookTitle01.getIsPreLoad();
		String ISBN = adoptionBookTitle01.ISBN;
		String Id = adoptionBookTitle01.getID();
		String bookTitle = adoptionBookTitle01.getBookTitle();
		
		SelectAdoptionBookTitlesCon.AdoptionBookTitle adoptionBookTitle02 = new SelectAdoptionBookTitlesCon.AdoptionBookTitle(getBookTitle(), oi, priceBookId, oi.CurrencyIsoCode);
		System.assert(adoptionBookTitle02.needSave());		
	}
			
		
// Test SelectableProduct

	public static testMethod void selectableProductTest() {
		Product2 p = new Product2();
		SelectAdoptionBookTitlesCon.SelectableProduct sp = new SelectAdoptionBookTitlesCon.SelectableProduct(p);
        sp.isSelected = true;
        System.assert(true, sp.Product === p);
        System.assert(true, sp.isSelected); 
	}
	
// Test SearchingCriteria
	
	public static testMethod void searchingCriteriaTest01() {        
        SearchingCriteria s0 = new SearchingCriteria();
        SearchingCriteria s1 = new SearchingCriteria(2, false, 'name', 'e', 'John');
        SearchingCriteria s2 = new SearchingCriteria(3, false, 'id', 's', 'ABC12', 'OR'); 
        SearchingCriteria s3 = new SearchingCriteria(4);
        SearchingCriteria s4 = new SearchingCriteria(5, true, 'id', 'q', 'CBA321', 'AND');       
        
        List<SearchingCriteria> criterias = new SearchingCriteria[]{s0, s1, s2, s3, s4};
        
        String query = SearchingCriteria.formWhereClause(criterias);  
            
        System.assertEquals(' WHERE (name LIKE \'%John\' AND ( id LIKE \'ABC12%\' OR id = \'CBA321\'))', query);  
    }
    
    public static testMethod void searchingCriteriaTest02() {        
    	// Test - Incomplete criterias
        SearchingCriteria s0 = new SearchingCriteria();
        SearchingCriteria s1 = new SearchingCriteria(2, false, 'name', 'e', 'John');
        SearchingCriteria s2 = new SearchingCriteria(3, false, 'id', 's', 'ABC12', 'OR');         
        SearchingCriteria s4 = new SearchingCriteria(4, true, 'id', 'q', 'CBA321', 'AND'); 
        SearchingCriteria s3 = new SearchingCriteria(5);      
        
        List<SearchingCriteria> criterias = new SearchingCriteria[]{s0, s1, s2, s3, s4};
        
        String query = SearchingCriteria.formWhereClause(criterias);  
            
        System.assertEquals(' WHERE (name LIKE \'%John\' AND ( id LIKE \'ABC12%\' OR id = \'CBA321\'))', query);  
    }
    
    public static testMethod void searchingCriteriaTest03() {        
    	// Test - Advanced Filter Conditions
        SearchingCriteria s0 = new SearchingCriteria();
        SearchingCriteria s1 = new SearchingCriteria(2, false, 'name', 'e', 'John');
        SearchingCriteria s2 = new SearchingCriteria(3, false, 'id', 's', 'ABC12', 'OR');         
        SearchingCriteria s4 = new SearchingCriteria(4, true, 'id', 'q', 'CBA321', 'AND'); 
        SearchingCriteria s3 = new SearchingCriteria(5);      
        
        List<SearchingCriteria> criterias = new SearchingCriteria[]{s0, s1, s2, s3, s4};
        
        String query = SearchingCriteria.formWhereClause(criterias, '2 AND (3 OR 4)');  
            
        System.assertEquals(' WHERE (name LIKE \'%John\' AND (id LIKE \'ABC12%\' OR id = \'CBA321\'))', query);  
    }
    
    public static testMethod void searchingCriteriaWhereClauseTest() {
        String f = 'Name';
        String v = 'ABC';
        
        System.assertEquals('Name = \'ABC\'', SearchingCriteria.getWhereClause(f,'q',v).builderWhereClause());
        System.assertEquals('Name != \'ABC\'', SearchingCriteria.getWhereClause(f,'n',v).builderWhereClause());
        System.assertEquals('Name LIKE \'ABC%\'', SearchingCriteria.getWhereClause(f,'s',v).builderWhereClause());
        System.assertEquals('Name LIKE \'%ABC\'', SearchingCriteria.getWhereClause(f,'e',v).builderWhereClause());
        System.assertEquals('Name LIKE \'%ABC%\'', SearchingCriteria.getWhereClause(f,'c',v).builderWhereClause());
        System.assertEquals('(NOT Name LIKE \'%ABC%\')', SearchingCriteria.getWhereClause(f,'k',v).builderWhereClause());
        System.assertEquals('Name IN(ABC)', SearchingCriteria.getWhereClause(f,'u',v).builderWhereClause());
        System.assertEquals('Name NOT IN(ABC)', SearchingCriteria.getWhereClause(f,'x',v).builderWhereClause());
    }
    
// Test General
	
	public static testMethod void getBookTitleTest() {
 		Test.startTest();
 		Product2 product2 = getBookTitle();
		Test.stopTest();
	}
		
	public static testMethod void getStandardControllerTest() {
 		Test.startTest();
 		ApexPages.StandardController stdController = getStandardController();
		Test.stopTest();
	}
	
	public static testMethod void preparePageWithParametersTest() {
		Test.startTest();
 		preparePageWithParameters();
		Test.stopTest();
	}
	
// Data Preparation

	private static SelectAdoptionBookTitlesCon preparePageWithoutParameters() {		
		SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = new SelectAdoptionBookTitlesCon(getStandardController());
		System.assert(selectAdoptionBookTitlesCon != null);
		return selectAdoptionBookTitlesCon;
	}
	
	private static SelectAdoptionBookTitlesCon preparePageWithParameters() {
		// Add parameters to page URL
        System.currentPageReference().getParameters().put('id' /*SelectAdoptionBookTitlesCon.ADOPTION_ID_PARAM*/, getAdoption().Id);
			
		SelectAdoptionBookTitlesCon selectAdoptionBookTitlesCon = new SelectAdoptionBookTitlesCon(getStandardController());
		System.assert(selectAdoptionBookTitlesCon != null);
		return selectAdoptionBookTitlesCon;
	}
	
	private static OpportunityLineItem createAdoptionBookTitle() {
		OpportunityLineItem opportunityLineItem = new OpportunityLineItem(OpportunityId = getAdoption().Id, Quantity = 0, UnitPrice = 0);
		return opportunityLineItem;
	}
	
    private static Opportunity getAdoption() {
        Opportunity[] opportunity = [SELECT o.Id, o.Pricebook2Id, o.CurrencyIsoCode FROM Opportunity o WHERE o.Pricebook2Id != null LIMIT 1]; // WHERE Id = '006S0000002Zj2qIAC'];
        System.debug('\n\n' + opportunity + '\n');
        System.assert(opportunity.size() >= 1, 'Please create at least 1 Opportunity for testing');
        return opportunity[0];
    }
    
    private static Product2 getBookTitle() {
        Product2[] products = [select Id from Product2 limit 1];
        System.assert(products.size() >= 1, 'Please create at least 1 Product(Book Title) for testing');
        return products[0];
    }
    
    private static void testPreLoadItems(SelectAdoptionBookTitlesCon controller) {
    	List<SelectAdoptionBookTitlesCon.AdoptionBookTitle> adoptionBookTitle = controller.getSelectedProducts();
    	for (SelectAdoptionBookTitlesCon.AdoptionBookTitle abt : adoptionBookTitle) {
    		if (abt.getIsPreLoad() == true) {
    		    System.assertEquals(null, abt.ISBN);
    			System.assertEquals(null, abt.getBookTitle());    			
    		}
    	}    	
    }
    
    private static ApexPages.StandardController getStandardController() {    
    	Opportunity opportunity = getAdoption();
		ApexPages.StandardController stdController = new ApexPages.StandardController(opportunity);
		return stdController;
    }
}