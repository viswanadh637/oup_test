/**
*   Batch Process for the creation of multiple Gratis Items and associated Gratis Line Items
*   This process will be called from Page GratisFromTargetProduct, whenever the creation can not be made immediately by controller
*   due to the SF Governor limitation.
*   @author PhanithChhun
*   @category Businese Logic
*/
global class BatchCreateGratisItems implements Database.Batchable<sObject>, Database.Stateful{
    private String campId;
    private String cUser;
    private Date cDate;
    private Boolean nonIntegration;
	
    private User cUserRec;
    private Integer noCreated = 0;
    private Integer noExisting = 0;
    private Map<String,List<String>> validationRuleMessageOnGi = new Map<String,List<String>>();
    
    private List<String> selectedTarPros;
    private String cratisRecTypeId;
    
 	public BatchCreateGratisItems(String campaignId, 
 									List<String> selectedTargetProducts, 
 									String currentUserId, 
 									Date createDate, 
 									Boolean nonIntegrationUser, String cratisRecordTypeId){
 		campId = campaignId;
 		selectedTarPros = selectedTargetProducts;
 		cUser = currentUserId;
 		cDate = createDate;
 		nonIntegration = nonIntegrationUser;
 		validationRuleMessageOnGi = new Map<String,List<String>>();
 		noCreated = 0;
 		noExisting = 0;
 		cratisRecTypeId = cratisRecordTypeId;
 	}
 	
 	
    global database.querylocator start(Database.BatchableContext BC){ 
    	//Email will be sent to the end-user after the completion of the process
    	cUserRec = MassCreationGratisItemsCon.getUserRecord(cUser);
		return Database.getQueryLocator([SELECT Contact.AccountId, ContactId, CampaignId, Contact.Address_Preference__c, Status FROM CampaignMember
										WHERE CampaignId = :campId ORDER BY Contact.LastName ASC]);
    }
    
    global void execute(Database.BatchableContext BC,List<sObject> scope){
    	List<CampaignMember> lcampmbr = (List<CampaignMember>)scope ;
    	
    	MassCreationGratisItemsCon controller = new MassCreationGratisItemsCon(new ApexPages.StandardController(new Campaign(Id=campId)), cratisRecTypeId, true, cDate, cUser);
    	MassCreationGratisItemsCon.GratisItemManager gratisItemMgr = controller.getGratisItemManager();
    	gratisItemMgr.selectedTargetProducts = selectedTarPros;
    	gratisItemMgr.memberIdByBatch = new Set<String>();
    	gratisItemMgr.isBatchRunning = true;
    	gratisItemMgr.maxRows = MassCreationGratisItemsCon.MAX_LIMIT_ALL_MEMBER;
    	for(CampaignMember campmbr : lcampmbr){
    		gratisItemMgr.memberIdByBatch.add(campmbr.Id);
    	}
    	gratisItemMgr.retrieveUnsentCampaignMember(true, controller.rolodexState, '',0);
    	gratisItemMgr.prepare();		
		gratisItemMgr.massCreateGratisAll();
    	
    	noCreated += gratisItemMgr.numCreatedGratisItems;
    	noExisting += gratisItemMgr.numExistingGratisItems;
    	if(!gratisItemMgr.validationRuleMessageOnGi.isEmpty()){
    		for(String msg : gratisItemMgr.validationRuleMessageOnGi.keySet()){
    			if(!validationRuleMessageOnGi.containsKey(msg)){
    				validationRuleMessageOnGi.put(msg, new List<String>());
    			}
    			validationRuleMessageOnGi.get(msg).addAll(gratisItemMgr.validationRuleMessageOnGi.get(msg));
    		}
    	}
    }
     
    global void finish(Database.BatchableContext BC){
    	String htmlBody = Label.BatchCreationGratisItemsCompletionMsg+ '<br/>' + noCreated+  ' ' + Label.NumberGratisItemCreated + '<br/>' + noExisting + ' ' + Label.NumberGratisItemExisting;
        if(!validationRuleMessageOnGi.isEmpty()){
        	htmlBody += '<br/>';
    		for(String msg : validationRuleMessageOnGi.keySet()){
    			htmlBody += '<br/>';
    			htmlBody += msg + ' ' + validationRuleMessageOnGi.get(msg).size() + ' failed record(s)';
    		}
    	}
        MassCreationGratisItemsCon.sendEmail(Label.BatchCreationGratisItemsCompletionEmailSubject, htmlBody, cUserRec);
    }
}