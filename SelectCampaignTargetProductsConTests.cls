@isTest
private class SelectCampaignTargetProductsConTests {
	
// Test Contoller
		
	public static testMethod void getQuery() {
		SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
		selectCampaignTargetProductsCon.getQuery();	
    }
    
	public static testMethod void saveTest() {
		System.currentPageReference().getParameters().put('retURL', '/home/home.jsp');
		SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
		selectCampaignTargetProductsCon.save();	
    }
    
    public static testMethod void cancelTest() {
    	System.currentPageReference().getParameters().put('cancelURL', '/home/home.jsp');    	
    	SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
    	selectCampaignTargetProductsCon.cancel();
    }   
    	
	public static testMethod void saveInternalTest() {
		SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
		System.assert(selectCampaignTargetProductsCon.saveInternal() == false);
	}
		
	public static testMethod void getRenderSearchResultsTest() {
    	SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
    	Boolean status = selectCampaignTargetProductsCon.getRenderSearchResults();
    	System.assert(status == false);
    }
    
    public static testMethod void initialSearchingCriteriasTest() {
        SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
        List<SearchingCriteria> criterias = selectCampaignTargetProductsCon.getSearchingCriterias();   
        System.assertEquals(5, criterias.size());				// ensure that's 5 empty criterias loaded
        System.assertEquals('Name', criterias.get(0).field);	// ensure first criteria default to book title field
        System.assertEquals('Name', criterias.get(1).field);	// ensure first criteria default to ISBN field  
    }
    
    public static testMethod void getFieldsTest() {
    	SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
    	List<SelectOption> selectOption = selectCampaignTargetProductsCon.getFields();
    	System.assert(selectOption != null);
    }
    	
	public static testMethod void getLookupProductsTest() {
		SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
		List<SelectCampaignTargetProductsCon.SelectableProduct> lookupProducts = selectCampaignTargetProductsCon.getLookupProducts();    	
    	System.assert(lookupProducts != null);
    }
	
	public static testMethod void getSearchingCriteriasTest() {
		SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
		List<SearchingCriteria> searchingCriterias = selectCampaignTargetProductsCon.getSearchingCriterias();    	
    	System.assert(searchingCriterias != null);
    }
	
	public static testMethod void searchByFieldTest() {
    	SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
    	System.assert(selectCampaignTargetProductsCon.searchByField() == null);
    }
    
    public static testMethod void searchTest() {
    	SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
    	selectCampaignTargetProductsCon.Search();
    	selectCampaignTargetProductsCon.searchField = 'search_byName';
    	selectCampaignTargetProductsCon.Search();
    	selectCampaignTargetProductsCon.searchField = 'search_byISBN';
    	selectCampaignTargetProductsCon.Search();
    	selectCampaignTargetProductsCon.searchField = 'search_byPublisher';
    	selectCampaignTargetProductsCon.Search();    	    	
    }
        
    public static testMethod void addSelectedProductsTest() {
    	SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
    	selectCampaignTargetProductsCon.addSelectedProducts();
    }
    
    public static testMethod void getPageTitleTest() {
    	SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
    	String pageTitle = selectCampaignTargetProductsCon.getPageTitle();
    	System.assert(pageTitle.length() > 0);
    }
    
    public static testMethod void removeSelectedProductTest() {
    	SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
    	selectCampaignTargetProductsCon.removeSelectedProduct();
    }
        
	public static testMethod void contollerWithoutParametersTest() {
		SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = preparePageWithoutParameters();
		System.assert(selectCampaignTargetProductsCon != null);
	}
	    
    public static testMethod void contollerWithParametersTest() {
    	System.currentPageReference().getParameters().put('retURL', '/home/home.jsp');
    	System.currentPageReference().getParameters().put('cancelURL', '/home/home.jsp');    	
    	
    	SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = preparePageWithParameters();
    	System.assert(selectCampaignTargetProductsCon != null);
    	
    	selectCampaignTargetProductsCon.resetSearchingCriterias();
    	List<SearchingCriteria> criterias = selectCampaignTargetProductsCon.getSearchingCriterias();
    	System.assertEquals(criterias.size(), 5);
    	
    	System.assertEquals(criterias.get(0).index, 1);
        System.assertEquals(criterias.get(0).field, 'Name');
        System.assertEquals(criterias.get(0).operator, 'c');
        System.assertEquals(criterias.get(0).logic, 'AND');
        System.assertEquals(criterias.get(0).value, '');
        
        System.assertEquals(criterias.get(1).index, 2);
        System.assertEquals(criterias.get(1).field, 'Name');
        System.assertEquals(criterias.get(1).operator, 'c');
        System.assertEquals(criterias.get(1).value, '');          
             
        selectCampaignTargetProductsCon.Search();
        List<SelectCampaignTargetProductsCon.SelectableProduct> lookupProducts = selectCampaignTargetProductsCon.getLookupProducts();
        
        for (SelectCampaignTargetProductsCon.SelectableProduct product : lookupProducts) {
        	System.assertEquals(product.isSelected, false);
        	product.isSelected = true;
            System.assertEquals(product.isSelected, true);
            product.isSelected = false;
            System.assertEquals(product.isSelected, false);
        }
        
        System.assert(lookupProducts.size() >= 1, 'Please create at least 1 Account(Insitution) for testing');
        lookupProducts.get(0).isSelected = true;
        
        selectCampaignTargetProductsCon.addSelectedProducts();
        List<SelectCampaignTargetProductsCon.TargetProduct> selectedProduct = selectCampaignTargetProductsCon.getSelectedProducts();
        System.debug('selectedProduct::'+selectedProduct);
        System.assertEquals(6 /* SelectCampaignTargetProductsCon.NO_OF_PRELOAD_ITEMS */, selectedProduct.size()); // check the list size should be 1 only
        // System.assertEquals(lookupProducts.get(0).product.id, selectedProduct.get(0).getBooksUsed().Book_Used__c); // ensure both object refer to the same instance
        
        selectCampaignTargetProductsCon.addSelectedProducts();
        System.assertEquals(6 /* SelectCampaignTargetProductsCon.NO_OF_PRELOAD_ITEMS */, selectedProduct.size()); // check the list size should be 1 only
        
        selectedProduct.get(0).needSave();
        selectedProduct.get(0).validate();
                
        String nextPage = selectCampaignTargetProductsCon.save().getUrl();
        nextPage = selectCampaignTargetProductsCon.cancel().getUrl();        
    }
    
// Test TargetProduct

	public static testMethod void targetProductPreLoadTest() {
		SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
		Test.startTest();
		testPreLoadItems(selectCampaignTargetProductsCon);
		Test.stopTest();
	}
	
	public static testMethod void targetProductTest() {
		SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
		
		SelectCampaignTargetProductsCon.TargetProduct targetProduct01 = new SelectCampaignTargetProductsCon.TargetProduct(null);
		System.assert(!targetProduct01.needSave());
		
		Product2 product = [SELECT id, name FROM Product2 WHERE ISBN__c = : '9780193120013'];
		SelectCampaignTargetProductsCon.TargetProduct targetProduct02 = new SelectCampaignTargetProductsCon.TargetProduct(product);
		System.assert(targetProduct02.needSave());		
	}

// Test SelectableProduct

	public static testMethod void selectableProductTest() {
		Product2 p = new Product2();
		SelectCampaignTargetProductsCon.SelectableProduct sp = new SelectCampaignTargetProductsCon.SelectableProduct(p);
        sp.isSelected = true;
        System.assert(true, sp.Product === p);
        System.assert(true, sp.isSelected); 
	}
	
// Test SearchingCriteria
	
	public static testMethod void searchingCriteriaTest01() {        
        SearchingCriteria s0 = new SearchingCriteria();
        SearchingCriteria s1 = new SearchingCriteria(2, false, 'name', 'e', 'John');
        SearchingCriteria s2 = new SearchingCriteria(3, false, 'id', 's', 'ABC12', 'OR'); 
        SearchingCriteria s3 = new SearchingCriteria(4);
        SearchingCriteria s4 = new SearchingCriteria(5, true, 'id', 'q', 'CBA321', 'AND');       
        
        List<SearchingCriteria> criterias = new SearchingCriteria[]{s0, s1, s2, s3, s4};
        
        String query = SearchingCriteria.formWhereClause(criterias);  
            
        System.assertEquals(' WHERE (name LIKE \'%John\' AND ( id LIKE \'ABC12%\' OR id = \'CBA321\'))', query);  
    }
    
    public static testMethod void searchingCriteriaTest02() {        
    	// Test - Incomplete criterias
        SearchingCriteria s0 = new SearchingCriteria();
        SearchingCriteria s1 = new SearchingCriteria(2, false, 'name', 'e', 'John');
        SearchingCriteria s2 = new SearchingCriteria(3, false, 'id', 's', 'ABC12', 'OR');         
        SearchingCriteria s4 = new SearchingCriteria(4, true, 'id', 'q', 'CBA321', 'AND'); 
        SearchingCriteria s3 = new SearchingCriteria(5);      
        
        List<SearchingCriteria> criterias = new SearchingCriteria[]{s0, s1, s2, s3, s4};
        
        String query = SearchingCriteria.formWhereClause(criterias);  
            
        System.assertEquals(' WHERE (name LIKE \'%John\' AND ( id LIKE \'ABC12%\' OR id = \'CBA321\'))', query);  
    }
    
    public static testMethod void searchingCriteriaTest03() {        
    	// Test - Advanced Filter Conditions
        SearchingCriteria s0 = new SearchingCriteria();
        SearchingCriteria s1 = new SearchingCriteria(2, false, 'name', 'e', 'John');
        SearchingCriteria s2 = new SearchingCriteria(3, false, 'id', 's', 'ABC12', 'OR');         
        SearchingCriteria s4 = new SearchingCriteria(4, true, 'id', 'q', 'CBA321', 'AND'); 
        SearchingCriteria s3 = new SearchingCriteria(5);      
        
        List<SearchingCriteria> criterias = new SearchingCriteria[]{s0, s1, s2, s3, s4};
        
        String query = SearchingCriteria.formWhereClause(criterias, '2 AND (3 OR 4)');  
            
        System.assertEquals(' WHERE (name LIKE \'%John\' AND (id LIKE \'ABC12%\' OR id = \'CBA321\'))', query);  
    }
    
    public static testMethod void searchingCriteriaWhereClauseTest() {
        String f = 'Name';
        String v = 'ABC';
        
        System.assertEquals('Name = \'ABC\'', SearchingCriteria.getWhereClause(f,'q',v).builderWhereClause());
        System.assertEquals('Name != \'ABC\'', SearchingCriteria.getWhereClause(f,'n',v).builderWhereClause());
        System.assertEquals('Name LIKE \'ABC%\'', SearchingCriteria.getWhereClause(f,'s',v).builderWhereClause());
        System.assertEquals('Name LIKE \'%ABC\'', SearchingCriteria.getWhereClause(f,'e',v).builderWhereClause());
        System.assertEquals('Name LIKE \'%ABC%\'', SearchingCriteria.getWhereClause(f,'c',v).builderWhereClause());
        System.assertEquals('(NOT Name LIKE \'%ABC%\')', SearchingCriteria.getWhereClause(f,'k',v).builderWhereClause());
        System.assertEquals('Name IN(ABC)', SearchingCriteria.getWhereClause(f,'u',v).builderWhereClause());
        System.assertEquals('Name NOT IN(ABC)', SearchingCriteria.getWhereClause(f,'x',v).builderWhereClause());
    }
    
// Test General
	
	public static testMethod void getBookTitleTest() {
 		Test.startTest();
 		Product2 product2 = getBookTitle();
		Test.stopTest();
	}
	
// Data Preparation

	private static SelectCampaignTargetProductsCon preparePageWithoutParameters() {		
		SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
		System.assert(selectCampaignTargetProductsCon != null);
		return selectCampaignTargetProductsCon;
	}
	
	private static SelectCampaignTargetProductsCon preparePageWithParameters() {
		// Add parameters to page URL
        System.currentPageReference().getParameters().put('Campaign_Id' /*SelectCampaignTargetProductsCon.ACCOUNT_ID_PARAM*/, getCampaign().Id);
			
		SelectCampaignTargetProductsCon selectCampaignTargetProductsCon = new SelectCampaignTargetProductsCon();
		System.assert(selectCampaignTargetProductsCon != null);
		return selectCampaignTargetProductsCon;
	}
	
    private static Campaign getCampaign() {
        Campaign[] campaigns = [SELECT Id FROM Campaign LIMIT 1];
        System.assert(campaigns.size() >= 1, 'Please create at least 1 Campaign for testing');
        return campaigns[0];
    }
    
    private static Product2 getBookTitle() {
        Product2[] products = [select Id from Product2 limit 1];
        System.assert(products.size() >= 1, 'Please create at least 1 Product(Book Title) for testing');
        return products[0];
    }
    
    private static void testPreLoadItems(SelectCampaignTargetProductsCon controller) {
    	List<SelectCampaignTargetProductsCon.TargetProduct> targetProduct = controller.getSelectedProducts();
    	for (SelectCampaignTargetProductsCon.TargetProduct tp : targetProduct) {
    		// if (tp.getIsPreLoad() == true) {
    		//	 System.assertEquals(null, tp.ISBN);
    		//	 System.assertEquals(null, tp.getBookTitle());    			
    		// }
    	}    	
    }
}