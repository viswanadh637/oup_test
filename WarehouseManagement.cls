public class WarehouseManagement
{

public static boolean userHasWarehouse(ID userID)
{
    //VERIFY IF USER OWNS A WAREHOUSE;
    List<Warehouse__c> wlist = [select id from Warehouse__c where Warehouse_Owner__c =: userID];
    
    if (wlist.size() > 0)
    {
    return true;
    }
    else
    {
        //VERIFY IF USER IS ADDITIONAL USER IN THE WAREHOUSE
        List<Warehouse_User__c> wulist =  [select id, Warehouse__c from Warehouse_User__c where User__c =: userID];
        
        if (wulist.size() > 0)
        {
        return true;
        }
        else
        {
        return false;
        }    
    }

}

public static Warehouse__c getWarehouseByUser(ID userID)
{


 //VERIFY IF USER OWNS A WAREHOUSE;
    List<Warehouse__c> wlist = [select id from Warehouse__c where Warehouse_Owner__c =: userID];
    
    if (wlist.size() > 0)
    {
    return wlist[0];
    }
    else
    {
        //VERIFY IF USER IS ADDITIONAL USER IN THE WAREHOUSE
        List<Warehouse_User__c> wulist =  [select id, Warehouse__c from Warehouse_User__c where User__c =: userID];
        
        if (wulist.size() > 0)
        {
        wlist = [select id from Warehouse__c where id =: wulist[0].Warehouse__c];
        return wlist[0];
        }
        else
        {
        return null;
        }    
    }

}

public static void createTransfer(Warehouse_Document__c masterDocument,  List<Warehouse_Document_Line__c> documentList)
{

Warehouse_Document__c targetDocument = new Warehouse_Document__c();

targetDocument.RecordTypeId =  [Select Id from RecordType where DeveloperName = 'Receipt'][0].Id;
targetDocument.Type__c = 'Transfer - In';
targetDocument.Warehouse__c = masterDocument.Transfer_Warehouse__c;
targetDocument.Transfer_Warehouse__c = masterDocument.Warehouse__c;
targetDocument.Date__c = Date.today();
targetDocument.Status__c = 'New';

Database.SaveResult sr = database.insert(targetDocument);

List<Warehouse_Document_Line__c> targetList = new List<Warehouse_Document_Line__c>();

for (Warehouse_Document_Line__c wdl :  documentList)
{

Warehouse_Document_Line__c targetLine = new Warehouse_Document_Line__c();
targetLine.Document__c = sr.getId();
targetLine.WD_book_title__c = wdl.WD_book_title__c;
targetLine.quantity__c = wdl.quantity__c;
targetList.Add(targetLine);
}

database.insert(targetList);


}

public static void updateStock(ID owner, List<Warehouse_Document_Line__c> documentList, string action)  {

Warehouse_Document__c stock;
Warehouse__c wh;

if (WarehouseManagement.userHasWarehouse(owner))
{
wh = WarehouseManagement.getWarehouseByUser(owner);
stock = [Select id, Status__c, RecordType.Name from Warehouse_Document__c where Warehouse__c=:wh.id and RecordType.Name = 'Stock'];
}

if (stock != null)
{
updateDocument(owner, documentList, action, stock);
}
//else
//{
//wd.addError('No stock document available');
//}
//end null stock

}

public static void updateDocument(ID owner, List<Warehouse_Document_Line__c> documentList, string action, Warehouse_Document__c doc)  {

if (doc.RecordType.Name == 'Stock')
{
//if stock temporarily change status to be able to update
doc.Status__c = 'Updating'; 
doc.Date__c = Date.Today();
database.update(doc);
}

List<Warehouse_Document_Line__c> docList = new List<Warehouse_Document_Line__c>();
List<Warehouse_Document_Line__c> updateList = new List<Warehouse_Document_Line__c>();
List<Warehouse_Document_Line__c> createList = new List<Warehouse_Document_Line__c>();

//get all docLines
docList = [select id, WD_book_title__c, quantity__c from Warehouse_Document_Line__c where Document__c =:doc.id];

For (Warehouse_Document_Line__c documentLine: documentList )
{
Boolean isMatched = false; 


//Match against updateList
if(!isMatched)
{
For (Warehouse_Document_Line__c docLine: updateList)
{
//check if products match
if(documentLine.WD_book_title__c == docLine.WD_book_title__c)
{
isMatched = true;
//inventory
if(action == 'update')
{
docLine.quantity__c = documentLine.quantity__c;
}
//receipt
if(action == 'add')
{
docLine.quantity__c = docLine.quantity__c + documentLine.quantity__c;
}
//shipment
if(action == 'substract')
{
docLine.quantity__c = docLine.quantity__c - documentLine.quantity__c;
}
}
//end match products
}
//stock loop
}
//Match updateList

//Match against createList
if(!isMatched)
{
For (Warehouse_Document_Line__c docLine: createList)
{
//check if products match
if(documentLine.WD_book_title__c == docLine.WD_book_title__c)
{
isMatched = true;
//inventory
if(action == 'update')
{
docLine.quantity__c = documentLine.quantity__c;
}
//receipt
if(action == 'add')
{
docLine.quantity__c = docLine.quantity__c + documentLine.quantity__c;
}
//shipment
if(action == 'substract')
{
docLine.quantity__c = docLine.quantity__c - documentLine.quantity__c;
}
}
//end match products
}
//stock loop
}
//Match createList


//Match against docList

if(!isMatched)
{
For (Warehouse_Document_Line__c docLine: docList)
{
Warehouse_Document_Line__c wdl = new Warehouse_Document_Line__c ();
//check if products match
if(documentLine.WD_book_title__c == docLine.WD_book_title__c)
{
isMatched = true;
wdl.id = docLine.id;
wdl.WD_book_title__c  = docLine.WD_book_title__c;
//inventory
if(action == 'update')
{
wdl.quantity__c = documentLine.quantity__c;
}
//receipt
if(action == 'add')
{
wdl.quantity__c = docLine.quantity__c + documentLine.quantity__c;
}
//shipment
if(action == 'substract')
{
wdl.quantity__c = docLine.quantity__c - documentLine.quantity__c;
}
updateList.Add(wdl);
}
//end match products
}
//stock loop
}
//Match docList

if (!isMatched)
{
Warehouse_Document_Line__c wdlNew = new Warehouse_Document_Line__c ();
wdlNew.Document__c = doc.id;
wdlNew.WD_book_title__c = documentLine.WD_book_title__c;
//inventory
if(action == 'update')
{
wdlNew.quantity__c = documentLine.quantity__c;
}
//receipt
if(action == 'add')
{
wdlNew.quantity__c = documentLine.quantity__c;
}
//shipment
if(action == 'substract')
{
wdlNew.quantity__c = 0 - documentLine.quantity__c;
}
createList.Add(wdlNew);

}
//not matched
}
//document loop
database.insert(createList);
database.update(updateList);
if (doc.RecordType.Name == 'Stock')
{
//if stock END temporarily change status to be able to update
doc.Status__c = 'Approved';
database.update(doc);
}
}

}