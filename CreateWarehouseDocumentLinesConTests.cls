@isTest(SeeAllData=true)
private class CreateWarehouseDocumentLinesConTests {
    
// Test Contoller
        
    public static testMethod void getQuery() {
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
        createWarehouseDocumentLinesCon.getQuery();  
    }
    
    public static testMethod void saveTest() {
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
        createWarehouseDocumentLinesCon.save();  
    }
    
    public static testMethod void cancelTest() {
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
        createWarehouseDocumentLinesCon.cancel();
    }   
        
    public static testMethod void saveInternalTest() {
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
        System.assert(createWarehouseDocumentLinesCon.saveInternal() == false);
    }
        
    public static testMethod void getRenderSearchResultsTest() {
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
        Boolean status = createWarehouseDocumentLinesCon.getRenderSearchResults();
        System.assert(status == false);
    }
    
    public static testMethod void initialSearchingCriteriasTest() {
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
        List<SearchingCriteria> criterias = createWarehouseDocumentLinesCon.getSearchingCriterias();   
        System.assertEquals(5, criterias.size());               // ensure that's 5 empty criterias loaded
        System.assertEquals('Name', criterias.get(0).field);    // ensure first criteria default to book title field
        System.assertEquals('Name', criterias.get(1).field);    // ensure first criteria default to ISBN field  
    }
    
    public static testMethod void getFieldsTest() {
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
        List<SelectOption> selectOption = createWarehouseDocumentLinesCon.getFields();
        System.assert(selectOption != null);
    }
        
    public static testMethod void getLookupProductsTest() {
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
        List<CreateWarehouseDocumentLinesCon.SelectableProduct> lookupProducts = createWarehouseDocumentLinesCon.getLookupProducts();     
        System.assert(lookupProducts != null);
    }
    
    public static testMethod void getSearchingCriteriasTest() {
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
        List<SearchingCriteria> searchingCriterias = createWarehouseDocumentLinesCon.getSearchingCriterias();        
        System.assert(searchingCriterias != null);
    }
    
    public static testMethod void searchByFieldTest() {
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
        System.assert(createWarehouseDocumentLinesCon.searchByField() == null);
    }
    
    public static testMethod void searchTest() {
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
        createWarehouseDocumentLinesCon.getPricebooks();
        createWarehouseDocumentLinesCon.Search();
        createWarehouseDocumentLinesCon.searchField = 'search_byName';
        createWarehouseDocumentLinesCon.Search();
        createWarehouseDocumentLinesCon.searchField = 'search_byISBN';
        createWarehouseDocumentLinesCon.Search();
        createWarehouseDocumentLinesCon.searchField = 'search_byPublisher';
        createWarehouseDocumentLinesCon.Search();      
         createWarehouseDocumentLinesCon.searchField = 'search_byMarket';
        createWarehouseDocumentLinesCon.Search(); 
         createWarehouseDocumentLinesCon.searchField = 'search_bySegment';
        createWarehouseDocumentLinesCon.Search(); 
          createWarehouseDocumentLinesCon.searchField = 'search_bySeries';
        createWarehouseDocumentLinesCon.Search(); 
                  
    }
        
    public static testMethod void addSelectedProductsTest() {
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
        createWarehouseDocumentLinesCon.addSelectedProducts();
       // createWarehouseDocumentLinesCon.removeSelectedProduct();
    }
    
    public static testMethod void getPageTitleTest() {
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
        String pageTitle = createWarehouseDocumentLinesCon.getPageTitle();
        System.assert(pageTitle.length() > 0);
    }
    
    //public static testMethod void removeSelectedProductTest() {
    //    CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
    //    createWarehouseDocumentLinesCon.removeSelectedProduct();
    //}
    
    
    public static testMethod void contollerWithoutParameters() {
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = preparePageWithoutParameters();
        System.assert(createWarehouseDocumentLinesCon != null);
    }
        
    public static testMethod void contollerWithDocParameters() {
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = preparePageWithDocParameters();
        System.assert(createWarehouseDocumentLinesCon != null);
        
        createWarehouseDocumentLinesCon.resetSearchingCriterias();
        List<SearchingCriteria> criterias = createWarehouseDocumentLinesCon.getSearchingCriterias();
        System.assertEquals(criterias.size(), 5);
        
        System.assertEquals(criterias.get(0).index, 1);
        System.assertEquals(criterias.get(0).field, 'Name');
        System.assertEquals(criterias.get(0).operator, 'c');
        System.assertEquals(criterias.get(0).logic, 'AND');
        System.assertEquals(criterias.get(0).value, '');
        
        System.assertEquals(criterias.get(1).index, 2);
        System.assertEquals(criterias.get(1).field, 'Name');
        System.assertEquals(criterias.get(1).operator, 'c');
        System.assertEquals(criterias.get(1).value, '');          
        createWarehouseDocumentLinesCon.selectedPricebook = 'None';
        createWarehouseDocumentLinesCon.Search();
        List<CreateWarehouseDocumentLinesCon.SelectableProduct> lookupProducts = createWarehouseDocumentLinesCon.getLookupProducts();
        System.debug(lookupProducts.size());
        for (CreateWarehouseDocumentLinesCon.SelectableProduct product : lookupProducts) {
            System.assertEquals(product.isSelected, false);
            product.isSelected = true;
            System.assertEquals(product.isSelected, true);
            product.isSelected = false;
            System.assertEquals(product.isSelected, false);
        }
        
        //System.assert(lookupProducts.size() >= 1, 'Please create at least 1 Account(Insitution) for testing');
        lookupProducts.get(0).isSelected = true;
        
        createWarehouseDocumentLinesCon.addSelectedProducts();
        List<CreateWarehouseDocumentLinesCon.DocumentLines> selectedProduct = createWarehouseDocumentLinesCon.getSelectedProducts();
        
        System.assertEquals(1/* CreateWarehouseDocumentLinesCon.NO_OF_PRELOAD_ITEMS */, selectedProduct.size()); // check the list size should be 1 only
        System.assertEquals(lookupProducts.get(0).product.id, selectedProduct.get(0).getDocumentLines().WD_book_title__c ); // ensure both object refer to the same instance
        
        createWarehouseDocumentLinesCon.addSelectedProducts();
        System.assertEquals(1 /* CreateWarehouseDocumentLinesCon.NO_OF_PRELOAD_ITEMS */, selectedProduct.size()); // check the list size should be 1 only
        
        selectedProduct.get(0).needSave();
        selectedProduct.get(0).validate();
        
        // new added item will appear at the end of the list
        Warehouse_Document_Line__c lineItem = selectedProduct.get(0).getDocumentLines();
        lineItem.Quantity__c = 1;
        
        //String nextPage = createWarehouseDocumentLinesCon.save().getUrl();
        //nextPage = createWarehouseDocumentLinesCon.cancel().getUrl();        
    }
    
// Test BooksUsed
    
    public static testMethod void booksUsedPreLoadItemsTest() {
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
        Test.startTest();
        testPreLoadItems(createWarehouseDocumentLinesCon);
        Test.stopTest();
    }
    
    public static testMethod void booksUsedTest() {
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
        Warehouse_Document_Line__c bu = createDocumentLines();
        CreateWarehouseDocumentLinesCon.DocumentLines booksUsed01 = new CreateWarehouseDocumentLinesCon.DocumentLines(null, bu);
        System.assert(!booksUsed01.needSave());
                
        CreateWarehouseDocumentLinesCon.DocumentLines booksUsed02 = new CreateWarehouseDocumentLinesCon.DocumentLines(getBookTitle(), bu);
        System.assert(booksUsed02.needSave());      
    }
    
    public static testMethod void createBooksUsedTest() {       
        Warehouse_Document_Line__c booksUsed = createDocumentLines();
        System.assert(booksUsed != null);       
    }
        
// Test SelectableProduct

    public static testMethod void selectableProductTest() {
        Product2 p = new Product2();
        CreateWarehouseDocumentLinesCon.SelectableProduct sp = new CreateWarehouseDocumentLinesCon.SelectableProduct(p);
        sp.isSelected = true;
        System.assert(true, sp.Product === p);
        System.assert(true, sp.isSelected); 
    }
    
// Test SearchingCriteria
    
    public static testMethod void searchingCriteriaTest01() {        
        SearchingCriteria s0 = new SearchingCriteria();
        SearchingCriteria s1 = new SearchingCriteria(2, false, 'name', 'e', 'John');
        SearchingCriteria s2 = new SearchingCriteria(3, false, 'id', 's', 'ABC12', 'OR'); 
        SearchingCriteria s3 = new SearchingCriteria(4);
        SearchingCriteria s4 = new SearchingCriteria(5, true, 'id', 'q', 'CBA321', 'AND');       
        
        List<SearchingCriteria> criterias = new SearchingCriteria[]{s0, s1, s2, s3, s4};
        
        String query = SearchingCriteria.formWhereClause(criterias);  
            
        System.assertEquals(' WHERE (name LIKE \'%John\' AND ( id LIKE \'ABC12%\' OR id = \'CBA321\'))', query);  
    }
    
    public static testMethod void searchingCriteriaTest02() {        
        // Test - Incomplete criterias
        SearchingCriteria s0 = new SearchingCriteria();
        SearchingCriteria s1 = new SearchingCriteria(2, false, 'name', 'e', 'John');
        SearchingCriteria s2 = new SearchingCriteria(3, false, 'id', 's', 'ABC12', 'OR');         
        SearchingCriteria s4 = new SearchingCriteria(4, true, 'id', 'q', 'CBA321', 'AND'); 
        SearchingCriteria s3 = new SearchingCriteria(5);      
        
        List<SearchingCriteria> criterias = new SearchingCriteria[]{s0, s1, s2, s3, s4};
        
        String query = SearchingCriteria.formWhereClause(criterias);  
            
        System.assertEquals(' WHERE (name LIKE \'%John\' AND ( id LIKE \'ABC12%\' OR id = \'CBA321\'))', query);  
    }
    
    public static testMethod void searchingCriteriaTest03() {        
        // Test - Advanced Filter Conditions
        SearchingCriteria s0 = new SearchingCriteria();
        SearchingCriteria s1 = new SearchingCriteria(2, false, 'name', 'e', 'John');
        SearchingCriteria s2 = new SearchingCriteria(3, false, 'id', 's', 'ABC12', 'OR');         
        SearchingCriteria s4 = new SearchingCriteria(4, true, 'id', 'q', 'CBA321', 'AND'); 
        SearchingCriteria s3 = new SearchingCriteria(5);      
        
        List<SearchingCriteria> criterias = new SearchingCriteria[]{s0, s1, s2, s3, s4};
        
        String query = SearchingCriteria.formWhereClause(criterias, '2 AND (3 OR 4)');  
            
        System.assertEquals(' WHERE (name LIKE \'%John\' AND (id LIKE \'ABC12%\' OR id = \'CBA321\'))', query);  
    }
    
    public static testMethod void searchingCriteriaWhereClauseTest() {
        String f = 'Name';
        String v = 'ABC';
        
        System.assertEquals('Name = \'ABC\'', SearchingCriteria.getWhereClause(f,'q',v).builderWhereClause());
        System.assertEquals('Name != \'ABC\'', SearchingCriteria.getWhereClause(f,'n',v).builderWhereClause());
        System.assertEquals('Name LIKE \'ABC%\'', SearchingCriteria.getWhereClause(f,'s',v).builderWhereClause());
        System.assertEquals('Name LIKE \'%ABC\'', SearchingCriteria.getWhereClause(f,'e',v).builderWhereClause());
        System.assertEquals('Name LIKE \'%ABC%\'', SearchingCriteria.getWhereClause(f,'c',v).builderWhereClause());
        System.assertEquals('(NOT Name LIKE \'%ABC%\')', SearchingCriteria.getWhereClause(f,'k',v).builderWhereClause());
        System.assertEquals('Name IN(ABC)', SearchingCriteria.getWhereClause(f,'u',v).builderWhereClause());
        System.assertEquals('Name NOT IN(ABC)', SearchingCriteria.getWhereClause(f,'x',v).builderWhereClause());
    }
    
// Test General
    
    public static testMethod void getBookTitleTest() {
        Test.startTest();
        Product2 product2 = getBookTitle();
        Test.stopTest();
    }
    
// Data Preparation

    private static CreateWarehouseDocumentLinesCon preparePageWithoutParameters() {      
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
        System.assert(createWarehouseDocumentLinesCon != null);
        return createWarehouseDocumentLinesCon;
    }
    
    private static CreateWarehouseDocumentLinesCon preparePageWithDocParameters() {
        // Add parameters to page URL
        System.currentPageReference().getParameters().put('Document_Id' /*CreateWarehouseDocumentLinesCon.ACCOUNT_ID_PARAM*/, 'a0Xg0000002WYgm');
            
        CreateWarehouseDocumentLinesCon createWarehouseDocumentLinesCon = new CreateWarehouseDocumentLinesCon();
        System.assert(createWarehouseDocumentLinesCon != null);
        return createWarehouseDocumentLinesCon;
    }
    
    private static Warehouse_Document_Line__c createDocumentLines() {
        Warehouse_Document_Line__c booksUsed = new Warehouse_Document_Line__c (Document__c = 'a0Xg0000002WYgm');
        System.assert(booksUsed != null, 'Please create at least 1 BookUsed for testing');
        return booksUsed;
    }
    
    //private static Account getInstitution() {
    //    Account[] accounts = [select Id from Account limit 1];
    //    System.assert(accounts.size() >= 1, 'Please create at least 1 Account(Insitution) for testing');
    //    return accounts[0];
    //}
    
    private static Product2 getBookTitle() {
        Product2[] products = [select Id from Product2 limit 1];
        System.assert(products.size() >= 1, 'Please create at least 1 Product(Book Title) for testing');
        return products[0];
    }
    
    private static void testPreLoadItems(CreateWarehouseDocumentLinesCon controller) {
        List<CreateWarehouseDocumentLinesCon.DocumentLines> documentLines = controller.getSelectedProducts();
        for (CreateWarehouseDocumentLinesCon.DocumentLines bu :documentLines ) {
            if (bu.getIsPreLoad() == true) {
                System.assertEquals(null, bu.ISBN);
                System.assertEquals(null, bu.getBookTitle());               
            }
        }       
    }
}