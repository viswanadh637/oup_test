public class AmiandoIntegration
{
	  
        public String exMessage { get; set; }
        public static final String CAMPAIGN_URL_PARAM = 'Id';
        public String identifier { get; set; }
        public Boolean useCategories { get; set; }
        public Boolean disableCategoriesChoice {get; set;}
        public string eventId {get; set;}
        public string eventName {get; set;}
        public string campaignName {get; set;}
        public string campaignNameLinked {get; set;}
        public string campaignIdLinked {get; set;}
        public boolean canMatch {get; set;}
        public boolean canProceed {get; set;}
        public boolean isConnected {get; set;}
        public boolean showLinkedCamp {get; set;}
        private final ID campaignId;
        public List<SelectOption> categories {get; set;}
        public List<String> categoriesUsed {get; set;}
        public List<String> categoriesUsedIds {get; set;}
        public List<String> categoriesIds {get; set;}
        public String category {get; set;}
        public String ticketCategories {get; set;}
        
        //constructor
        public AmiandoIntegration()
        {
                   try 
                   {
                    canProceed = true;
                    campaignId = System.currentPageReference().getParameters().get(CAMPAIGN_URL_PARAM);
                        if (campaignId != null)
                        {
                        List<Campaign> camps = new List<campaign>();
                        camps = [Select Id, Name, Amiando_ID__c, Amiando_Event_Name__c  FROM Campaign c WHERE c.Id =: campaignId];
                        campaignName = camps[0].Name;
                            if (camps[0].Amiando_ID__c == null)
                            {
                            isConnected = false;
                            }
                            else 
                            {
                            eventName = camps[0].Amiando_Event_Name__c;
                            isConnected = true;
                            }
                        }
                    canMatch = false;
                    }
                    catch (Exception ex)
                    {
                    canProceed = false;     
                    exMessage = ex.getMessage();      
                    }
        }


//method for removal of associated Amiando Event
public PageReference btnRemoveWarning() {
try 
{
        List<Campaign> camps = new List<campaign>();
  camps = [Select Id, Name FROM Campaign c WHERE c.Id =: campaignId];
 camps[0].Amiando_ID__c = '';
  camps[0].Run_Amiando_Integration__c = false;
  camps[0].Amiando_Event_Name__c = '';
  camps[0].Category_ID__c = '';
  camps[0].Category_Name__c = '';
  
  update camps;
  return new PageReference('/'+campaignId);
   }
          catch (Exception ex)
          {
          canProceed = false;     
          exMessage = ex.getMessage();      
           return null;     
          }
 }

        //method used to start the process even when a campaign is already matched
        public void btnYesWarning() 
        {
            eventName = '';
            isConnected = false;
        }

        //method used to cancel process when a campaign is already matched
        public PageReference btnNoWarning() 
        {
             return new PageReference('/'+campaignId);
        }

        //matching campaign with Amiando Event
        public PageReference match()
        {
             try 
             {
                List<Campaign> camps = new List<campaign>();
                camps = [Select Id, Name FROM Campaign c WHERE c.Id =: campaignId];
                camps[0].Amiando_ID__c = eventId;
                camps[0].Run_Amiando_Integration__c = true;
                camps[0].Amiando_Event_Name__c = eventName;
                if(useCategories)
                {
                camps[0].Category_ID__c = category;
                
                for (SelectOption so: categories)
                {
                if (so.getValue() == category)
                {
                   camps[0].Category_Name__c = so.getLabel();
                }
                }
                }
                else
                {
                camps[0].Category_ID__c = null;
                camps[0].Category_Name__c = '';
                
                }
                update camps;
                return new PageReference('/'+campaignId);
             }
             catch (Exception ex)
             {
                 canProceed = false;     
                 exMessage = ex.getMessage(); 
                 return null;     
             }
        
        }
        //method used for getting Amiando Event details - not to use in tests as it calls out
        public void getEvent()
        {
            try 
            {
                eventId= '';
                HttpRequest req = buildWebServiceRequest('http://www.amiando.com/api/event/find?apikey=t8YCKlHoqWQWC8z7dKD9FAYcySnWRrtf315FCrKI3RmyhoO75f&format=json&version=1&identifier='+ identifier );     
                Http http = new Http();  
                HTTPResponse res = invokeWebService(http, req);
                eventId = eventId(res.getBody());
                //verify whether there is an Amiando Event with the identifier
                if (eventId != '')
                {
                req = buildWebServiceRequest('http://www.amiando.com/api/event/'+eventId+'?apikey=t8YCKlHoqWQWC8z7dKD9FAYcySnWRrtf315FCrKI3RmyhoO75f&format=json&version=1');
                http = new Http();  
                res = invokeWebService(http, req);
                eventName = eventName(res.getBody()); 
                if (eventName == '')
                {
                    canProceed = false;     
                    exMessage = 'Verify if your Event Identifier is valid: ' + identifier;    
                }
                else
                {
                verifyIfMatched();
                if (useCategories)
                {
                getCategoriesIds();
                getCategories();           
                }
                }
                }
                else
                {
                eventName = 'No such Amiando Identifier';
                }   
             
             
             
             }
             catch (Exception ex)
             {
             canProceed = false;     
             exMessage = ex.getMessage();      
             }
         }
 
     //method used to build Web Service request
     public HttpRequest buildWebServiceRequest(String endpoint)
     {
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        return req;
     }
 
     //method used to Invoke Web Service
     public HttpResponse invokeWebService(Http h, HttpRequest req)
     {
     HttpResponse res = h.send(req);
     return res;
     }
     
     
     //getting the ID of the event from Amiando API response
     public string eventId(string body)
     {
        JSONParser idsJSON = JSON.createParser(body);
        while (idsJSON.NextToken() != null)
            {
                if (idsJSON.getCurrentToken() == JSONToken.VALUE_NUMBER_INT)
                {
                return idsJSON.getText();
                }
            }
    
        return '';
    }
    
    //getting the name of the event from Amiando API response
    public string eventName(string body)
    {
        
        JSONParser  idsJSON = JSON.createParser(body);
        while (idsJSON.NextToken() != null)
            {
                if (idsJSON.getText() == 'title')
                {
                idsJSON.NextToken();
                return eventName = idsJSON.getText();
                }
            } 
        return '';
    }
        public string categoryName(string body)
    {
        string categoryName = '';
        JSONParser  idsJSON = JSON.createParser(body);
        while (idsJSON.NextToken() != null)
            {
                if (idsJSON.getText() == 'name')
                {
                idsJSON.NextToken();
                return categoryName = idsJSON.getText();
                }
            } 
        return '';
    }
    
    
    public string getCategoryName(string id)
    {
        try
          {
           HttpRequest    req = buildWebServiceRequest('http://www.amiando.com/api/ticketCategory/'+id+'?apikey=t8YCKlHoqWQWC8z7dKD9FAYcySnWRrtf315FCrKI3RmyhoO75f&format=json&version=1');
           Http     http = new Http();  
           HTTPResponse   res = invokeWebService(http, req);
           return categoryName(res.getBody());
          }
          catch (Exception ex)
          {
          return 'Error';      
          }
    }
    
    public void getCategoriesIds()
    {
            try 
            {
             categoriesIds = new List<String>();
             HttpRequest   req = buildWebServiceRequest('http://www.amiando.com/api/event/'+eventId+'/ticketCategories?apikey=t8YCKlHoqWQWC8z7dKD9FAYcySnWRrtf315FCrKI3RmyhoO75f&format=json&version=1');
             Http    http = new Http();  
             HTTPResponse  res = invokeWebService(http, req);
             JSONParser  idsJSON = JSON.createParser(res.getBody());
                 
             while (idsJSON.NextToken() != null)
            {
                if (idsJSON.getText() == 'ticketCategories')
                {
                idsJSON.NextToken();
                while (idsJSON.getText() != ']')
                {
                idsJSON.NextToken();
                if(idsJSON.getText() != ']')
                { 
                categoriesIds.add(idsJSON.getText());
                }
                }
                }
            }
            }
            catch (Exception ex)
            {
            }
     }

     public void getCategories()
    {
             integer count = 1;
             categories = new List<SelectOption>();
          
           for (String cat : categoriesIds)
           {
                 Boolean getThisCategory = true;
                 for (string s :categoriesUsedIds)
                 {
                 if (s == cat)
                 {
                 getThisCategory = false;
                 }
                 }
  
                 if (getThisCategory)
                 {
                 
                 if (count < 8)
                 {
               
                  categories.add(new SelectOption(cat, getCategoryName(cat)));
                  count = count+1;
                  }
                  }
                }
            
            if (categories.size() == 0)
            {canProceed =  false; exMessage = 'No more event categories available for this event';}
            else
            {canMatch = true;}
    }
    
    public void verifyIfMatched()
    {
        //getting all campaigns with the Amiando ID found by the user
       List<Campaign> camps = new List<campaign>();
       categoriesUsedIds= new List<String>();
       categoriesUsed= new List<String>();
       camps = [Select Id, Name, Category_ID__c, Category_Name__c FROM Campaign c WHERE c.Amiando_ID__c =: eventId];
       
        //if there are none is ok to match            
        if (camps.size() == 0)
        { 
            canMatch = true;
            showLinkedCamp =false;
            disableCategoriesChoice = false;
        } 
        //if there is a camp with this Amiando ID do not allow to match and show the campaign with this ID
        else 
        {
        Boolean usedCategories = false;
         for(Campaign camp : camps) 
         {
              if (camp.Category_ID__c != null)
              {
               disableCategoriesChoice = true;
               useCategories = true;
               usedCategories = true;
               canMatch = false;
               categoriesUsedIds.add(camp.Category_ID__c);
               categoriesUsed.add(camp.Name + ': '+camp.Category_Name__c);
               
              }
         }
        if (!usedCategories)
        {
        disableCategoriesChoice = true;
        useCategories = false;
        canMatch = false;
        showLinkedCamp = true;
        campaignNameLinked = camps[0].Name;
        campaignIdLinked = camps[0].Id;
        }
        }
    }
}