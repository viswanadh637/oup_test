public class GratisItemManager {
//        
    public GratisItemManager() {        
    }   
//    
//    
//    //
//    // Campaign
//    //
//    
//    public void UpdateCampaignTargetProductTotal(Campaign[] campaignNew, Campaign[] campaignOld) {
//        if ((campaignNew == null) || (campaignOld == null)) {
//            return;
//        }
//        for (Campaign camNew : campaignNew) {
//            for (Campaign camOld : campaignOld) {
//                if (camNew.Id == camOld.Id) {
//                    // sync target product totals
//                    camNew.Target_Product_1_Total__c = SyncTargetProducts(camNew.Target_Product__c,   camNew, camOld);
//                    camNew.Target_Product_2_Total__c = SyncTargetProducts(camNew.Target_Product_2__c, camNew, camOld);
//                    camNew.Target_Product_3_Total__c = SyncTargetProducts(camNew.Target_Product_3__c, camNew, camOld);
//                    camNew.Target_Product_4_Total__c = SyncTargetProducts(camNew.Target_Product_4__c, camNew, camOld);
//                    camNew.Target_Product_5_Total__c = SyncTargetProducts(camNew.Target_Product_5__c, camNew, camOld);
//                    camNew.Target_Product_6_Total__c = SyncTargetProducts(camNew.Target_Product_6__c, camNew, camOld);
//                    // clear totals if there is no associated target product
//                    if (camNew.Target_Product__c   == null) { camNew.Target_Product_1_Total__c = null; }
//                    if (camNew.Target_Product_2__c == null) { camNew.Target_Product_2_Total__c = null; }
//                    if (camNew.Target_Product_3__c == null) { camNew.Target_Product_3_Total__c = null; }
//                    if (camNew.Target_Product_4__c == null) { camNew.Target_Product_4_Total__c = null; }
//                    if (camNew.Target_Product_5__c == null) { camNew.Target_Product_5_Total__c = null; }
//                    if (camNew.Target_Product_6__c == null) { camNew.Target_Product_6_Total__c = null; }
//                    
//                    break;      
//                }
//            } 
//        }
//    }
//    
//    private Double SyncTargetProducts(Id product, Campaign camNew, Campaign camOld) {
//        Double total = 0;
//        if (product == camOld.Target_Product__c) {
//            total = camNew.Target_Product_1_Total__c;
//        } else if (product == camOld.Target_Product_2__c) {
//            total = camNew.Target_Product_2_Total__c;
//        } else if (product == camOld.Target_Product_3__c) {
//            total = camNew.Target_Product_3_Total__c;
//        } else if (product == camOld.Target_Product_4__c) {
//            total = camNew.Target_Product_4_Total__c;
//        } else if (product == camOld.Target_Product_5__c) {
//            total = camNew.Target_Product_5_Total__c;
//        } else if (product == camOld.Target_Product_6__c) {
//            total = camNew.Target_Product_6_Total__c;
//        }       
//        return total;
//    }
//    
//    //
//    // Gratis_Item__c
//    //
//    
//    public void UpdateCampaignTargetProductTotal(Gratis_Item__c[] gis, Boolean increment) {
//        if (gis == null) {
//            return;
//        }
//        Set<ID> campaignIds = new Set<ID>();
//        campaignIds.addAll(getCampaignIds(gis));
//        Map<ID, Campaign> cpns = getCampaigns(campaignIds);
//    
//        List<ID> gisIds = new List<ID>();       
//        // iterate gratis items
//        for (Gratis_Item__c gi : gis) {
//            if (gi.Campaign__c != null) {
//                gisIds.add(gi.Id);              
//            }
//        }
//        // update/decrement product totals using campaign map/gratis items
//        UpdateCampaignProductTotals(cpns, gis, gisIds, increment);
//        // update campaign product totals       
//        database.update(cpns.values());
//    }
//    
//    public void UpdateCampaignTargetProductTotal(Gratis_Item__c[] gisNew, Gratis_Item__c[] gisOld) {
//        if ((gisNew == null) || (gisOld == null)) {
//            return;
//        }
//        // store list of campaign ids and retrieve campaigns
//        Set<ID> campaignIds = new Set<ID>();
//        campaignIds.addAll(getCampaignIds(gisNew));
//        campaignIds.addAll(getCampaignIds(gisOld));     
//        Map<ID, Campaign> cpns = getCampaigns(campaignIds);
//                
//        List<ID> gisIds = new List<ID>();
//        if (gisOld != null) {           
//            // iterate old gratis items
//            for (Gratis_Item__c giOld : gisOld) {
//                if (giOld.Campaign__c != null) {
//                    // decrement campaign product totals                    
//                    gisIds.add(giOld.Id);
//                }
//            }
//            // update/decrement product totals using campaign map/gratis items
//            updateCampaignProductTotals(cpns, gisOld, gisIds, false);
//        }
//        if (gisNew != null) {   
//            gisIds = new List<ID>();        
//            // iterate new gratis items
//            for (Gratis_Item__c giNew : gisNew) {
//                if (giNew.Campaign__c != null) {
//                    // increment campaign product totals
//                    gisIds.add(giNew.Id);               
//                }
//            }
//            // update/increment product totals using campaign map/gratis items
//            updateCampaignProductTotals(cpns, gisNew, gisIds, true);            
//        }       
//        // update campaign product totals
//        database.update(cpns.values());
//    }
//    
//    //
//    // Gratis_Line_Items__c
//    //
//    
//    public void UpdateCampaignTargetProductTotal(Gratis_Line_Items__c[] glis, Boolean increment) {
//        if (glis == null) {
//            return; 
//        }
//        // retrieve related gratis item
//        Set<ID> relateGratisItemIds = new Set<ID>();
//        for (Gratis_Line_Items__c gli : glis) {
//            relateGratisItemIds.add(gli.Related_Gratis_Item__c);                
//        }
//        // store list of campaign ids and retrieve campaigns
//        Set<ID> campaignIds = new Set<ID>();
//        campaignIds.addAll(getCampaignIds(relateGratisItemIds));
//        Map<ID, Campaign> cpns = getCampaigns(campaignIds);
//        
//        // iterate gratis line items
//        for (Gratis_Line_Items__c gli : glis) {
//            // determine campaign id from gratis line item related gratis item reference
//            ID campaignId = getCampaignId(gli, cpns);
//            if (campaignId != null) {
//                setTargetProductTotal(cpns.get(campaignId), gli, increment);
//            }
//        }
//        // update campaign product totals       
//        database.update(cpns.values());
//    }
//    
//    private void UpdateCampaignProductTotals(Map<ID, Campaign> cpns, Gratis_Item__c[] gi, List<ID> gisIds, Boolean increment) {
//        if ((cpns == null) && (gisIds == null) && (gi == null)) {
//            return;
//        }       
//        for (Gratis_Line_Items__c gli : [SELECT g.Title__c, g.Related_Gratis_Item__c, g.Id 
//                                         FROM Gratis_Line_Items__c g 
//                                         WHERE g.Related_Gratis_Item__c IN :gisIds]) {
//            for (Gratis_Item__c gitm : gi) {
//                // ensure that we use the correct campaign id
//                if (gitm.id == gli.Related_Gratis_Item__c) {
//                    // update the product total
//                    setTargetProductTotal(cpns.get(gitm.Campaign__c), gli, increment);
//                }
//            }                                           
//        }
//    }
//    
//    private Set<ID> getCampaignIds(Gratis_Item__c[] gis) {
//        Set<ID> campaignIds = new Set<ID>();
//        if (gis != null) {
//            for (Gratis_Item__c gi : gis) {
//                if (gi.Campaign__c != null) {
//                    campaignIds.add(gi.Campaign__c);
//                }
//            }               
//        }       
//        return campaignIds;
//    }
//    
//    private Set<ID> getCampaignIds(Set<ID> relateGratisItemIds) {
//        Set<ID> campaignIds = new Set<ID>();
//        if (relateGratisItemIds != null) {
//            for (Gratis_Item__c gi : [SELECT g.Id, g.Campaign__r.Id, g.Campaign__c 
//                                      FROM Gratis_Item__c g
//                                      WHERE g.Id IN :relateGratisItemIds]) {
//                campaignIds.add(gi.Campaign__r.Id);                                     
//            }
//        }       
//        return campaignIds;
//    }
//        
//    private ID getCampaignId(Gratis_Item__c gi, Gratis_Item__c[] gis) {
//        ID campaignId = null;
//        if ((gi == null) && (gis == null)) {
//            return campaignId;
//        }
//        // match gratis item from list of gratis items and set campaignId accordingly
//        for (Gratis_Item__c giCurrent : gis) {           
//            if (giCurrent.Id == gi.Id) {                 
//                campaignId = giCurrent.Campaign__c;
//                break;
//            }
//        }
//        return campaignId;
//    }
//    
//    private ID getCampaignId(Gratis_Line_Items__c gli, Map<ID, Campaign> cpns) {
//        ID campaignId = null;
//        if ((gli == null) && (cpns == null)) {
//            return campaignId;
//        }
//        // match gratis line item using related gratis item and set campaignId accordingly
//        for (ID key : cpns.keySet()) {
//            Gratis_Item__c[] gis = cpns.get(key).Gratis_Items1__r;
//            if (gis != null) { 
//                for (Gratis_Item__c gi : gis) {
//                    if (gi.Id == gli.Related_Gratis_Item__c) {
//                        campaignId = key;
//                        break;
//                    }
//                }
//                if (campaignId != null) {
//                    break;
//                }
//            }
//        }
//        return campaignId;      
//    }
//    
//    public Map<ID, Campaign> getCampaigns(Set<ID> campaignIds) {
//        Map<ID, Campaign> cpns = new Map<ID, Campaign>();
//        if ((campaignIds == null) || (campaignIds.size() == 0)) {
//            return cpns;
//        }       
//        cpns = new Map<ID, Campaign>([SELECT c.Id, c.Target_Product__r.Id, c.Target_Product_1_Total__c, c.Target_Product__c, 
//                                        c.Target_Product_2__r.Id, c.Target_Product_2_Total__c, c.Target_Product_2__c, 
//                                        c.Target_Product_3__r.Id, c.Target_Product_3_Total__c, c.Target_Product_3__c,
//                                        c.Target_product_4__r.Id, c.Target_Product_4_Total__c, c.Target_product_4__c,
//                                        c.Target_Product_5__r.Id, c.Target_Product_5_Total__c, c.Target_Product_5__c,
//                                        c.Target_Product_6__r.Id, c.Target_Product_6_Total__c, c.Target_Product_6__c,
//                                      (SELECT gi.Id, gi.Contact__c FROM Gratis_Items1__r gi)     
//                                      FROM Campaign c 
//                                      WHERE c.Id IN :campaignIds]);               
//        return cpns;
//    }
//
//    private void setTargetProductTotal(Campaign cpn, Gratis_Line_Items__c gli, Boolean increment) {
//        if ((cpn == null) && (gli == null)) {
//            return;
//        }
//        try {
//            if (cpn.Target_Product__c != null) {
//                if (gli.Title__c == cpn.Target_Product__c) {
//                    if (cpn.Target_Product_1_Total__c == null) {
//                        cpn.Target_Product_1_Total__c = 0;
//                    } 
//                    if (increment == true) {
//                        cpn.Target_Product_1_Total__c++;
//                    } else {
//                        if (cpn.Target_Product_1_Total__c > 0) { cpn.Target_Product_1_Total__c--; }
//                    }   
//                }
//            }
//            if (cpn.Target_Product_2__c != null) {
//                if (gli.Title__c == cpn.Target_Product_2__c) {
//                    if (cpn.Target_Product_2_Total__c == null) {
//                        cpn.Target_Product_2_Total__c = 0;
//                    }
//                    if (increment == true) {
//                        cpn.Target_Product_2_Total__c++;
//                    } else  {
//                        if (cpn.Target_Product_2_Total__c > 0) { cpn.Target_Product_2_Total__c--; }
//                    }   
//                }
//            }
//            if (cpn.Target_Product_3__c != null) {
//                if (gli.Title__c == cpn.Target_Product_3__c) {
//                    if (cpn.Target_Product_3_Total__c == null) {
//                        cpn.Target_Product_3_Total__c = 0;
//                    }
//                    if (increment == true) {
//                        cpn.Target_Product_3_Total__c++;
//                    } else  {
//                        if (cpn.Target_Product_3_Total__c > 0) { cpn.Target_Product_3_Total__c--; }
//                    }   
//                }
//            }
//            if (cpn.Target_Product_4__c != null) {
//                if (gli.Title__c == cpn.Target_Product_4__c) {
//                    if (cpn.Target_Product_4_Total__c == null) {
//                        cpn.Target_Product_4_Total__c = 0;
//                    }
//                    if (increment == true) {
//                        cpn.Target_Product_4_Total__c++;
//                    } else  {
//                        if (cpn.Target_Product_4_Total__c > 0) { cpn.Target_Product_4_Total__c--; }
//                    }   
//                }
//            }
//            if (cpn.Target_Product_5__c != null) {
//                if (gli.Title__c == cpn.Target_Product_5__c) {
//                    if (cpn.Target_Product_5_Total__c == null) {
//                        cpn.Target_Product_5_Total__c = 0;
//                    }
//                    if (increment == true) {
//                        cpn.Target_Product_5_Total__c++;
//                    } else  {
//                        if (cpn.Target_Product_5_Total__c > 0) { cpn.Target_Product_5_Total__c--; }
//                    }   
//                }
//            }
//            if (cpn.Target_Product_6__c != null) {
//                if (gli.Title__c == cpn.Target_Product_6__c) {
//                    if (cpn.Target_Product_6_Total__c == null) {
//                        cpn.Target_Product_6_Total__c = 0;
//                    }
//                    if (increment == true) {
//                        cpn.Target_Product_6_Total__c++;
//                    } else  {
//                        if (cpn.Target_Product_6_Total__c > 0) { cpn.Target_Product_6_Total__c--; }
//                    }   
//                }
//            }
//        
//        } catch (system.exception ex) {
//            system.debug('setTargetProductTotal::system.exception - ' + ex);
//        }
//    }
//    
//    
//    //
//    // test cases
//    //
//    
//    static testmethod void runPosTestCaseCampaign() {
//        final Integer NUM_CONTACTS = 10;
//        final Integer NUM_PRODUCTS = 10;
//        
//        // create contacts      
//        List<Contact> contacts = new List<Contact>(); 
//        for (Integer i = 0; i < NUM_CONTACTS; i++) {
//            contacts.add(new Contact(LastName='Tester'+i));         
//        } 
//        database.insert(contacts);
//        
//        // create products
//        List<Product2> products = new List<Product2>(); 
//        for (Integer i = 0; i < NUM_PRODUCTS; i++) {
//            products.add(new Product2(Name='Product'+i, Series__c='Series'+i));
//        }
//        database.insert(products);
//        
//        Test.startTest();
//
//        // create campaign
//        Campaign cpn = new Campaign(Name='Test Campaign', Target_Product__c=products[0].Id, Target_Product_2__c=products[1].Id,
//                                    Target_Product_3__c=products[2].Id,Target_Product_4__c=products[3].Id,
//                                    Target_Product_5__c=products[4].Id,Target_Product_6__c=products[5].Id,
//                                    Campaign_Objectives__c = 'Campaign Objective');
//        database.insert(cpn);
//        
//        // Test edit
//        Campaign cpnEdit = [SELECT Target_Product__c, Target_Product_2__c, Target_Product_3__c, Target_Product_4__c, Target_Product_5__c, Target_Product_6__c
//                            FROM Campaign
//                            WHERE Id =: cpn.Id];
//                
//        cpnEdit.Target_Product__c   = null;
//        database.update(cpnEdit);
//        cpnEdit.Target_Product_2__c = null;
//        database.update(cpnEdit);
//        cpnEdit.Target_Product_3__c = null;
//        database.update(cpnEdit);
//        cpnEdit.Target_Product_4__c = null;
//        database.update(cpnEdit);
//        cpnEdit.Target_Product_5__c = null;
//        database.update(cpnEdit);
//        cpnEdit.Target_Product_6__c = null;     
//        database.update(cpnEdit);
//        
//        Test.stopTest();            
//    }
//        
//    static testmethod void runPosTestCaseGratis() {
//        final Integer NUM_CONTACTS = 10;
//        final Integer NUM_PRODUCTS = 10;
//        
//        // create territory
//        OUP_Territory__c territory = new OUP_Territory__c(Name='Sample', OUP_Country__c='Brazil', Territory__c='ABC');
//        database.insert(territory);
//        
//        // create accounts
//        List<Account> accounts = new List<Account>(); 
//        for (Integer i = 0; i < NUM_CONTACTS; i++) {
//            accounts.add(new Account(Name='Test Account'+i, Territory__c=territory.Id, ShippingCity='London', 
//                                     ShippingPostalCode='LDN1', ShippingStreet='Brook'));           
//        } 
//        database.insert(accounts);
//        
//        // create contacts      
//        List<Contact> contacts = new List<Contact>(); 
//        for (Integer i = 0; i < NUM_CONTACTS; i++) {
//            contacts.add(new Contact(AccountId=accounts[i].Id, LastName='Tester'+i));           
//        } 
//        database.insert(contacts);
//        
//        // create products
//        List<Product2> products = new List<Product2>(); 
//        for (Integer i = 0; i < NUM_PRODUCTS; i++) {
//            products.add(new Product2(Name='Product'+i, Series__c='Series'+i));
//        }
//        database.insert(products);
//        
//        // create campaign(s)
//        Campaign cpn_1 = new Campaign(Name='Test Campaign 1', Target_Product__c=products[0].Id, Target_Product_2__c=products[1].Id,
//                                    Target_Product_3__c=products[2].Id, Target_Product_4__c=products[3].Id, 
//                                    Target_Product_5__c=products[4].Id, Target_Product_6__c=products[5].Id,
//                                    Campaign_Objectives__c = 'Campaign Objective');
//        Campaign cpn_2 = new Campaign(Name='Test Campaign 2', Target_Product__c=products[0].Id, Target_Product_2__c=products[1].Id,
//                                    Target_Product_3__c=products[2].Id, Target_Product_4__c=products[3].Id, 
//                                    Target_Product_5__c=products[4].Id, Target_Product_6__c=products[5].Id,
//                                    Campaign_Objectives__c = 'Campaign Objective');
//        database.insert(new List<Campaign>{cpn_1, cpn_2});
//        
//        // create gratis items
//        List<Gratis_Item__c> gratisItems01 = new List<Gratis_Item__c>();
//        for (Integer i = 0; i < NUM_CONTACTS; i++) {
//            gratisItems01.add(new Gratis_Item__c(Institution__c=accounts[i].Id, Contact__c=contacts[i].Id, Campaign__c=cpn_1.Id));
//        }       
//        List<Gratis_Item__c> gratisItems02 = new List<Gratis_Item__c>();
//        for (Integer i = 0; i < NUM_CONTACTS; i++) {
//            gratisItems02.add(new Gratis_Item__c(Institution__c=accounts[i].Id, Contact__c=contacts[i].Id, Campaign__c=cpn_2.Id));
//        }
//        database.insert(gratisItems01);
//        database.insert(gratisItems02);
//    
//        // create gratis line items
//        List<Gratis_Line_Items__c> gratisLineItems = new List<Gratis_Line_Items__c>(); 
//        for (Integer i = 0; i < NUM_CONTACTS; i++) {
//            for (Integer j = 0; j < NUM_PRODUCTS; j++) {
//                gratisLineItems.add(new Gratis_Line_Items__c(Related_Gratis_Item__c=gratisItems01[i].Id, Title__c=products[j].Id, Quantity__c=1));
//            }
//        }       
//        database.insert(gratisLineItems);
//        
//        // amend gratis line items
//        for (Integer i = 0; i < NUM_CONTACTS; i++) {
//            for (Gratis_Line_Items__c gli : gratisLineItems) {
//                gli.Title__c = null;
//            }
//        }
//        database.update(gratisLineItems);
//        // remove gratis line items
//        database.delete(gratisLineItems);
//        
//        // change gratis item campaign
//        for (Gratis_Item__c gi : gratisItems01) {
//            gi.Campaign__c=cpn_2.Id;
//        }
//        database.update(gratisItems01);
//        for (Gratis_Item__c gi : gratisItems02) {
//            gi.Campaign__c=null;
//        }
//        database.update(gratisItems02);
//        // remove gratis items
//        database.delete(gratisItems01);
//    }
}