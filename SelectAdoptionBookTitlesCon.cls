public class SelectAdoptionBookTitlesCon {
        
    public static final Integer NO_OF_PRELOAD_ITEMS = 10;    
    public static final String ADOPTION_ID_PARAM    = 'id';
    public static final String PRICE_BOOK_ID_PARAM  = 'PriceBook_Id';    
    public static final String RETURN_URL_PARAM     = 'retURL';
    private final ID adoptionId;
    private final ID priceBookId;
    private final String retURL;
    
    private final Opportunity opportunity;
    private List<SelectableProduct> lookupProducts;               // book titles product lookup list
    private List<AdoptionBookTitle> selectedProducts;
    private List<SearchingCriteria> searchingCriterias;                 
    private String query = '';                                    // query build by search criterias         
    public String filterAdvance { get; set; }
    public Boolean excludeInternationalTitles { get; set; }
    
    public String searchField { get; set; }
    public Boolean sortAsc { get; set; }
        
    public SelectAdoptionBookTitlesCon(ApexPages.StandardController stdController) {
        
        try
        {
            opportunity = (Opportunity)stdController.getRecord();
            adoptionId  = System.currentPageReference().getParameters().get(ADOPTION_ID_PARAM);
            retURL      = System.currentPageReference().getParameters().get(RETURN_URL_PARAM);
            sortAsc     = true;
            searchField = 'search_byISBN';
            excludeInternationalTitles = false;

            // get associated Price book Id            
            Opportunity op = [SELECT o.Pricebook2Id 
                              FROM Opportunity o
                              WHERE o.Id =: opportunity.Id];
            
            priceBookId = op.Pricebook2Id;
            
            if (selectedProducts == null) {
                selectedProducts = new List<AdoptionBookTitle>();
                for (Integer i = 0; i < NO_OF_PRELOAD_ITEMS;i++) {
                    selectedProducts.add(new AdoptionBookTitle(null, createAdoptionBookTitle(), priceBookId, opportunity.CurrencyIsoCode));                
                }  
            }
            
        } catch (Exception ex) {
            // System.debug('EXCEPTION[\n'+ex.getMessage()+'\n]\n');
        }               
    }
    
    public Boolean getRenderSearchResults() {
        return (((lookupProducts == null) || (lookupProducts.size() == 0)) ? false : true);
    }
    
    public PageReference searchByField() {
        Search();
        return null;    
    }    
    
    // perform search by defiend searching criterias
    public void Search() {     
        try {
            List<SearchingCriteria> criterias = getSearchingCriterias();
            String userOUPCountry = getUserOUPCountry();
            String whereClause = SearchingCriteria.formWhereClause(criterias, filterAdvance);
            whereClause = prefixFieldNames(whereClause, 'p.Product2.');
            Integer resultSetLimit = 100;
                        
            String marketInternational = '';
            if (excludeInternationalTitles == false) {
                marketInternational = ' p.Product2.Market__c = \'INTERNATIONAL\' OR ';
            }
                    
            // determine sort by field and order 
            String orderByClause = 'ORDER BY ';
            if (searchField == 'search_byName') {
                orderByClause += 'p.Product2.Name';
            } else if (searchField == 'search_byISBN') {
                orderByClause += 'p.Product2.ISBN__c';
            } else if (searchField == 'search_byMarket') {
                orderByClause += 'p.Product2.Market__c';
            } else if (searchField == 'search_bySegment') {
                orderByClause += 'p.Product2.Segment__c';
            } else if (searchField == 'search_bySeries') {
                orderByClause += 'p.Product2.Series__c';
            }
            
            // asc or desc?
            orderByClause += (sortAsc == true) ? ' ASC' : ' DESC';
            
            if (whereClause.length() > 0) {
                whereClause  = whereClause + ' AND (' + marketInternational + ' p.Product2.Market__c = \'' + userOUPCountry  + '\' ) AND p.CurrencyIsoCode = \'' + opportunity.CurrencyIsoCode + '\' AND p.Pricebook2Id =\'' + priceBookId + '\' AND p.Product2.Selectable__c = true AND p.IsActive = true ' + orderByClause + ' LIMIT ' + resultSetLimit;
            } else {
                whereClause  =' WHERE (' + marketInternational + ' p.Product2.Market__c = \'' + userOUPCountry  + '\') ' + ' AND p.CurrencyIsoCode = \'' + opportunity.CurrencyIsoCode + '\' AND p.Pricebook2Id =\'' + priceBookId + '\' AND p.Product2.Selectable__c = true AND p.IsActive = true ' + orderByClause + ' LIMIT ' + resultSetLimit;
            }
                  
            lookupProducts = new List<SelectableProduct>(); 
    
            List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
                   
            try {
                query  = 'SELECT p.Product2.Series__c, p.Product2.Segment__c, p.Product2.Publisher__c, p.Product2.ProductCode, p.Product2.Selectable__c, ';
                query += '       p.Product2.Name, p.Product2.Market__c,  p.Product2.Level__c, p.Product2.Id, p.Product2.ISBN__c ';
                query += 'FROM PricebookEntry p ';
                query += whereClause;               
                pricebookEntries = Database.query(query);                                              
            } catch (DmlException ex) {
            } catch (Exception ex) {   
            }
            
            if ((pricebookEntries != null) && (pricebookEntries.size() > 0)) {
                Product2 p = null;
                for (PricebookEntry pb : pricebookEntries) {
                    p = pb.Product2;
                    lookupProducts.add(new SelectableProduct(p));                                                        
                }
            }
        } catch (DmlException ex) {
        } catch (Exception ex) {
        }   
    }
    
    public void resetSearchingCriterias() {
        searchingCriterias = new List<SearchingCriteria>();
        searchingCriterias.add(new SearchingCriteria(1, true, 'Name', 'c', '', 'AND'));
        searchingCriterias.add(new SearchingCriteria(2, true, 'Name', 'c', ''));
        searchingCriterias.add(new SearchingCriteria(3));
        searchingCriterias.add(new SearchingCriteria(4));
        searchingCriterias.add(new SearchingCriteria(5, false));    
        
        if (lookupProducts != null) {
            lookupProducts.clear();
        }
    }

    // Get Lookup Book titles product
    public List<SelectableProduct> getLookupProducts() { 
        if (lookupProducts == null) {
            lookupProducts = new List<SelectableProduct>();
        }        
        return lookupProducts;
    }
    
    public List<SearchingCriteria> getSearchingCriterias() {
        if(searchingCriterias == null) {
           resetSearchingCriterias();           
        }        
        return searchingCriterias;
    }
    
    public String getQuery() {
        return query;
    }
    
    public String getPageTitle() {
        String pageTitle = 'Select Products';        
        return pageTitle;
    }
    
    // Add selecting book titles to selected items list
    public void addSelectedProducts() {             
        for (SelectableProduct sp: getLookupProducts()) {
            if ((sp.isSelected) && (!IsProductSelected(sp.product))) {  
               if ((selectedProducts == null) || (selectedProducts.size() == 0)) {
                       getSelectedProducts().add(new AdoptionBookTitle(sp.product, createAdoptionBookTitle(), priceBookId, opportunity.CurrencyIsoCode));
               } else {
                       selectedProducts.add(0, new AdoptionBookTitle(sp.product, createAdoptionBookTitle(), priceBookId, opportunity.CurrencyIsoCode));
               }              
            } 
        }
    }  
    
    public void removeSelectedProduct()
    {
        ID bookTitleID = System.currentPageReference().getParameters().get('title_id');
        
        for (Integer i = 0; i < selectedProducts.size(); i++) {
            AdoptionBookTitle abt = selectedProducts.get(i);
            if (abt.ID == bookTitleID) {
                selectedProducts.remove(i);                
                return;
            }                    
        }
    }
    
    // Get selected Book Title(s)
    public List<AdoptionBookTitle> getSelectedProducts() {
        if (selectedProducts == null) {
            selectedProducts = new List<AdoptionBookTitle>();
        }
        return selectedProducts;
    }    
    
    // Get searching fields in SelectOption object 
    public List<SelectOption> getFields() {
        Map<String, Schema.SObjectField> M = Schema.SObjectType.Product2.fields.getMap();
        //List<String> searchingFields = new List<String>(M.keySet());
        List<String> searchingFields = getSearchingFieldNames();
        List<SelectOption> selectOption = new List<SelectOption>();
        selectOption.add(new SelectOption('', '--None--'));
        
        for (String s : searchingFields) {
            Schema.DescribeFieldResult F;
            F = M.get(s).getDescribe();
            selectOption.add(new SelectOption(F.getName(), F.getLabel()));
        }            
  
        return selectOption;       
    }     
    
    public PageReference save() {
        if (saveInternal() == false) {
            return null;
        }        
        PageReference pageReference = new PageReference('/'+adoptionId);
        pageReference.setRedirect(true);
        return pageReference;
    }
    
    public PageReference cancel() {
        PageReference pageReference = new PageReference('/'+adoptionId);
        pageReference.setRedirect(true);
        return pageReference;  
    }
    
    // internal save 
    public Boolean saveInternal()
    {   
        Boolean status = false;
        Boolean insertValues = true;     
        
        try 
        {
            // make sure we do not have duplicates             
            Map<ID, OpportunityLineItem> bookIds = getValidatedLineItems();
            if ((bookIds != null) && (bookIds.size() > 0)) {
                
                // Book Title records can not be created for Book Titles that already appear against the Adoption
                
                final List<OpportunityLineItem> existingLineItems = [SELECT Id, PricebookEntryId, PricebookEntry.Name  
                                                                     FROM OpportunityLineItem 
                                                                     WHERE OpportunityId =: adoptionId];
                                                                     
                if (existingLineItems.size() > 0) {
                    for (OpportunityLineItem existingItem: existingLineItems) {
                        OpportunityLineItem duplicateItem = bookIds.get(existingItem.PricebookEntryId);
                        if (duplicateItem != null)  {
                            duplicateItem.addError('Duplicate title ' + existingItem.PricebookEntry.Name);
                            insertValues = false;                                    
                        }
                    }
                }
                                
                if (insertValues == true) {
                    insert bookIds.values();
                    status = true;
                }
            } else {
                // no book titles were selected
                addErrorMessage('No products were selected.');               
            }       
        } catch (DmlException ex) {
            System.debug('DmlException ' + ex);            
            System.currentPageReference().getParameters().put('error', 'noInsert');
            ApexPages.addMessages(ex);
        } catch (Exception e) {
            System.debug('Exception ' + e);
            System.currentPageReference().getParameters().put('error', 'noInsert');                 
        }
        
        return status;
    }
         
    private OpportunityLineItem createAdoptionBookTitle() {
        // ctor - set PriceBookEntryId with the PriceBook2 Id as this will be overwritten later on
        final OpportunityLineItem opportunityLineItem = new OpportunityLineItem(OpportunityId = adoptionId, Quantity = 0, UnitPrice = 0);
        return opportunityLineItem;     
    }
    
    private Map<ID, OpportunityLineItem> getValidatedLineItems() {
        Boolean isValid = true;
        
        final Map<ID, OpportunityLineItem> bookIds = new Map<ID, OpportunityLineItem>();
        for (AdoptionBookTitle abt : selectedProducts) {
            if (!abt.needSave()) {
                continue;
            }
            if (!abt.validate()) {
                isValid = false;
            } else {
                OpportunityLineItem lineItem = abt.getAdoptionBookTitle();
                if (!bookIds.containsKey(lineItem.PricebookEntryId)) {                                   
                   bookIds.put(lineItem.PricebookEntryId, lineItem);                            
                } else {
                    // duplicates on the page
                    lineItem.addError('Duplicate title on the page');
                    isValid = false;
                }
            }     
        }
        
        return isValid ? bookIds : null;
    }
    
    // Check whether Product is selected or not
    private Boolean IsProductSelected(Product2 p) {
        for (AdoptionBookTitle abt : getSelectedProducts()) {
            if (abt.ID == p.id) {
                return true;
            }            
        }
        
        return false;
    }
    
    private List<String> getSearchingFieldNames() {
        // JRS 31/03/2011 - ELT-3-4 [Add Component Type, Edition]
        return new String[] { 'Name', 'ISBN__c', 'Level__c', 'Segment__c', 'Series__c', 'Market__c', 'Component_Type__c', 'Edition__c' };
    }
    
    private String prefixFieldNames(String fieldNames, String prefix) {
        String prefixFN = fieldNames;
        
        // iterate through the fields that require a prefix (namely the search criteria fields)
        List<String> flds = getSearchingFieldNames();
        for (String s : flds) {
            prefixFN = prefixFN.replace(s, prefix + s);
        }
        return prefixFN;
    }
    
    private String getUserOUPCountry() {
        String userID = UserInfo.getUserId();
        User user = [SELECT Id, OUP_Country__c FROM User WHERE Id = :userID LIMIT 1];
        if (user != null) {
            return user.OUP_Country__c;
        } else {
            return '';
        }
    }
    
    private void addErrorMessage(String msg) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
    }

        
    // Inner Classe(s)
    
    public class AdoptionBookTitle {
        
        public String getID() { return id; } 
        public String getBookTitle() { return bookTitle; } 
        Public OpportunityLineItem getAdoptionBookTitle() { return opportunityLineItem; }
        Public Boolean getIsPreLoad() { return isPreLoad; }
        public String ISBN { get; set; }
        public String getErrorMessage() { return errorMessage; }
        public Boolean getIsError() { return isError; }
        
        private ID priceBookId;
        private ID id;      
        private String bookTitle; // book title from selected book title product
        private OpportunityLineItem opportunityLineItem;    
        private String currencyIsoCode;
        private Boolean isPreLoad;
        private Boolean isError;
        private String errorMessage;
        
        public AdoptionBookTitle(Product2 pd, OpportunityLineItem oi, ID priceBookId, String curencyIsoCode) {
            
            try
            {
                isError = false;
            
                if (pd == null) {
                    opportunityLineItem = oi;
                    this.priceBookId    = priceBookId;
                    currencyIsoCode     = curencyIsoCode;
                    isPreLoad           = true;                    
                } else {
                    id                  = pd.id;
                    bookTitle           = pd.Name;
                    ISBN                = pd.ISBN__c;              
                    opportunityLineItem = oi;
                    this.priceBookId    = priceBookId;
                    currencyIsoCode     = curencyIsoCode;
                    isPreLoad           = false;
                    
                    // set the PricebookEntryId using the PricebookId (as set in ctor)
                    PricebookEntry pricebookEntry        = getPricebookEntry(pd.id, priceBookId, currencyIsoCode);                  
                    opportunityLineItem.PriceBookEntryId = pricebookEntry.Id;
                    
                    opportunityLineItem.UnitPrice        = getPricebookEntryUnitPrice(ISBN, priceBookId, currencyIsoCode);
                }                        
            } catch (Exception ex) {
            // System.debug('EXCEPTION[\n'+ex.getMessage()+'\n]\n');
            }
        }        
        
        public Boolean needSave() {
            if (isPreLoad == true) {
                if ((ISBN == null) || (ISBN.length() == 0)) {
                    this.bookTitle = null;
                    isError = false;
                    errorMessage = '';
                    return false;
                }
            }
            
            return true;             
        }
                
        public Boolean validate() {   
            Boolean status = true; 
 
            if (isPreLoad == true) {
                // ensure sure that's booktitle match with the ISBN input
                String arg = ISBN.trim();
                List<Product2> products = [SELECT id, name FROM Product2 WHERE ISBN__c = :arg];
                if (products.size() == 1) { 
                    Product2 product      = products.get(0);
                    bookTitle             = product.name;
                    id                    = product.id;
                    isError               = false;
                    errorMessage          = '';
                    
                    // set the PricebookEntryId using the PricebookId (as set in ctor)
                    PricebookEntry pricebookEntry        = getPricebookEntry(product.id, priceBookId, currencyIsoCode);
                    opportunityLineItem.PriceBookEntryId = pricebookEntry.Id;
                    
                    // only replace the price if it has not been entered by the user
                    if (opportunityLineItem.UnitPrice == 0.00) {
                        opportunityLineItem.UnitPrice = getPricebookEntryUnitPrice(arg, priceBookId, currencyIsoCode);
                    }
                } else {
                    isError      = true;
                    errorMessage = 'Invalid ISBN';
                    status       = false;
                }
            }
            
            if ((opportunityLineItem.Quantity == null) || (opportunityLineItem.Quantity < 1)) {
                opportunityLineItem.Quantity.addError('Quantity is a Required field');
                status = false;
            }
            
            // jrs 2009/06/24 - changed unit price from < 1 to < 0 
            if ((opportunityLineItem.UnitPrice == null) || (opportunityLineItem.UnitPrice < 0)) {
                opportunityLineItem.UnitPrice.addError('Sales Price is a Required field');
                status = false;
            }
                     
            if (isPreLoad && !status) {
                bookTitle  = null;
            }
            
            return status;                     
        }
        
        private PricebookEntry getPricebookEntry(ID product2Id, ID pricebook2Id, String currencyIsoCode) {
            
            // use product id and the PriceBook2 id to determine the pricebook entry id         
            PricebookEntry pricebookEntry = [SELECT Id 
                                             FROM PricebookEntry 
                                             WHERE Product2Id =: product2Id AND CurrencyIsoCode =: currencyIsoCode AND Pricebook2Id =: pricebook2Id AND IsActive = true];       
            
            return pricebookEntry;
        }
        
        private Double getPricebookEntryUnitPrice(String ISBN, ID pricebook2Id, String currencyIsoCode) {
            Double unitprice = 0;
            
            try {               
                List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
                
                String sql  = 'SELECT p.UnitPrice FROM PricebookEntry p ';
                sql        += 'WHERE (p.Product2.ISBN__c = \''+ISBN+'\') AND (p.CurrencyIsoCode = \''+currencyIsoCode+'\') AND p.Pricebook2Id = \''+pricebook2Id+'\'';
                  
                pricebookEntries = Database.query(sql);
                unitprice = pricebookEntries.get(0).UnitPrice;
                            
            } catch (Exception ex) {
                // System.debug('EXCEPTION[\n'+ex.getMessage()+'\n]\n');
            }
            
            return unitprice;
        }
    }
     
    public class SelectableProduct {   
        public Product2 product { get; set; } 
        public Boolean isSelected { get; set; }
            
        public SelectableProduct(Product2 p) {
            product = p;
            isSelected = false;
        }          
    }   
    
}