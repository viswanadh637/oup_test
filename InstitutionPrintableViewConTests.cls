@isTest(SeeAllData=true)
private class InstitutionPrintableViewConTests {

// Test Contoller
    
    public static testMethod void getPageTitleTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        String pageTitle = institutionPrintableViewCon.getPageTitle();
        System.assert(pageTitle.length() > 0);
    }
    
    public static testMethod void getAccountTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        Account account = institutionPrintableViewCon.getAccount();
        System.assert(account != null);
    }
    
    public static testMethod void cancelTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        PageReference pageReference = institutionPrintableViewCon.cancel();
        System.assert(pageReference != null);
    }    
    
    public static testMethod void refreshPreviewTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        institutionPrintableViewCon.RefreshPreview();       
    }
        
    public static testMethod void getBackToDetailsTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        String backToDetails = institutionPrintableViewCon.getBackToDetails();      
    } 
       
    public static testMethod void getPrintDetailsTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        String printDetails = institutionPrintableViewCon.getPrintDetails();        
    }    
       
    public static testMethod void getTargetInstitutionImageNameTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        String targetInstitutionImageName = institutionPrintableViewCon.getTargetInstitutionImageName();        
    }
       
    public static testMethod void getAssociatedContactsTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        List<Positions_Held__c> associatedContacts = institutionPrintableViewCon.getAssociatedContacts();       
    } 
       
    public static testMethod void getBooksUsedTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        List<Books_Used__c> booksUsed = institutionPrintableViewCon.getBooksUsed();     
    }
      
    public static testMethod void getOpenActivitiesTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        List<InstitutionPrintableViewCon.Activity> openActivities = institutionPrintableViewCon.getOpenActivities();        
    }
      
    public static testMethod void getAdoptionsTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        List<Opportunity> adoptions = institutionPrintableViewCon.getAdoptions();       
    }
    
    public static testMethod void getGratisLineItemsTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        List<Gratis_Line_Items__c> gratisLineItems = institutionPrintableViewCon.getGratisLineItems();      
    }
    
    public static testMethod void getPartnersTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        List<Partner> partners = institutionPrintableViewCon.getPartners();     
    }
    
    public static testMethod void getActivitiesTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        List<InstitutionPrintableViewCon.Activity> activities = institutionPrintableViewCon.getActivities();        
    }
    
    public static testMethod void getBookUsedStartDateTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        Task bu_startDate = institutionPrintableViewCon.getBookUsedStartDate();     
    }
    
    public static testMethod void getBookUsedEndDateTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        Task bu_endDate = institutionPrintableViewCon.getBookUsedEndDate();     
    }
    
    public static testMethod void getOpenActivitiesStartDateTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        Task oa_startDate = institutionPrintableViewCon.getOpenActivitiesStartDate();       
    }
    
    public static testMethod void getOpenActivitiesEndDateTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        Task oa_endDate = institutionPrintableViewCon.getOpenActivitiesEndDate();       
    }
    
    public static testMethod void getGratisLineItemsStartDateTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        Task gi_startDate = institutionPrintableViewCon.getGratisLineItemsStartDate();      
    }
    
    public static testMethod void getGratisLineItemsEndDateTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        Task gi_endDate = institutionPrintableViewCon.getGratisLineItemsEndDate();      
    }
    
    public static testMethod void getActivityHistoryStartDateTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        Task ah_startDate = institutionPrintableViewCon.getActivityHistoryStartDate();      
    }
    
    public static testMethod void getActivityHistoryEndDateTest() {
        InstitutionPrintableViewCon institutionPrintableViewCon = pageWithParameters01();
        Task ah_endDate = institutionPrintableViewCon.getActivityHistoryEndDate();      
    }
    
    
// Test Activity    
    
    public static testMethod void activityTest() {
        List<Task> task = new List<Task>();
        
        String sql  = 'SELECT t.What.Name, t.Who.Name, t.WhoId, t.Subject, t.Status, t.Priority, t.Owner.Name, t.OwnerId, t.IsClosed, t.Id, t.Description, t.ActivityDate, t.Account.Name, t.AccountId, ';
        sql += ' t.Task_Notes__c, t.Purpose__c ';
        sql += 'FROM Task t ';
        sql += 'LIMIT 1 ';
        // sql += 'WHERE (t.AccountId = \'' + getInstitution().Id + '\') AND (t.Status != \'Completed\') ';        
        task = Database.query(sql);
        
        if ((task != null) && (task.size() > 0)) {
            for (Task tk : task) {
                InstitutionPrintableViewCon.Activity at = new InstitutionPrintableViewCon.Activity(tk.Subject, tk.ActivityDate, tk.Who.Name, tk.What.Name, tk.Status, tk.Task_Notes__c, tk.Purpose__c, tk.Owner.Name, '', true);
                String subject         = at.getSubject();
                Boolean isTask         = at.getIsTask();
                Date activityDate      = at.getActivityDate();
                String dtTmp           = at.getActivityDateToString();                
                String name            = at.getName();
                String relatedTo       = at.getRelatedTo();
                String status          = at.getStatus();
                String notes           = at.getNotes();
                String purpose         = at.getPurpose();
                String ownerName       = at.getOwnerName();
                String needsObjectives = at.getNeedsObjectives();                
            }
        }       
    }



// Data Preparation
    
    private static Account getInstitution() {
        Account[] accounts = [SELECT Id FROM Account LIMIT 20];
        System.assert(accounts.size() >= 1, 'Please create at least 1 Account(Insitution) for testing');
        return accounts[0];
    }
    
    private static InstitutionPrintableViewCon pageWithParameters01() {
        // Add parameters to page URL
        System.currentPageReference().getParameters().put('Id' /* InstitutionPrintableViewCon.INSTITUTE_URL_PARAM */, getInstitution().Id);
        System.currentPageReference().getParameters().put('incAC', '1');
        System.currentPageReference().getParameters().put('incBU', '1');
        System.currentPageReference().getParameters().put('incOA', '1');
        System.currentPageReference().getParameters().put('incAH', '1');
        System.currentPageReference().getParameters().put('incAD', '1');
        System.currentPageReference().getParameters().put('incGI', '1');
        System.currentPageReference().getParameters().put('incPT', '1');
        System.currentPageReference().getParameters().put('incADC', '1');
        System.currentPageReference().getParameters().put('retURL', '/home/home.jsp');
        System.currentPageReference().getParameters().put('cancelURL', '/home/home.jsp');       
                
        InstitutionPrintableViewCon institutionPrintableViewCon = new InstitutionPrintableViewCon();
        System.assert(institutionPrintableViewCon != null);
        
        Boolean incAC = institutionPrintableViewCon.incAC;
        Boolean incBU = institutionPrintableViewCon.incBU;
        Boolean incOA = institutionPrintableViewCon.incOA;
        Boolean incAH = institutionPrintableViewCon.incAH;
        Boolean incAD = institutionPrintableViewCon.incAD;
        Boolean incADC = institutionPrintableViewCon.incADC;
        Boolean incGI = institutionPrintableViewCon.incGI;
        Boolean incPT = institutionPrintableViewCon.incPT;
        
        return institutionPrintableViewCon;
    }
}