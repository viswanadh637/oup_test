public class WarehouseDocumentUpload
{

    public List<string> getMissing() {
        return missing;
    }


    public boolean imported { get; set; }

    public PageReference cancel() {
        return new PageReference('/'+documentId);
    }

    public String numberCreated { get; set; }
    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    String[] lines= new String[]{};
    String[] lines2= new String[]{};
    List<Warehouse_Document_Line__c> LinesToCreate = new List<Warehouse_Document_Line__c>();
     public static final String DOCUMENT_URL_PARAM = 'Id';
     public ID documentId {get;set;}
     public boolean canProceed {get; set;}
    List<string> missing = new List<string>();
     
     public WarehouseDocumentUpload()
     {  
                     imported = false;

                    documentId = System.currentPageReference().getParameters().get(DOCUMENT_URL_PARAM);
                        if (documentId != null)
                        {
                        canProceed = true;
                     
                        }
                        else
                        {
                        canProceed = false;
                         ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Please use this page from a warehouse document screen.');
                         ApexPages.addMessage(errormsg);     
                        }
                   
     }
     
    public void ReadFile()
    {
    
        try 
        {
        missing = new List<string>();
         imported = false;
            nameFile = contentFile.toString().replace('\r', '');     
            lines = nameFile.split('\n');
             lines2 = nameFile.split('\n');
            system.debug(lines.size());
         Set<string> ISBNs = new Set<string>();
         
            for (string s :lines)
                {
                s= s.replace(';',',').replace('"','');
                
               ISBNs.add(s.split(',')[0]);
                }
                
                List<Product2> products = [select id, ISBN__c from Product2 where ISBN__c in: ISBNs];
                
                for (string s :lines2)
                {
                  s= s.replace(';',',').replace('"','');
                Warehouse_Document_Line__c wdl = new Warehouse_Document_Line__c();
                wdl.Document__c = documentId;
                wdl.Quantity__c = integer.valueOf(s.split(',')[1]);
                Boolean isMissing = true;
                
                
                for (Product2 p2 : products)
                {
                   if (p2.ISBN__c == s.split(',')[0]) 
                   {
                   wdl.WD_book_title__c = p2.Id;
                   LinesToCreate.Add(wdl);
                   isMissing = false;
                   break;
                   }
                }
                
                if (isMissing) {missing.add(s.split(',')[0]);}
                }
                
            database.insert(LinesToCreate);
            //return to page
            imported = true;
         }
         catch (Exception e)
         {
             canProceed = false;     
             ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage());
             ApexPages.addMessage(errormsg); 
              imported = false;
      
         }   
       
    }
    
}