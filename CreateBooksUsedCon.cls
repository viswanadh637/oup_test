public class CreateBooksUsedCon {
    
    public static final Integer NO_OF_PRELOAD_ITEMS               = 10;
    public static final String POSITION_HELD_ID_PARAM             = 'Positions_Held_Id';
    public static final String POSITION_HELD_CONTACT_ID_PARAM     = 'Positions_Held_ContactId';
    public static final String POSITION_HELD_INSTITUTION_ID_PARAM = 'Positions_Held_InstitutionId';
    public static final String ACCOUNT_ID_PARAM                   = 'Account_Id';
    private final ID positionHeldId;
    private final ID positionHeldContactId;
    private final ID positionHeldInstitutionId;
    private final ID accountId;
    private final ID institutionId;
    
    private List<SelectableProduct> lookupProducts;               // book titles product lookup list
    private List<BooksUsed> selectedProducts;
    private List<SearchingCriteria> searchingCriterias;                 
    private String query = '';                                    // query build by search criterias         
    public String filterAdvance { get; set; }
    public Boolean excludeInternationalTitles { get; set; }
    
    public String searchField { get; set; }
    public Boolean sortAsc { get; set; }
         public List<SelectOption> getPricebooks() {
        
        pricebooks.Add(new SelectOption('None', 'None'));
        for (Pricebook2 p: [Select id, Name from Pricebook2 where IsActive = true ORDER BY Name])
        {
        pricebooks.Add(new SelectOption(p.Id, p.Name));
        
        }
        return pricebooks;
    }


    private List<SelectOption> pricebooks = new List<SelectOption>();

    public String selectedPricebook {get; set;}
    public CreateBooksUsedCon() {
        
        try
        {
            positionHeldId             = System.currentPageReference().getParameters().get(POSITION_HELD_ID_PARAM);
            positionHeldContactId      = System.currentPageReference().getParameters().get(POSITION_HELD_CONTACT_ID_PARAM);
            positionHeldInstitutionId  = System.currentPageReference().getParameters().get(POSITION_HELD_INSTITUTION_ID_PARAM);
            accountId                  = System.currentPageReference().getParameters().get(ACCOUNT_ID_PARAM);
            excludeInternationalTitles = false;
            
            if (accountId != null) {
                institutionId = accountId;
            } else if (positionHeldId != null) {
                institutionId = positionHeldInstitutionId;
            }
            
            if (selectedProducts == null) {
                selectedProducts = new List<BooksUsed>();
                for (Integer i = 0; i < NO_OF_PRELOAD_ITEMS;i++) {
                    selectedProducts.add(new BooksUsed(null, createBooksUsed()));                
                }  
            }
            
            sortAsc     = true;
            searchField = 'search_byName';            
        } catch (Exception ex) {
            // System.debug('EXCEPTION[\n'+ex.getMessage()+'\n]\n');
        }               
    }
    
    public Boolean getRenderSearchResults() {
        return (((lookupProducts == null) || (lookupProducts.size() == 0)) ? false : true);
    }
    
    public PageReference searchByField() {
        Search();
        return null;    
    }    
    
    // perform search by defiend searching criterias
    public void Search() {
        
        List<SearchingCriteria> criterias = getSearchingCriterias();  
        String userOUPCountry = getUserOUPCountry();    
        String whereClause = SearchingCriteria.formWhereClause(criterias, filterAdvance);
        Integer resultSetLimit = 100;

        String marketInternational = '';
        if (excludeInternationalTitles == false) {
            marketInternational = 'Market__c = \'INTERNATIONAL\' OR ';
        }
        
        // determine sort by field and order 
        String orderByClause = 'ORDER BY ';
        if (searchField == 'search_byName') {
            orderByClause += 'Name';
        } else if (searchField == 'search_byISBN') {
            orderByClause += 'ISBN__c';
        } else if (searchField == 'search_byPublisher') {
            orderByClause += 'Publisher__c';
        } else if (searchField == 'search_byMarket') {
            orderByClause += 'Market__c';
        } else if (searchField == 'search_bySegment') {
            orderByClause += 'Segment__c';
        } else if (searchField == 'search_bySeries') {
            orderByClause += 'Series__c';
        }
        
        // asc or desc?
        orderByClause += (sortAsc == true) ? ' ASC' : ' DESC';
        
        
        
                if(whereClause.length() > 0) 
        {
            whereClause  = whereClause + ' AND ((' + marketInternational + ' Market__c = \'' + userOUPCountry  + '\'))';// LIMIT 100';
        }
        else
        {
            whereClause  =' WHERE (' + marketInternational + ' Market__c = \'' + userOUPCountry  + '\')';// LIMIT 100';
        }    
              
              if (selectedPricebook != 'None')
              {
              whereClause = whereClause +' AND Id IN (SELECT Product2Id from PricebookEntry where Pricebook2Id = \''+selectedPricebook+'\' and IsActive = true) ' + orderByClause + ' LIMIT ' + resultSetLimit; 
           
              }
              else
              {
              whereClause = whereClause + orderByClause + ' LIMIT ' + resultSetLimit;
              }
        
              
        lookupProducts = new List<SelectableProduct>(); 

        List<Product2> products = new List<Product2>();        
        try {
            query = 'SELECT Series__c, Segment__c, Publisher__c, ProductCode, Name, Market__c, Level__c, Id, ISBN__c FROM Product2' + whereClause;
            products = Database.query(query);
        } catch (DmlException ex) {
            products = null;
        } catch (Exception ex) {
            products = null;
        }
         
        if ((products != null) && (products.size() > 0)) {
            for (Product2 p : products) {          
                lookupProducts.add(new SelectableProduct(p));
            }
        }
    }
    
    public void resetSearchingCriterias() {
        searchingCriterias = new List<SearchingCriteria>();
        searchingCriterias.add(new SearchingCriteria(1, true, 'Name', 'c', '', 'AND'));
        searchingCriterias.add(new SearchingCriteria(2, true, 'Name', 'c', ''));
        searchingCriterias.add(new SearchingCriteria(3));
        searchingCriterias.add(new SearchingCriteria(4));
        searchingCriterias.add(new SearchingCriteria(5, false));    
        
        if (lookupProducts != null) {
            lookupProducts.clear();
        }
    }

    // Get Lookup Book titles product
    public List<SelectableProduct> getLookupProducts() { 
        if (lookupProducts == null) {
            lookupProducts = new List<SelectableProduct>();
        }        
        return lookupProducts;
    }
    
    public List<SearchingCriteria> getSearchingCriterias() {
        if(searchingCriterias == null) {
           resetSearchingCriterias();           
        }        
        return searchingCriterias;
    }
    
    public String getQuery() {
        return query;
    }
    
    public String getPageTitle() {
        String pageTitle = 'Create Products Used';
        
        if (positionHeldContactId != null) {
            Contact contact = [SELECT name FROM Contact WHERE Id = :positionHeldContactId];
            pageTitle += ' - ' + contact.Name;
        }
        
        if (institutionId != null) {
            Account account = [SELECT name FROM Account WHERE Id = :institutionId];
            pageTitle += ' - ' + account.Name;
        }
        
        return pageTitle;
    }
    
    // Add selecting book titles to selected items list
    public void addSelectedProducts() {             
        for (SelectableProduct sp: getLookupProducts()) {
            if ((sp.isSelected) && (!IsProductSelected(sp.product))) {  
               if ((selectedProducts == null) || (selectedProducts.size() == 0)) {
                       getSelectedProducts().add(new BooksUsed(sp.product, createBooksUsed()));
               } else {
                       selectedProducts.add(0, new BooksUsed(sp.product, createBooksUsed()));
               }              
            } 
        }
    }  
    
    public void removeSelectedProduct()
    {
        ID bookTitleID = System.currentPageReference().getParameters().get('title_id');
        
        for (Integer i = 0; i < selectedProducts.size(); i++) {
            BooksUsed bu = selectedProducts.get(i);
            if (bu.getBooksUsed().Book_Used__c == bookTitleID) {
                selectedProducts.remove(i);                
                return;
            }                    
        }
    }
    
    // Get selected Book Title(s)
    public List<BooksUsed> getSelectedProducts() {
        if (selectedProducts == null) {
            selectedProducts = new List<BooksUsed>();
        }
        return selectedProducts;
    }    
    
    // Get searching fields in SelectOption object 
    public List<SelectOption> getFields() {
        Map<String, Schema.SObjectField> M = Schema.SObjectType.Product2.fields.getMap();
        //List<String> searchingFields = new List<String>(M.keySet());
        List<String> searchingFields = getSearchingFieldNames();
        List<SelectOption> selectOption = new List<SelectOption>();
        selectOption.add(new SelectOption('', '--None--'));
        
        for (String s : searchingFields) {
            Schema.DescribeFieldResult F;
            F = M.get(s).getDescribe();
            selectOption.add(new SelectOption(F.getName(), F.getLabel()));
        }            
  
        return selectOption;       
    }     
    
    public PageReference save() {
        if (saveInternal() == false) {
            return null;
        }
        return redirectToSourcePage(); 
    }
    
    public PageReference cancel() {
        return redirectToSourcePage();       
    }
    
    // internal save 
    public Boolean saveInternal()
    {   
        Boolean status = false;
        Boolean insertValues = true;     
        
        try 
        {
            // make sure we do not have duplicates             
            Map<ID, Books_Used__c> newBookIds = getValidatedLineItems();
            if ((newBookIds != null) && (newBookIds.size() > 0)) {                
                
                // Books Used records can not be created for Book titles that already appear 
                // against the Institution or Contact that don’t have a status of “Obsolete”
                
                if (institutionId != null) {
                    List<Books_Used__c> existingBooksUsed = null;
                    if (positionHeldId != null) {
                        existingBooksUsed = [SELECT Book_Used__r.Name, Book_Used__c, Status__c, Id
                                                                       FROM Books_Used__c
                                                                       WHERE Institution__c =: institutionId AND Teacher__c =: positionHeldContactId AND Book_Used__c IN: newBookIds.keySet()];
                    } else {
                        existingBooksUsed = [SELECT Book_Used__r.Name, Book_Used__c, Status__c, Id
                                                                       FROM Books_Used__c
                                                                       WHERE Institution__c =: institutionId AND Book_Used__c IN: newBookIds.keySet()];
                    }
                        
                    if (existingBooksUsed.size() > 0) {
                        for (Books_Used__c existingItem: existingBooksUsed){
                            Books_Used__c duplicateItem = newBookIds.get(existingItem.Book_Used__c);
                            if (duplicateItem != null)  {
                                if (existingItem.Status__c.equals('Obsolete') == false) {
                                    duplicateItem.addError('Duplicate title ' + existingItem.Book_Used__r.Name);
                                    insertValues = false;                                    
                                }
                            } 
                        }
                    }    
                        
                    if (insertValues == true) {
                        insert newBookIds.values();
                        status = true;
                    }
                }        
            }
        } catch (DmlException ex) {
            System.debug(ex);            
            System.currentPageReference().getParameters().put('error', 'noInsert');
            ApexPages.addMessages(ex);
        } catch (Exception e) {
            System.debug(e);
            System.currentPageReference().getParameters().put('error', 'noInsert');                 
        }
        
        return status;
    }
    
    public PageReference cascadeAdoptionDate() {        
        Date dtCopy = null;
        
        for (Integer i = 0; i < selectedProducts.size(); i++) {
            BooksUsed bu = selectedProducts.get(i);
            if (bu.BookTitle == null) {
                break;
            }
            if (i == 0) {
                // get the first line date
                dtCopy = bu.getBooksUsed().Adoption_Date__c;
            } else {
                // copy the first line date to remaining fields that have a book title
                // and if item has not yet been assigned a date
                if (bu.getBooksUsed().Adoption_Date__c == null) {
                    bu.getBooksUsed().Adoption_Date__c = dtCopy;
                }
            }
        }
        
        return null;
    }
    
    public PageReference cascadeRenewalDate() {
        Date dtCopy = null;
        
        for (Integer i = 0; i < selectedProducts.size(); i++) {
            BooksUsed bu = selectedProducts.get(i);
            if (bu.BookTitle == null) {
                break;
            }
            if (i == 0) {
                // get the first line date
                dtCopy = bu.getBooksUsed().Renewal_Date__c;
            } else {
                // copy the first line date to remaining fields that have a book title
                // and if item has not yet been assigned a date
                if (bu.getBooksUsed().Renewal_Date__c == null) {
                    bu.getBooksUsed().Renewal_Date__c = dtCopy;
                }
            }
        }
        
        return null;
    }
    
    public PageReference cascadePurchaseDate() {
        
        Date dtCopy = null;
        
        for (Integer i = 0; i < selectedProducts.size(); i++) {
            BooksUsed bu = selectedProducts.get(i);
            if (bu.BookTitle == null) {
                break;
            }
            if (i == 0) {
                // get the first line date
                dtCopy = bu.getBooksUsed().Purchase_Date__c;
            } else {
                // copy the first line date to remaining fields that have a book title
                // and if item has not yet been assigned a date
                if (bu.getBooksUsed().Purchase_Date__c == null) {
                    bu.getBooksUsed().Purchase_Date__c = dtCopy;
                }
            }
        }
        
        return null;
    }
        
    public Boolean getHasContactName() 
    {
        return (positionHeldContactId != null);  
    }
    
    private Books_Used__c createBooksUsed()
    {
        final Books_Used__c booksUsed = new Books_Used__c(Institution__c = institutionId, Teacher__c = positionHeldContactId);
        return booksUsed;
    }
    
    private Map<ID, Books_Used__c> getValidatedLineItems()
    {
        Boolean isValid = true;
        
        final Map<ID, Books_Used__c> newBookIds = new Map<ID, Books_Used__c>();
        for (BooksUsed bu : selectedProducts) {                        
            if (!bu.needSave()) {
                continue;
            }
            if (!bu.validate()) {
                isValid = false;
            } else {
                Books_Used__c lineItem = bu.getBooksUsed();
                if (!newBookIds.containsKey(lineItem.Book_Used__c)) {                                   
                   newBookIds.put(lineItem.Book_Used__c, lineItem);                                   
                } else {
                    // duplicates on the page
                    lineItem.addError('Duplicate title on the page');
                    isValid = false;
                }
            }     
        }
        
        return isValid ? newBookIds : null;
    }
    
    // Check whether Product is selected or not
    private Boolean IsProductSelected(Product2 p) {
        for (BooksUsed bu : getSelectedProducts()) {
            if (bu.getBooksUsed().Book_Used__c == p.id) {
                return true;
            }            
        }
        return false;
    }
    
    private PageReference redirectToSourcePage() {
        String reference = 'home/home.jsp';
            
        // determine original page
        if (accountId != null) {
            // return to institute page
            reference = accountId;          // reference = '001/o';
        } else if (positionHeldId != null) {
            reference = positionHeldId;     // reference = '003/o';
        }
            
        PageReference pageReference = new PageReference('/'+reference);
        pageReference.setRedirect(true);
        return pageReference;
    }
    
    private List<String> getSearchingFieldNames() {
        // JRS 31/03/2011 - ELT-3-4 [Add Component Type, Edition]
        return new String[] { 'Name', 'ISBN__c', 'Level__c', 'Segment__c', 'Series__c', 'Publisher__c', 'Component_Type__c', 'Edition__c', 'Subject__c' };
    }    
    
    private String getUserOUPCountry() {
        String userID = UserInfo.getUserId();
        User user = [SELECT Id, OUP_Country__c FROM User WHERE Id = :userID LIMIT 1];
        if (user != null) {
            return user.OUP_Country__c;
        } else {
            return '';
        }
    }
    
    private void addErrorMessage(String msg) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
    }

        
    // Inner Classe(s)
    
    public class BooksUsed {
        
        public String getBookTitle() { return bookTitle; } 
        Public Books_Used__c getBooksUsed() { return bookused; }
        Public Boolean getIsPreLoad() { return isPreLoad; }
        public String ISBN{ get; set; }
        public String getErrorMessage() { return errorMessage; }
        public Boolean getIsError() { return isError; }                
        private String bookTitle; // book title from selected book title product
        private Books_Used__c bookused;    
        private Boolean isPreLoad;
        private Boolean isError;
        private String errorMessage;
        
        public BooksUsed(Product2 bt, Books_Used__c bu) {
            
            try
            {
                isError = false;
            
                if (bt == null) {
                    bookused        = bu;                
                    isPreLoad       = true;
                } else {
                    bookTitle       = bt.Name;
                    ISBN            = bt.ISBN__c;
                    bu.Book_Used__c = bt.Id;
                    bookused        = bu;
                    isPreLoad       = false;
                }                        
            } catch (Exception ex) {
            // System.debug('EXCEPTION[\n'+ex.getMessage()+'\n]\n');
            }
        }        
        
        public Boolean needSave()
        {
            if (isPreLoad == true) {
                if ((ISBN == null) || (ISBN.length() == 0)) {
                    this.bookTitle = null;
                    isError = false;
                    errorMessage = '';
                    return false;
                }
            }
            
            return true;             
        }
                
        public Boolean validate()
        {   
            Boolean status = true; 
            
            if (isPreLoad == true) {
                // ensure sure that's booktitle match with the ISBN input
                String arg = ISBN.trim();
                List<Product2> products = [SELECT id, name FROM Product2 WHERE ISBN__c = :arg];
                if (products.size() == 1) {
                    Product2 product      = products.get(0);
                    bookTitle             = product.name;    
                    bookused.Book_Used__c = product.id;
                    isError               = false;
                    errorMessage          = '';            
                } else {
                    isError      = true;
                    errorMessage = 'Invalid ISBN';
                    status       = false;
                }
            }
            
            if ((bookused.Number_used__c == null) || (bookused.Number_used__c < 1)) {
                bookused.Number_used__c.addError('Number Used is Required field');
                status = false;
            }
            
            if (bookused.Purchase_Date__c == null) {
                // non-mandatory field
                // bookused.Purchase_Date__c.addError('Purchase Date is Required field');
                // status = false;
            }
            
            if (bookused.Adoption_Date__c == null) {
                bookused.Adoption_Date__c.addError('Adoption Date is Required field');
                status = false;
            }
            
            if (bookused.Renewal_Date__c == null) {
                bookused.Renewal_Date__c.addError('Renewal Date is Required field');
                status = false;
            }
            
            if (isPreLoad && !status) {
                bookTitle  = null;
            }
            
            return status;                     
        }
    }
     
    public class SelectableProduct {   
        public Product2 product { get; set; } 
        public Boolean isSelected { get; set; }
            
        public SelectableProduct(Product2 p) {
            product = p;
            isSelected = false;
        }          
    }
}